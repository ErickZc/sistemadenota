﻿namespace SistemaDeNotas.Model
{
    class Estudiante
    {
        public Estudiante()
        {

        }

        public int id_estudiante { get; set; }

        public string nombre { get; set; }

        public string apellido { get; set; }

        public string direccion { get; set; }

        public string padreResponsable { get; set; }

        public string telefono { get; set; }

        public string estado_promedio { get; set; }

        public string fecha_creacion { get; set; }

        public string estado { get; set; }

        public Grado id_grado { get; set; }

    }
}
