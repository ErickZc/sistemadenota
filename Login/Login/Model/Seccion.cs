﻿namespace SistemaDeNotas.Model
{
    class Seccion
    {
        public Seccion()
        {

        }

        public int id_seccion { get; set; }

        public string seccion { get; set; }

        public string estado { get; set; }
    }
}
