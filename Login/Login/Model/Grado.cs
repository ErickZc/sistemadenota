﻿using System;

namespace SistemaDeNotas.Model
{
    class Grado
    {
        public Grado()
        {

        }

        public int id_grado { get; set; }

        public string grado { get; set; }

        public Seccion id_seccion { get; set; }

        public string estado { get; set; }

        public int cantidad_alumno { get; set; }

        public static implicit operator int(Grado v)
        {
            throw new NotImplementedException();
        }
    }
}
