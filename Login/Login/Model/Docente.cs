﻿namespace SistemaDeNotas.Model
{
    class Docente
    {
        public Docente() { }

        public int id_docente { get; set; }

        public string nombre { get; set; }

        public string apellido { get; set; }

        public string direccion { get; set; }

        public string telefono { get; set; }

        public string dui { get; set; }

        public string nit { get; set; }

        public string correo { get; set; }

        public string fecha_creacion { get; set; }

        public string estado { get; set; }

        public string usuario { get; set; }

        public string password { get; set; }

        public string genero { get; set; }

        public Rol_Usuario id_rol { get; set; }
    }
}
