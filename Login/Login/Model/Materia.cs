﻿namespace SistemaDeNotas.Model
{
    class Materia
    {
        public Materia()
        {

        }

        public int id_materia { get; set; }

        public string nombre_materia { get; set; }

        public string estado { get; set; }
    }
}
