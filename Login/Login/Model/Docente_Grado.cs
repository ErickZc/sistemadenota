﻿namespace SistemaDeNotas.Model
{
    class Docente_Grado
    {
        public Docente_Grado()
        {

        }

        public int idDocente_Grado { get; set; }

        public Docente id_docente { get; set; }

        //public int id_docente { get; set; }

        public Grado id_grado { get; set; }

        //public int id_grado { get; set; }

        public string fecha { get; set; }

        public string estado { get; set; }


    }
}
