﻿using SistemaDeNotas.DAO;
using SistemaDeNotas.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SistemaDeNotas.Formularios.Nota
{
    /// <summary>
    /// Lógica de interacción para FrmNota.xaml
    /// </summary>
    public partial class FrmNota : UserControl
    {
        int id_docente = 0;
        int _periodo = 0;
        int _materia = 0;
        int _grado = 0;

        bool editando = true;
        bool agregando = false;
        bool del = false;

        public FrmNota(int not)
        {
            id_docente = not;
            InitializeComponent();
            cargarComboMateria();
            cmbMateria.SelectionChanged += new SelectionChangedEventHandler(cmbMateria_SelectionChanged);
            bloquearCajas(false);
            bloquearBotones();
            cmbEstudiante.IsEnabled = false;
            cargarFiltro();
            btnLimpiar.IsEnabled = false;
        }

        #region

        public void cargarComboMateria()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            DaoDocenteMateriaG daoDMG = new DaoDocenteMateriaG();
            List<Model.Docente_Materia_Grado> listadoMateria = new List<Model.Docente_Materia_Grado>();
            try
            {
                listadoMateria = daoDMG.listarMateriaById(id_docente);

                foreach (var p in listadoMateria)
                {
                    lista.Add(p.id_materia.id_materia, p.id_materia.nombre_materia);
                }

                cmbMateria.ItemsSource = lista;
                cmbMateria.DisplayMemberPath = "Value";
                cmbMateria.SelectedValuePath = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }

        public void cargarComboGrado(int valor)
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            DaoGrado daoGrado = new DaoGrado();
            List<Model.Grado> listadoGrados = new List<Model.Grado>();

            cmbGrado.ItemsSource = null;
            cmbPeriodo.ItemsSource = null;
            cmbGrado.Items.Clear();
            cmbPeriodo.Items.Clear();

            try
            {
                listadoGrados = daoGrado.MostrarGradosById(valor, id_docente);

                foreach (var p in listadoGrados)
                {
                    lista.Add(p.id_grado, p.grado + ' ' + p.id_seccion.seccion);
                }

                cmbGrado.ItemsSource = lista;
                cmbGrado.DisplayMemberPath = "Value";
                cmbGrado.SelectedValuePath = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }

        public void cargarComboPeriodo(string valor)
        {
            cmbPeriodo.ItemsSource = null;
            cmbPeriodo.Items.Clear();

            if (!valor.Equals(""))
            {
                DaoPeriodo daoPeriodo = new DaoPeriodo();
                Dictionary<int, string> lista = new Dictionary<int, string>();
                List<Periodo> listaPeriodo = new List<Periodo>();

                try
                {
                    listaPeriodo = daoPeriodo.MostrarPeriodos();
                    foreach (var p in listaPeriodo)
                    {
                        lista.Add(p.id_periodo, p.periodo);
                    }
                    cmbPeriodo.ItemsSource = lista;
                    cmbPeriodo.DisplayMemberPath = "Value";
                    cmbPeriodo.SelectedValuePath = "Key";

                }
                catch
                {

                }

            }
        }

        public bool validarCombo()
        {
            if (cmbGrado.Text.Equals(""))
            {
                return false;
            }
            if (cmbMateria.Text.Equals(""))
            {
                return false;
            }
            if (cmbPeriodo.Text.Equals(""))
            {
                return false;
            }
            return true;
        }

        public void cargarDataGrid()
        {
            _periodo = int.Parse(cmbPeriodo.SelectedValue.ToString());
            _materia = int.Parse(cmbMateria.SelectedValue.ToString());
            _grado = int.Parse(cmbGrado.SelectedValue.ToString());

            DaoNotas daoNota = new DaoNotas();

            dgDatos.ItemsSource = daoNota.verNotasDocente(_periodo, _materia, _grado);
            cargarComboEstudiante(_periodo, _materia, _grado);

        }

        public void cargarComboEstudiante(int _periodo, int _materia, int _grado)
        {
            cmbEstudiante.ItemsSource = null;
            cmbEstudiante.Items.Clear();

            DaoEstudiante daoEstudiante = new DaoEstudiante();
            Dictionary<int, string> lista = new Dictionary<int, string>();
            List<Model.Estudiante> listaEstudiante = new List<Model.Estudiante>();

            try
            {
                listaEstudiante = daoEstudiante.MostrarEstudiantesNotIn(_periodo, _materia, _grado);
                foreach (var p in listaEstudiante)
                {
                    lista.Add(p.id_estudiante, p.nombre + ' ' + p.apellido);
                }
                cmbEstudiante.ItemsSource = lista;
                cmbEstudiante.DisplayMemberPath = "Value";
                cmbEstudiante.SelectedValuePath = "Key";

            }
            catch
            {

            }

        }

        public void bloquearCajas(bool valor)
        {
            txtIdNota.IsEnabled = false;
            txtAct1.IsEnabled = valor;
            txtAct2.IsEnabled = valor;
            txtAct3.IsEnabled = valor;
            txtExamen.IsEnabled = valor;
            cmbEstudiante.IsEnabled = false;
        }

        public void bloquearBotones()
        {
            if (agregando)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }

            if (editando)
            {

                btnEliminar.IsEnabled = true;
                btnGuardar.IsEnabled = false;
                btnModificar.IsEnabled = true;
                btnNuevo.IsEnabled = true;
            }
            if (del)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }
        }

        public void limpiar()
        {
            txtIdNota.Clear();
            txtAct1.Clear();
            txtAct2.Clear();
            txtAct3.Clear();
            txtExamen.Clear();
            cmbEstudiante.SelectedIndex = -1;
            lblNotaPromedio.Content = "0.0";
            lblEstado.Content = "ESTADO";
        }

        public bool cajasVaciasInsert()
        {
            if (cmbEstudiante.Text.Equals("") || txtAct1.Text.Equals("") || txtAct2.Text.Equals("") || txtAct3.Text.Equals("") || txtExamen.Text.Equals(""))
            {
                tbData.Text = "Las cajas no pueden ir vacias!\nPor favor valide cada una";
                return false;
            }
            return true;
        }

        public bool cajasVaciasUpdate()
        {
            if (txtIdNota.Text.Equals("") || txtAct1.Text.Equals("") || txtAct2.Text.Equals("") || txtAct3.Text.Equals("") || txtExamen.Text.Equals(""))
            {
                tbData.Text = "Las cajas no pueden ir vacias!\nPor favor valide cada una";
                return false;
            }
            return true;
        }

        public bool cantidadNota()
        {
            bool cantidad = false;

            if ((double.Parse(txtAct1.Text) >= 0
                && double.Parse(txtAct1.Text) <= 10)
                && (double.Parse(txtAct2.Text) >= 0
                && double.Parse(txtAct2.Text) <= 10)
                && (double.Parse(txtAct3.Text) >= 0
                && double.Parse(txtAct3.Text) <= 10)
                && (double.Parse(txtExamen.Text) >= 0
                && double.Parse(txtExamen.Text) <= 10))
            {
                cantidad = true;
            }

            return cantidad;

        }

        public decimal calcularPromedio(decimal act1, decimal act2, decimal act3, decimal exam)
        {
            decimal promedio = 0;
            double actividades = 0;
            double examen = 0;
            double suma;
            double _act1 = double.Parse(act1.ToString());
            double _act2 = double.Parse(act2.ToString());
            double _act3 = double.Parse(act3.ToString());
            double _exam = double.Parse(exam.ToString());

            actividades = (((_act1 + _act2 + _act3) / 3) * 0.60);
            examen = _exam * 0.4;
            suma = examen + actividades;
            promedio = decimal.Parse(suma.ToString());

            return decimal.Round(promedio, 2);
        }

        public void agregarNota()
        {
            if (cajasVaciasInsert())
            {
                if (cantidadNota())
                {
                    Model.Nota nota = new Model.Nota();
                    Model.Estudiante estudiante = new Model.Estudiante();
                    Model.Materia materia = new Model.Materia();
                    Periodo periodo = new Periodo();
                    estudiante.id_estudiante = int.Parse(cmbEstudiante.SelectedValue.ToString());
                    materia.id_materia = _materia;
                    periodo.id_periodo = _periodo;


                    nota.id_estudiante = estudiante;
                    nota.id_materia = materia;
                    nota.actividad1 = decimal.Parse(txtAct1.Text);
                    nota.actividad2 = decimal.Parse(txtAct2.Text);
                    nota.actividad3 = decimal.Parse(txtAct3.Text);
                    nota.examen = decimal.Parse(txtExamen.Text);

                    nota.promedio = calcularPromedio(nota.actividad1, nota.actividad2, nota.actividad3, nota.examen);
                    nota.id_periodo = periodo;

                    DaoNotas daoNota = new DaoNotas();
                    daoNota.InsertarNota(nota);
                    tbData.Text = "La nota ha sido agregada ...";
                }
                else
                {
                    tbData.Text = "El rango de nota permitido debe de estar entre 0 & 10";
                }
            }
        }

        public void modificarNota()
        {
            if (cajasVaciasUpdate())
            {
                if (cantidadNota())
                {
                    Model.Nota nota = new Model.Nota();
                    Model.Estudiante estudiante = new Model.Estudiante();
                    Model.Materia materia = new Model.Materia();
                    Periodo periodo = new Periodo();
                    estudiante.id_estudiante = int.Parse(lblIdEstudiante.Content.ToString());
                    materia.id_materia = _materia;
                    periodo.id_periodo = _periodo;


                    nota.id_estudiante = estudiante;
                    nota.id_materia = materia;
                    nota.actividad1 = decimal.Parse(txtAct1.Text);
                    nota.actividad2 = decimal.Parse(txtAct2.Text);
                    nota.actividad3 = decimal.Parse(txtAct3.Text);
                    nota.examen = decimal.Parse(txtExamen.Text);
                    nota.id_nota = int.Parse(txtIdNota.Text);

                    nota.promedio = calcularPromedio(nota.actividad1, nota.actividad2, nota.actividad3, nota.examen);
                    nota.id_periodo = periodo;

                    DaoNotas daoNota = new DaoNotas();
                    daoNota.actualizarNota(nota);
                    tbData.Text = "La nota ha sido actualizada ...";

                }
                else
                {
                    tbData.Text = "El rango de nota permitido debe de estar entre 0 & 10";
                }
            }
        }

        public void editar()
        {
            btnGuardar.IsEnabled = true;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            btnNuevo.IsEnabled = false;
        }

        public void cargarFiltro()
        {
            cmbFiltro.Items.Add("ID Estudiante");
            cmbFiltro.Items.Add("Nombre");
            cmbFiltro.Items.Add("Apellido");
        }

        #endregion
        #region EVENTOS
        private void cmbMateria_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarComboGrado(cmbMateria.SelectedIndex + 1);
            return;
        }

        private void cmbGrado_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarComboPeriodo("dgDatos");
        }

        private void btnNuevo_Copy_Click(object sender, RoutedEventArgs e)
        {
            borderPrincipal.Visibility = Visibility.Visible;
            borderSecundario.Visibility = Visibility.Hidden;
        }

        private void btnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            if (validarCombo())
            {
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                limpiar();
                cargarDataGrid();

                borderPrincipal.Visibility = Visibility.Hidden;
                borderSecundario.Visibility = Visibility.Visible;

                lblGrado.Content = cmbGrado.Text;
                lblMateria.Content = cmbMateria.Text;
                lblPeriodo.Content = cmbPeriodo.Text;
                cargarDataGrid();
                limpiar();
            }
        }

        private void dgDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Model.Nota nota = (Model.Nota)dgDatos.SelectedItem;
            if (nota == null)
            {
                return;
            }

            txtIdNota.Text = nota.id_nota.ToString();
            txtAct1.Text = nota.actividad1.ToString();
            txtAct2.Text = nota.actividad2.ToString();
            txtAct3.Text = nota.actividad3.ToString();
            txtExamen.Text = nota.examen.ToString();
            lblNotaPromedio.Content = nota.promedio.ToString();
            lblIdEstudiante.Content = nota.id_estudiante.id_estudiante.ToString();

            if (nota.promedio >= 6)
            {
                lblEstado.Content = "APROBADO";
            }
            else
            {
                lblEstado.Content = "REPROBADO";
            }

        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            btnLimpiar.IsEnabled = true;
            agregando = true;
            editando = false;
            del = false;
            bloquearBotones();
            bloquearCajas(true);
            limpiar();
            cmbEstudiante.IsEnabled = true;
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (agregando)
            {
                if (cajasVaciasInsert())
                {

                    agregarNota();
                    editando = true;
                    agregando = false;
                    bloquearBotones();
                    bloquearCajas(false);
                    limpiar();
                    btnLimpiar.IsEnabled = false;
                    cargarDataGrid();
                }
                else
                {
                    return;
                }
            }
            else if (editando)
            {
                if (cajasVaciasUpdate() && !txtIdNota.Text.Equals(""))
                {
                    modificarNota();
                    editando = true;
                    agregando = false;
                    bloquearBotones();
                    bloquearCajas(false);
                    limpiar();
                    btnLimpiar.IsEnabled = false;
                    cargarDataGrid();

                }
                else
                {
                    return;
                }
            }
            else if (del)
            {

            }
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            agregando = true;
            editando = false;
            del = false;

            Model.Nota doc = (Model.Nota)dgDatos.SelectedItem;
            if (doc == null)
            {
                tbData.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }

            if (txtIdNota.Text.Equals(""))
            {
                tbData.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }
            else
            {
                tbData.Text = "¿Seguro que desea Modificar esa nota?";
                btnLimpiar.IsEnabled = true;
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                editando = true;
                agregando = false;
                del = false;
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            agregando = true;
            editando = false;
            del = false;
            Model.Nota doc = (Model.Nota)dgDatos.SelectedItem;
            if (doc == null)
            {
                tbData.Text = "Seleccione un registro para Eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                //bloquearBotones();
                return;
            }

            if (txtIdNota.Text.Equals(""))
            {
                tbData.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                //bloquearBotones();
                return;
            }
            else
            {
                tbData.Text = "¿Seguro que desea Eliminar esa nota?";
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                editando = false;
                agregando = false;
                del = true;
            }
        }

        private void btnAceptarHidden_Click(object sender, RoutedEventArgs e)
        {
            if (del)
            {
                //editar();
                bloquearCajas(true);
                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
                Model.Nota nota = new Model.Nota();
                DAO.DaoNotas daoNota = new DAO.DaoNotas();
                nota.id_nota = int.Parse(txtIdNota.Text);
                daoNota.eliminarNota(nota);
                cargarDataGrid();
                limpiar();

            }
            else if (editando)
            {
                editar();
                bloquearCajas(true);
                cmbEstudiante.IsEnabled = false;
                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnLimpiar.IsEnabled = false;
            btnCancelar.Visibility = Visibility.Hidden;
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptarHidden.Visibility = Visibility.Hidden;
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            DaoNotas daoNotas = new DaoNotas();
            if (!cmbFiltro.Text.Equals("") && !txtValor.Text.Equals(""))
            {
                if (cmbFiltro.Text.Equals("ID Estudiante"))
                {
                    dgDatos.ItemsSource = daoNotas.verNotasDocenteById(_periodo, _materia, _grado, 1, txtValor.Text);
                }
                else if (cmbFiltro.Text.Equals("Nombre"))
                {
                    dgDatos.ItemsSource = daoNotas.verNotasDocenteById(_periodo, _materia, _grado, 2, txtValor.Text);
                }
                else
                {
                    dgDatos.ItemsSource = daoNotas.verNotasDocenteById(_periodo, _materia, _grado, 3, txtValor.Text);
                }
            }
        }

        private void btnRecargar_Click(object sender, RoutedEventArgs e)
        {
            cargarDataGrid();
            cmbFiltro.SelectedIndex = -1;
            txtValor.Clear();
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnLimpiar.IsEnabled = false;
            agregando = false;
            editando = true;
            del = false;
            bloquearBotones();
            bloquearCajas(false);
        }

        #endregion
    }
}
