﻿using SistemaDeNotas.DAO;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SistemaDeNotas.Formularios.Docente
{
    /// <summary>
    /// Lógica de interacción para FrmDocente.xaml
    /// </summary>
    public partial class FrmDocente : UserControl
    {
        public FrmDocente()
        {
            InitializeComponent();
            mostrarDatagrid();
            bloquearCajas(false);
            bloquearBotones();
            btnLimpiar.IsEnabled = false;
        }

        bool editando = true;
        bool agregando = false;
        bool del = false;

        public void mostrarDatagrid()
        {
            DaoDocente doc = new DaoDocente();
            dgDatos.ItemsSource = doc.MostrarDocente();

        }

        
        #region
        public void bloquearCajas(bool valor)
        {
            txtID.IsEnabled = false;
            txtNombre.IsEnabled = valor;
            txtApellido.IsEnabled = valor;
            txtDireccion.IsEnabled = valor;
            rdFemenino.IsEnabled = valor;
            rdMasculino.IsEnabled = valor;
            txtDui.IsEnabled = valor;
            txtNit.IsEnabled = valor;
            txtCorreo.IsEnabled = valor;
            txtUsuario.IsEnabled = valor;
            txtTelefono.IsEnabled = valor;

        }

        public void bloquearBotones()
        {
            if (agregando)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }

            if (editando)
            {

                btnEliminar.IsEnabled = true;
                btnGuardar.IsEnabled = false;
                btnModificar.IsEnabled = true;
                btnNuevo.IsEnabled = true;
            }
            if (del)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }


        }

        public void limpiar()
        {
            txtID.Clear();
            txtNombre.Clear();
            txtApellido.Clear();
            txtDireccion.Clear();
            rdFemenino.IsChecked = false;
            rdMasculino.IsChecked = false;
            txtDui.Clear();
            txtNit.Clear();
            txtCorreo.Clear();
            txtUsuario.Clear();
            txtTelefono.Clear();

        }

        public bool cajasVacias()
        {
            if (txtNombre.Text.Equals("") || txtApellido.Text.Equals("") || txtDireccion.Text.Equals("") || !txtDui.IsMaskCompleted || !txtNit.IsMaskCompleted || txtCorreo.Text.Equals("")
                || txtUsuario.Text.Equals("") || !txtTelefono.IsMaskCompleted)
            {
                tbData.Text = "¡Las cajas no pueden ir vacias!\nPor favor valide cada una";
                return false;
            }
            if (rdFemenino.IsChecked == false && rdMasculino.IsChecked == false)
            {
                tbData.Text = "¡Debe seleccionar un genero!\nSeleccione uno y vuelva a intentarlo";
                return false;
            }


            return true;
        }

        public void agregarDocente()
        {
            try
            {
                Model.Docente docente = new Model.Docente();
                docente.nombre = txtNombre.Text;
                docente.apellido = txtApellido.Text;
                docente.direccion = txtDireccion.Text;
                docente.telefono = txtTelefono.Text;
                docente.dui = txtDui.Text;
                docente.nit = txtNit.Text;
                docente.correo = txtCorreo.Text;
                docente.usuario = txtUsuario.Text;

                if (rdFemenino.IsChecked == true)
                {
                    docente.genero = "Femenino";
                }
                if (rdMasculino.IsChecked == true)
                {
                    docente.genero = "Masculino";
                }
                DAO.DaoDocente daoDocente = new DAO.DaoDocente();
                List<Model.Docente> doc = new List<Model.Docente>();
                doc = daoDocente.buscarUsernameDocente(docente);
                Model.Docente docenteDatos = new Model.Docente();

                foreach (Model.Docente a in doc)
                {
                    docenteDatos.id_docente = a.id_docente;
                }

                if (docenteDatos.id_docente == 0)
                {
                    daoDocente.InsertarDocente(docente);
                    tbData.Text = "¡El registro se ha ingresado correctamente!";
                }
                else
                {
                    tbData.Text = "¡El usuario ya existe!, favor actualicelo";
                    return;
                }

            }
            catch (Exception error)
            {
                tbData.Text = "Ha ocurrido un error:\n" + error.Message;
            }

        }

        public void modificarDocente()
        {
            Model.Docente docente = new Model.Docente();
            docente.nombre = txtNombre.Text;
            docente.apellido = txtApellido.Text;
            docente.direccion = txtDireccion.Text;
            docente.telefono = txtTelefono.Text;
            docente.dui = txtDui.Text;
            docente.nit = txtNit.Text;
            docente.correo = txtCorreo.Text;
            docente.usuario = txtUsuario.Text;
            docente.id_docente = int.Parse(txtID.Text);

            if (rdFemenino.IsChecked == true)
            {
                docente.genero = "Femenino";
            }
            if (rdMasculino.IsChecked == true)
            {
                docente.genero = "Masculino";
            }

            DAO.DaoDocente daoDocente = new DAO.DaoDocente();
            daoDocente.actualizarDocente(docente);
            tbData.Text = "¡El registro se ha actualizado correctamente!";

        }

        public void editar()
        {
            btnGuardar.IsEnabled = true;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            btnNuevo.IsEnabled = false;
        }

        #endregion
        #region
        private void dgDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Model.Docente doc = (Model.Docente)dgDatos.SelectedItem;
            if (doc == null)
            {
                return;
            }

            txtID.Text = doc.id_docente.ToString();
            txtNombre.Text = doc.nombre;
            txtApellido.Text = doc.apellido;
            txtDireccion.Text = doc.direccion;
            txtDui.Text = doc.dui;
            txtNit.Text = doc.nit;
            txtCorreo.Text = doc.correo;
            txtUsuario.Text = doc.usuario;
            txtTelefono.Text = doc.telefono;

            if (doc.genero.Equals("Masculino"))
            {
                rdMasculino.IsChecked = true;
            }
            else
            {
                rdFemenino.IsChecked = true;
            }

        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            btnLimpiar.IsEnabled = true;
            agregando = true;
            editando = false;
            del = false;
            bloquearBotones();
            bloquearCajas(true);
            limpiar();

        }



        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {

            if (agregando)
            {
                if (cajasVacias())
                {

                    agregarDocente();
                    editando = true;
                    agregando = false;
                    bloquearBotones();
                    bloquearCajas(false);
                    limpiar();
                    btnLimpiar.IsEnabled = false;
                    mostrarDatagrid();
                }
                else
                {
                    return;
                }
            }
            else if (editando)
            {
                if (cajasVacias() && !txtID.Text.Equals(""))
                {
                    modificarDocente();
                    editando = true;
                    agregando = false;
                    bloquearBotones();
                    bloquearCajas(false);
                    limpiar();
                    btnLimpiar.IsEnabled = false;
                    mostrarDatagrid();

                }
                else
                {
                    return;
                }
            }
            else if (del)
            {

            }
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {

            agregando = true;
            editando = false;
            del = false;

            Model.Docente doc = (Model.Docente)dgDatos.SelectedItem;
            if (doc == null)
            {
                tbData.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }

            if (txtID.Text.Equals(""))
            {
                tbData.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }
            else
            {
                tbData.Text = "¿Seguro que desea modificar el registro?";
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                btnLimpiar.IsEnabled = true;
                editando = true;
                agregando = false;
                del = false;
            }

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            agregando = true;
            editando = false;
            del = false;
            Model.Docente doc = (Model.Docente)dgDatos.SelectedItem;
            if (doc == null)
            {
                tbData.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                //bloquearBotones();
                return;
            }

            if (txtID.Text.Equals(""))
            {
                tbData.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                //bloquearBotones();
                return;
            }
            else
            {
                tbData.Text = "¿Seguro que desea eliminar el registro?";
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                editando = false;
                agregando = false;
                del = true;
            }
        }


        private void btnAceptarHidden_Click(object sender, RoutedEventArgs e)
        {
            if (del)
            {
                //editar();
                bloquearCajas(true);
                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
                Model.Docente docente = new Model.Docente();
                DAO.DaoDocente daoDocente = new DAO.DaoDocente();
                docente.id_docente = int.Parse(txtID.Text);
                daoDocente.eliminarDocente(docente);
                mostrarDatagrid();
                limpiar();

            }
            else if (editando)
            {
                editar();
                bloquearCajas(true);
                txtUsuario.IsEnabled = false;
                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnLimpiar.IsEnabled = false;
            btnCancelar.Visibility = Visibility.Hidden;
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptarHidden.Visibility = Visibility.Hidden;
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnLimpiar.IsEnabled = false;
            agregando = false;
            editando = true;
            del = false;
            bloquearBotones();
            bloquearCajas(false);
        }
        #endregion
    }
}
