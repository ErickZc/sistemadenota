﻿using SistemaDeNotas.Formularios.Error;
using SistemaDeNotas.Formularios.Estudiante;
using SistemaDeNotas.Formularios.Inicio;

using SistemaDeNotas.Formularios.Nota;
using System.Windows;
using System.Windows.Controls;

using SistemaDeNotas.Formularios.Cambio_Contraseña_Docente;


namespace SistemaDeNotas.Formularios.Docente
{
    /// <summary>
    /// Lógica de interacción para FrmDashboardDocente.xaml
    /// </summary>
    public partial class FrmDashboardDocente : Window
    {
        public int id_docente = 0;
        public int idGrado = 0;
        public string username;
        public FrmDashboardDocente(int doc, int _idGrado, string _username)
        {
            InitializeComponent();
            id_docente = doc;
            idGrado = _idGrado;
            username = _username;
            validarGrado();
        }

        public void recargar()
        {
            ListViewItemMenu.SelectedIndex = -1;
            ListViewMenu3.SelectedIndex = -1;
            ListViewItemMenu5.SelectedIndex = -1;
            ListViewItemMenu4.SelectedIndex = -1;
        }

        #region

        internal void SwitchScreen(object sender)
        {
            var screen = ((UserControl)sender);

            if (screen != null)
            {
                StackPanelMain.Children.Clear();
                StackPanelMain.Children.Add(screen);
            }
        }

        public void mantenimiento()
        {
            StackPanelMain.Children.Clear();
            if (ListViewMenu3.SelectedIndex == 0)
            {
                FrmNota nota = new FrmNota(id_docente);
                StackPanelMain.Children.Add(nota);
            }
            else if (ListViewMenu3.SelectedIndex == 1)
            {
                if (idGrado != 0)
                {
                    FrmMatricularEstudiante matricula = new FrmMatricularEstudiante(idGrado);
                    StackPanelMain.Children.Add(matricula);
                }
                else
                {
                    frmError error = new frmError();
                    StackPanelMain.Children.Add(error);
                }
            }

        }

        public void validarGrado()
        {
            if (idGrado == 0)
            {
                MatricularAlumno.IsEnabled = false;

            }

        }

        #endregion

        private void ListViewItemMenu4_Selected(object sender, RoutedEventArgs e)
        {
            SistemaDeNotas.Formularios.Login.FrmLogin login = new SistemaDeNotas.Formularios.Login.FrmLogin();
            login.Show();
            this.Close();
        }

        private void ListViewItemMenu4_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SistemaDeNotas.Formularios.Login.FrmLogin login = new SistemaDeNotas.Formularios.Login.FrmLogin();
            login.Show();
            this.Close();
        }

        private void ListViewMenu3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            mantenimiento();
        }

        private void ListViewItemMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            recargar();
            StackPanelMain.Children.Clear();
            FrmInicio inicio = new FrmInicio();
            StackPanelMain.Children.Add(inicio);
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ListViewItemMenu5_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            StackPanelMain.Children.Clear();
            FrmCambioContraseñaDocente cdoc = new FrmCambioContraseñaDocente(id_docente,username);
            StackPanelMain.Children.Add(cdoc);
        }
    }
}
