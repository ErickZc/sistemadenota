﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SistemaDeNotas.Model;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using SistemaDeNotas.Formularios;


namespace SistemaDeNotas.Formularios.Login
{
    /// <summary>
    /// Lógica de interacción para FrmLogin.xaml
    /// </summary>
    public partial class FrmLogin : Window
    {
        public FrmLogin()
        {
            InitializeComponent();
            cargarDatos();
        }
        #region EVENTOS
        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void btnIngresar_Click(object sender, RoutedEventArgs e)
        {
            try {
                if (validaciones()) {
                    return;
                }
                if (cmbRol.Text.Equals("Administrador")) {
                    DAO.DaoAdminitrador admin = new DAO.DaoAdminitrador();
                    List<Administrador_Sistema> administrador = new List<Administrador_Sistema>();
                    Administrador_Sistema adm = new Administrador_Sistema();
                    administrador = admin.getAdministrador(txtUsuario.Text, txtContrasena.Password.ToString());

                    foreach (Administrador_Sistema a in administrador)
                    {
                        adm.id_admin = a.id_admin;
                        adm.nombre = a.nombre;
                        adm.apellido = a.apellido;
                        adm.username = a.username;
                        adm.telefono = a.telefono;
                        adm.correo = a.correo;
                        adm.dui = a.dui;
                        adm.estado = a.estado;
                    }

                    if (adm.id_admin != 0)
                    {
                        tbData.Text = "Bien!";
                        DropDownMenu.MainWindow dashboard = new DropDownMenu.MainWindow();
                        dashboard.Show();
                        this.Close();
                        
                    }
                    else
                    {
                        tbData.Text = "Usuario y/o credenciales incorrectas!";
                    }

                }
                else
                {
                    // Docente

                    DAO.DaoDocente docenteDao = new DAO.DaoDocente();
                    DAO.DaoGradoDocente gradoDocenteDao = new DAO.DaoGradoDocente();
                    List<Model.Docente> docente = new List<Model.Docente>();
                    List<Model.Docente_Grado> enc = new List<Model.Docente_Grado>();
                    List<Model.Docente_Materia_Grado> docenteMateriaGrado = new List<Model.Docente_Materia_Grado>();
                    Model.Docente doc = new Model.Docente();
                    Model.Docente_Grado encargado = new Docente_Grado();
                    Model.Docente_Materia_Grado dmg = new Docente_Materia_Grado();
                    DAO.DaoDocenteMG dmgDao = new DAO.DaoDocenteMG();

                    docente = docenteDao.getDocenteLogin(txtUsuario.Text, txtContrasena.Password.ToString());
                    foreach (Model.Docente a in docente)
                    {
                        doc.id_docente = a.id_docente;
                        doc.nombre = a.nombre;
                        doc.apellido = a.apellido;
                        doc.direccion = a.direccion;
                        doc.telefono = a.telefono;
                        doc.dui = a.dui;
                        doc.nit= a.nit;
                        doc.correo = a.correo;
                        doc.fecha_creacion = a.fecha_creacion;
                        doc.estado = a.estado;
                        doc.usuario = a.usuario;
                        doc.password = a.password;
                        doc.genero = a.genero;
                        //doc.id_rol.id_rol = a.id_rol.id_rol;
                    }
                    enc = gradoDocenteDao.MostrarGradoDocenteByID(doc.id_docente);

                    foreach (Docente_Grado i in enc)
                    {
                        Model.Grado gra = new Model.Grado();
                        encargado.idDocente_Grado = i.idDocente_Grado;
                        encargado.id_docente = i.id_docente;
                        gra.id_grado = i.id_grado.id_grado;
                        encargado.id_grado = gra;
                        encargado.fecha = i.fecha;
                        encargado.estado = i.estado;
                    }

                    docenteMateriaGrado = dmgDao.MostrarDocenteMateriaByID(doc.id_docente);

                    if (doc.id_docente != 0) {
                        //string variable = "Docente registrado!";
                        
                        if (encargado.idDocente_Grado != 0)
                        {
                            //variable = variable + "\nEl docente es coordinador de un grado";
                            Docente.FrmDashboardDocente dashDoc = new Docente.FrmDashboardDocente(doc.id_docente,encargado.id_grado.id_grado,doc.usuario);
                            dashDoc.Show();
                            this.Close();
                        }
                        else
                        {
                            //variable = variable + "\nPero no posee un grado encargado";
                            Docente.FrmDashboardDocente dashDoc = new Docente.FrmDashboardDocente(doc.id_docente,0,doc.usuario);
                            dashDoc.Show();
                            this.Close();
                        }
                        //tbData.Text = variable;
                    }
                    else
                    {
                        tbData.Text = "Usuario y/o credenciales incorrectas!";
                    }

                }
            }
            catch (Exception error)
            {
                //MessageBox.Show(error.Message);
                
            }
        }
        #endregion

        public bool validaciones()
        {
            string variable = "";
            if (txtUsuario.Text == "")
            {  
                variable = variable + "Debe ingresar su usuario\n";
            }
            if (txtContrasena.Equals(""))
            {
                variable = variable + "Debe ingresar su contraseña\n";
            }
            if (cmbRol.Text.Equals(""))
            {
                variable = variable + "Debe seleccionar un rol\n";
            }

            tbData.Text = variable;

            if (txtUsuario.Text == "")
                return true;
            else if (txtContrasena.Equals(""))
                return true;
            else if (cmbRol.Text.Equals(""))
                return true;

            return false;
            
        }

        public void cargarDatos()
        {
            cmbRol.Items.Add("Administrador");
            cmbRol.Items.Add("Docente");
        }

        public void limpiarCampos()
        {
            txtUsuario.Text = "";
            txtContrasena.Clear();
            cmbRol.SelectedIndex = 0;
        }

    }
}
