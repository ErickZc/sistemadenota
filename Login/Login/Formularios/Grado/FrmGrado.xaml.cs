﻿using SistemaDeNotas.DAO;
using SistemaDeNotas.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SistemaDeNotas.Formularios.Grado
{
    /// <summary>
    /// Lógica de interacción para UserControl1.xaml
    /// </summary>
    public partial class FrmGrado : UserControl
    {

        int id_seccion = 0;

        public FrmGrado()
        {
            InitializeComponent();
            mostrarDatagrid();
            bloquearCajas(false);
            bloquearBotones();
            cargarComboSeccion();
            btnLimpiar.IsEnabled = false;

        }

        bool editando = true;
        bool agregando = false;
        bool del = false;

        #region 
        public void mostrarDatagrid()
        {
            DaoGrado mat = new DaoGrado();
            dgDatos.ItemsSource = mat.MostrarGrados();

        }

        

        public void bloquearCajas(bool valor)
        {
            txtIDGrado.IsEnabled = false;
            txtGrado.IsEnabled = valor;
            txtCantidadAlumnos.IsEnabled = valor;
            cmbSeccion.IsEnabled = valor;

        }

        public void bloquearBotones()
        {
            if (agregando)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }

            if (editando)
            {

                btnEliminar.IsEnabled = true;
                btnGuardar.IsEnabled = false;
                btnModificar.IsEnabled = true;
                btnNuevo.IsEnabled = true;
            }
            if (del)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }


        }
        public void cargarComboSeccion()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            DaoGrado daoGrado = new DaoGrado();
            List<Model.Seccion> listadoSeccion = new List<Model.Seccion>();


            cmbSeccion.ItemsSource = null;
            cmbSeccion.Items.Clear();


            try
            {
                listadoSeccion = daoGrado.MostrarSeccionByIdgrado();


                foreach (var p in listadoSeccion)
                {
                    lista.Add(p.id_seccion, p.seccion);
                }

                cmbSeccion.ItemsSource = lista;
                cmbSeccion.DisplayMemberPath = "Value";
                cmbSeccion.SelectedValuePath = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }

        public void limpiar()
        {
            txtIDGrado.Clear();
            txtGrado.Clear();
            txtCantidadAlumnos.Clear();
            cmbSeccion.SelectedIndex = -1;
        }

        public bool cajasVacias()
        {
            if (txtGrado.Text.Equals("") || txtCantidadAlumnos.Text.Equals(""))
            {
                tbDataGrado.Text = "¡Las cajas no pueden ir vacias!\nPor favor valide cada una";
                return false;
            }



            return true;
        }


        public void agregarGrado()
        {
            try
            {
                Model.Grado grado = new Model.Grado();
                Model.Seccion seccion = new Model.Seccion();
                grado.grado = txtGrado.Text;

                seccion.id_seccion = int.Parse(cmbSeccion.SelectedValue.ToString());

                grado.id_seccion = seccion;
                grado.cantidad_alumno = int.Parse(txtCantidadAlumnos.Text);

                DAO.DaoGrado daoGrado = new DAO.DaoGrado();
                List<Model.Grado> doc = new List<Model.Grado>();
                doc = daoGrado.buscarGrado(grado);
                Model.Grado gradoDatos = new Model.Grado();

                foreach (Model.Grado a in doc)
                {
                    gradoDatos.id_grado = a.id_grado;
                }

                if (gradoDatos.id_grado == 0)
                {
                    daoGrado.InsertarGrado(grado);
                    tbDataGrado.Text = "¡El registro se ha ingresado correctamente!.";
                }
                else
                {
                    tbDataGrado.Text = "¡El Grado ya existe!, favor actualicelo";
                    return;
                }

            }
            catch (Exception error)
            {
                tbDataGrado.Text = "Ha ocurrido un error:\n" + error.Message;
            }

        }

        public void modificarGrado()
        {
            Model.Grado grado = new Model.Grado();
            Model.Seccion seccion = new Model.Seccion();
            grado.id_grado = int.Parse(txtIDGrado.Text);
            grado.grado = txtGrado.Text;
            grado.cantidad_alumno = int.Parse(txtCantidadAlumnos.Text);
            seccion.seccion = cmbSeccion.ToString();
            seccion.id_seccion = int.Parse(cmbSeccion.SelectedValue.ToString());
            grado.id_seccion = seccion;
            DAO.DaoGrado daoGrado = new DAO.DaoGrado();
            daoGrado.actualizarGrado(grado);
            tbDataGrado.Text = "¡El registro se ha actualizado correctamente!";

        }

        public void editar()
        {
            btnGuardar.IsEnabled = true;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            btnNuevo.IsEnabled = false;
        }
        #endregion

        #region EVENTOS
        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            btnLimpiar.IsEnabled = true;
            agregando = true;
            editando = false;
            del = false;
            bloquearBotones();
            bloquearCajas(true);
            limpiar();
            cmbSeccion.SelectedIndex = -1;
        }



        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {

            if (agregando)
            {
                if (cajasVacias())
                {
                    if (cantidad() > 0)
                    {
                        agregarGrado();
                        editando = true;
                        agregando = false;
                        bloquearBotones();
                        bloquearCajas(false);
                        limpiar();
                        btnLimpiar.IsEnabled = false;
                        mostrarDatagrid();
                    }
                    else
                    {
                        tbDataGrado.Text = "La cantidad de estudiantes debe de ser mayor a cero";
                    }
                }
                else
                {
                    tbDataGrado.Text = "Seleccione un registro para empezar con los cambios";
                    return;
                }
            }
            else if (editando)
            {
                if (cajasVacias() && !txtGrado.Text.Equals(""))
                {
                    if (cantidad() > 0)
                    {
                        modificarGrado();
                        editando = true;
                        agregando = false;
                        bloquearBotones();
                        bloquearCajas(false);
                        limpiar();
                        btnLimpiar.IsEnabled = false;
                        mostrarDatagrid();
                    }
                    else
                    {
                        tbDataGrado.Text = "La cantidad de estudiantes debe de ser mayor a cero";
                    }

                }
                else
                {
                    
                    return;
                }
            }
            else if (del)
            {

            }
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {

            agregando = true;
            editando = false;
            del = false;

            Model.Grado doc = (Model.Grado)dgDatos.SelectedItem;
            if (doc == null)
            {
                tbDataGrado.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }

            if (txtGrado.Text.Equals(""))
            {
                tbDataGrado.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }
            else
            {
                tbDataGrado.Text = "¿Seguro que desea modificar el registro?";
                btnLimpiar.IsEnabled = true;
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                editando = true;
                agregando = false;
                del = false;
            }

        }

        public int cantidad()
        {
            if (int.Parse(txtCantidadAlumnos.Text) > 0)
            {
                return 1;
            }

            return 0;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            agregando = true;
            editando = false;
            del = false;
            Model.Grado doc = (Model.Grado)dgDatos.SelectedItem;
            if (doc == null)
            {
                tbDataGrado.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                //bloquearBotones();
                return;
            }

            if (txtGrado.Text.Equals(""))
            {
                tbDataGrado.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                return;
            }
            else
            {
                tbDataGrado.Text = "¿Seguro que desea eliminar el registro?";
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                editando = false;
                agregando = false;
                del = true;
            }
        }


        private void btnAceptarHidden_Click(object sender, RoutedEventArgs e)
        {
            if (del)
            {

                bloquearCajas(true);
                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
                Model.Grado grado = new Model.Grado();
                DAO.DaoGrado daoGrado = new DAO.DaoGrado();
                grado.id_grado = int.Parse(txtIDGrado.Text);
                daoGrado.eliminarGrado(grado);
                mostrarDatagrid();
                limpiar();
                bloquearCajas(false);

            }
            else if (editando)
            {
                editar();
                bloquearCajas(true);
                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnLimpiar.IsEnabled = false;
            btnCancelar.Visibility = Visibility.Hidden;
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptarHidden.Visibility = Visibility.Hidden;
        }
        private void dgDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Model.Grado doc = (Model.Grado)dgDatos.SelectedItem;
            if (doc == null)
            {
                return;
            }

            txtIDGrado.Text = doc.id_grado.ToString();
            txtGrado.Text = doc.grado.ToString();
            txtCantidadAlumnos.Text = doc.cantidad_alumno.ToString();
            cmbSeccion.SelectedValue = doc.id_seccion.id_seccion;
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnLimpiar.IsEnabled = false;
            agregando = false;
            editando = true;
            del = false;
            bloquearBotones();
            bloquearCajas(false);
        }

        #endregion

        private void txtCantidadAlumnos_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            int ascci = Convert.ToInt32(Convert.ToChar(e.Text));

            if (ascci >= 48 && ascci <= 57) e.Handled = false;

            else e.Handled = true;
        }


    }
}