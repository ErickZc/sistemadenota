﻿using SistemaDeNotas.Formularios.Docente;
using SistemaDeNotas.Formularios.Estudiante;
using SistemaDeNotas.Formularios.Inicio;
using SistemaDeNotas.Formularios.Materia;
using SistemaDeNotas.Formularios.Reportes;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SistemaDeNotas.Formularios.DocenteMateria;
using SistemaDeNotas.Formularios.Grado;
using SistemaDeNotas.Formularios.Login;
using SistemaDeNotas.Formularios.AdministradorSistema;
using SistemaDeNotas.Formularios.CoordinadorGrado;
using SistemaDeNotas.Formularios.Cambio_Contraseña;


namespace DropDownMenu
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        int doc = 0;
        int adm = 0;
        int rep = 0;

        #region

        public void recargar()
        {
            ListViewItemMenu.SelectedIndex = -1;
            ListViewMenu2.SelectedIndex = -1;
            ListViewMenu3.SelectedIndex = -1;
            ListViewMenu5.SelectedIndex = -1;
            ListViewItemMenu6.SelectedIndex = -1;
            ListViewItemMenu7.SelectedIndex = -1;
        }


        public void recargaradmin()
        {
            if (ListViewMenu2.SelectedIndex != -1)
            {
                ListViewItemMenu.SelectedIndex = -1;
                ListViewMenu3.SelectedIndex = -1;
                ListViewMenu5.SelectedIndex = -1;
                ListViewItemMenu6.SelectedIndex = -1;
                ListViewItemMenu7.SelectedIndex = -1;
                administracion();
            }
        }

        public void recargarDocente()
        {
            
            if (ListViewMenu3.SelectedIndex != -1)
            {
                ListViewItemMenu.SelectedIndex = -1;
                ListViewMenu2.SelectedIndex = -1;
                ListViewMenu5.SelectedIndex = -1;
                ListViewItemMenu6.SelectedIndex = -1;
                ListViewItemMenu7.SelectedIndex = -1;
                docentes();
            }
        }

        public void recargarReporte()
        {
            if (ListViewMenu5.SelectedIndex != -1)
            {
                ListViewItemMenu.SelectedIndex = -1;
                ListViewMenu2.SelectedIndex = -1;
                ListViewMenu3.SelectedIndex = -1;
                ListViewItemMenu6.SelectedIndex = -1;
                ListViewItemMenu7.SelectedIndex = -1;
                reportes();
            }
        }

        public void administracion()
        {
            StackPanelMain.Children.Clear();
            
            if (ListViewMenu2.SelectedIndex == 0)
            {
                FrmDocente docente = new FrmDocente();
                StackPanelMain.Children.Add(docente);

            }
            else if (ListViewMenu2.SelectedIndex == 1)
            {
                FrmMateria materia = new FrmMateria();
                StackPanelMain.Children.Add(materia);

            }
            else if (ListViewMenu2.SelectedIndex == 2)
            {
                FrmGrado grado = new FrmGrado();
                StackPanelMain.Children.Add(grado);

            }
            else if (ListViewMenu2.SelectedIndex == 3)
            {
                FrmEstudiantes estudiante = new FrmEstudiantes();
                StackPanelMain.Children.Add(estudiante);

            }
            else if (ListViewMenu2.SelectedIndex == 4)
            {
                FrmAdministrador_Sistema admin = new FrmAdministrador_Sistema();
                StackPanelMain.Children.Add(admin);
            }
            //recargaradmin();
        }

        public void docentes()
        {
            StackPanelMain.Children.Clear();
            
            if (ListViewMenu3.SelectedIndex == 0)
            {
                FrmCoordinadorGrado coordinador = new FrmCoordinadorGrado();
                StackPanelMain.Children.Add(coordinador);
            }
            else if (ListViewMenu3.SelectedIndex == 1)
            {
                 SistemaDeNotas.Formularios.DocenteMateria.AgregarMateriaAdocente mg = new SistemaDeNotas.Formularios.DocenteMateria.AgregarMateriaAdocente();
                StackPanelMain.Children.Add(mg);
            }
            //recargarDocente();
        }

        public void reportes()
        {

            StackPanelMain.Children.Clear();
            
            if (ListViewMenu5.SelectedIndex ==0)
            {
                FrmReporteNotas nota = new FrmReporteNotas();
                StackPanelMain.Children.Add(nota);
            }
            else if (ListViewMenu5.SelectedIndex ==1)
            {
                FrmReporteEstudiantesMatriculados alumnos = new FrmReporteEstudiantesMatriculados();
                StackPanelMain.Children.Add(alumnos);
            }
            else if (ListViewMenu5.SelectedIndex == 2)
            {
                FrmReporteBoleta boleta = new FrmReporteBoleta();
                StackPanelMain.Children.Add(boleta);
            }
            else if (ListViewMenu5.SelectedIndex == 3)
            {
                FrmReporteDocente docente = new FrmReporteDocente();
                StackPanelMain.Children.Add(docente);
            }
            else if (ListViewMenu5.SelectedIndex == 4)
            {
                FrmReporteDocentesxMateria dxm = new FrmReporteDocentesxMateria();
                StackPanelMain.Children.Add(dxm);
            }
            else if (ListViewMenu5.SelectedIndex == 5)
            {
                FrmReporteGradoxDocente gxd = new FrmReporteGradoxDocente();
                StackPanelMain.Children.Add(gxd);
            }
            //recargarReporte();
        }

        internal void SwitchScreen(object sender)
        {
            var screen = ((UserControl)sender);

            if (screen != null)
            {
                StackPanelMain.Children.Clear();
                StackPanelMain.Children.Add(screen);
            }
        }

        #endregion


        #region EVENTOS 
        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            SistemaDeNotas.Formularios.Login.FrmLogin login = new SistemaDeNotas.Formularios.Login.FrmLogin();
            login.Show();
            this.Close();
        }

        private void ListViewMenu2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            adm = 1;
            rep = 0;
            doc = 0;
            recargaradmin();
        }

        private void AdminDocente_Selected(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("");
        }

        private void ListViewMenu3_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void AdminDocente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void AdminDocente_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            //instAdministracion();
        }

        private void AdminEstudiante_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //instAdministracion();
        }

        private void ListViewItemMenu_Selected(object sender, RoutedEventArgs e)
        {
            recargar();
            StackPanelMain.Children.Clear();
            FrmInicio inicio = new FrmInicio();
            StackPanelMain.Children.Add(inicio);
            
        }

        private void ListViewItemMenu7_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FrmLogin login = new FrmLogin();
            login.Show();
            this.Close();
        }

        private void ListViewItemMenu6_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            StackPanelMain.Children.Clear();
            FrmCambioContraseña change = new FrmCambioContraseña();
            StackPanelMain.Children.Add(change);
        }

        private void ListViewMenu3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            doc = 1;
            rep = 0;
            adm = 0;
            recargarDocente();
        }
        #endregion

        private void ListViewMenu5_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            rep = 1;
            doc = 0;
            adm = 0;
            recargarReporte();
        }

        private void ExpanderMenu2_Collapsed(object sender, RoutedEventArgs e)
        {
            
        }

        private void ExpanderMenu3_Collapsed(object sender, RoutedEventArgs e)
        {
            
        }

        private void ExpanderMenu5_Collapsed(object sender, RoutedEventArgs e)
        {
            
        }

        private void ExpanderMenu2_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            //recargaradmin();
        }

        private void ExpanderMenu3_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            //recargarDocente();
        }

        private void ExpanderMenu5_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            
        }

        private void ExpanderMenu5_Expanded(object sender, RoutedEventArgs e)
        {
            //recargarReporte();
        }

        private void ExpanderMenu2_Collapsed_1(object sender, RoutedEventArgs e)
        {
        }

        private void ExpanderMenu3_Collapsed_1(object sender, RoutedEventArgs e)
        {
        }
    }
}
