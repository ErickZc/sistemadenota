﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SistemaDeNotas.DAO;
using SistemaDeNotas.Model;

namespace SistemaDeNotas.Formularios.AdministradorSistema
{
    /// <summary>
    /// Lógica de interacción para UserControl2.xaml
    /// </summary>
    public partial class FrmAdministrador_Sistema : UserControl
    {
        public FrmAdministrador_Sistema()
        {
            InitializeComponent();
            MostrarDataGrid();
            bloquearBotones();
            bloquearCajas(false);
            btnLimpiar.IsEnabled = false;
        }
        
        bool editando = true;
        bool agregando = false;
        bool del = false;

        #region
        public void bloquearCajas(bool valor)
        {
            txtId.IsEnabled = false;
            txtNombres.IsEnabled = valor;
            txtApellido.IsEnabled = valor;
            txtCorreo.IsEnabled = valor;            
            txtTelefono.IsEnabled = valor;
            txtDui.IsEnabled = valor;
            txtUsuario.IsEnabled = valor;

        }

        public void bloquearBotones()
        {
            if (agregando)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }

            if (editando)
            {

                btnEliminar.IsEnabled = true;
                btnGuardar.IsEnabled = false;
                btnModificar.IsEnabled = true;
                btnNuevo.IsEnabled = true;
            }
            if (del)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }


        }
        public void Limpiar()
        {
            txtId.Clear();
            txtNombres.Clear();
            txtApellido.Clear();
            
            txtTelefono.Clear();
            txtUsuario.Clear();
            txtDui.Clear();
            
            txtCorreo.Clear();
        }
        public bool cajasVacias()
        {
            if (txtNombres.Text.Equals("") || txtApellido.Text.Equals("") || txtCorreo.Text.Equals("") || !txtDui.IsMaskCompleted || txtTelefono.Text.Equals("") || txtUsuario.Text.Equals("") || !txtTelefono.IsMaskCompleted)
            {
                tbData.Text = "¡Las cajas no pueden ir vacias!\nPor favor valide cada una";
                return false;
            }


            return true;
        }
        public void MostrarDataGrid()
        {
            DaoAdminitrador admin = new DaoAdminitrador();
            Administrador_Sistema _Sistema = new Administrador_Sistema();
            dgDatos.ItemsSource = admin.MostrarAdmin();
        }

        DaoEncrypt en = new DaoEncrypt();
        public void AgregarAdministrador()

        {
            try
            {
                DaoEncrypt encrypt = new DaoEncrypt();
                Administrador_Sistema admin = new Administrador_Sistema();
                admin.nombre = txtNombres.Text;
                admin.apellido = txtApellido.Text;
              
                admin.username = txtUsuario.Text;
                admin.password = encrypt.Encriptar(txtpassword.Password.ToString());
                admin.telefono = txtTelefono.Text;
                admin.correo = txtCorreo.Text;
                admin.dui = txtDui.Text;


                DAO.DaoAdminitrador daoAdminitrador = new DAO.DaoAdminitrador();
                List<Model.Administrador_Sistema> response = new List<Model.Administrador_Sistema>();
                response = daoAdminitrador.buscarUsernameAdmin(admin);
                Model.Administrador_Sistema AdministradorSistema = new Model.Administrador_Sistema();


                foreach (Model.Administrador_Sistema a in response)
                {
                    AdministradorSistema.id_admin = a.id_admin;
                }

                if (AdministradorSistema.id_admin == 0)
                {
                    daoAdminitrador.InsertarAdmin(admin);
                    tbData.Text = "¡El registro se ha ingresado correctamente!";
                }
                else
                {
                    tbData.Text = "¡los datos ya existen!, favor actualicelo";
                    return;
                }


            }
            catch (Exception error)
            {
                tbData.Text = "Ha ocurrido un error:\n" + error.Message;
            }

        }
        public void editar()
        {
            btnGuardar.IsEnabled = true;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            btnNuevo.IsEnabled = false;
        }
        public void ModificarAd()
        {
            Model.Administrador_Sistema administrador_Sistema = new Administrador_Sistema();
            administrador_Sistema.nombre = txtNombres.Text;
            administrador_Sistema.apellido = txtApellido.Text;
            administrador_Sistema.username = txtUsuario.Text;
            
            administrador_Sistema.telefono = txtTelefono.Text;
            administrador_Sistema.correo = txtCorreo.Text;
            administrador_Sistema.dui = txtDui.Text;
            
            administrador_Sistema.id_admin = int.Parse(txtId.Text);

            DAO.DaoAdminitrador daoAdminitrador = new DaoAdminitrador();
            var response = daoAdminitrador.ActualizarAdmin(administrador_Sistema);

            if (response != 0)
            {
                tbData.Text = "¡El registro se ha actualizado correctamente!";

            }
            else
            {
                tbData.Text = "¡Ha ocurrido un error en el ingreso!";
            }



        }

        public void eliminarAdministrador()
        {
            Model.Administrador_Sistema adminsistema = new Model.Administrador_Sistema ();
            DaoAdminitrador daoAdminitrador = new DaoAdminitrador();

            adminsistema.id_admin = int.Parse(txtId.Text);
            daoAdminitrador.EliminarAdmin(adminsistema);
        }
        #endregion
        
        #region
        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
           
        }
        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {

            agregando = true;
            editando = false;
            del = false;

            Model.Administrador_Sistema administrador = new Administrador_Sistema();
            if (administrador == null)
            {
                tbData.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }

            if (txtId.Text.Equals(""))
            {
                tbData.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }
            else
            {
                tbData.Text = "¿Seguro que desea modificar el registro?";
                editar();
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                btnLimpiar.IsEnabled = true;
                bloquearCajas(true);
                editando = true;
                agregando = false;
                del = false;

            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {

            if (agregando)
            {
                if (cajasVacias())
                {
                    AgregarAdministrador();
                    editando = true;
                    agregando = false;
                    bloquearBotones();
                    bloquearCajas(false);
                    Limpiar();
                    btnLimpiar.IsEnabled = false;
                    MostrarDataGrid();
                }
                else
                {
                    return;
                }
            }
            else if (editando)
            {
                if (cajasVacias() && !txtId.Text.Equals(""))
                {
                    ModificarAd();
                    editando = true;
                    agregando = false;
                    bloquearBotones();
                    bloquearCajas(false);
                    Limpiar();
                    btnLimpiar.IsEnabled = false;
                    MostrarDataGrid();

                }
                else
                {
                    return;
                }
            }
            else if (del)
            {

            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {

            agregando = true;
            editando = false;
            del = false;
            Model.Administrador_Sistema administrador_Sistema = (Model.Administrador_Sistema)dgDatos.SelectedItem;
            if (administrador_Sistema == null)
            {
                tbData.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                //bloquearBotones();
                return;
            }

            if (txtId.Text.Equals(""))
            {
                tbData.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                //bloquearBotones();
                return;
            }
            else
            {
                tbData.Text = "¿Seguro que desea eliminar el registro?";
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                editando = false;
                agregando = false;
                del = true;
            }
        }
        private void btnAceptarHidden_Click(object sender, RoutedEventArgs e)
        {
            if (del)
            {
             
                bloquearCajas(true);
                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
                eliminarAdministrador();              
                MostrarDataGrid();
                Limpiar();

            }
            else if (editando)
            {
                editar();
                bloquearCajas(true);
                txtUsuario.IsEnabled = false;
                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Limpiar();
            btnLimpiar.IsEnabled = false;
            agregando = false;
            editando = true;
            del = false;
            bloquearBotones();
            bloquearCajas(false);
            btnCancelar.Visibility = Visibility.Hidden;
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptarHidden.Visibility = Visibility.Hidden;
        }

        private void dgDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            Model.Administrador_Sistema administrador_Sistema = (Model.Administrador_Sistema)dgDatos.SelectedItem;
            if (administrador_Sistema == null)
            {
                return;
            }

            txtId.Text = administrador_Sistema.id_admin.ToString();
            txtNombres.Text = administrador_Sistema.nombre;
            txtApellido.Text = administrador_Sistema.apellido;
            txtUsuario.Text = administrador_Sistema.username;
           
            txtTelefono.Text = administrador_Sistema.telefono;
            txtCorreo.Text = administrador_Sistema.correo;
            txtDui.Text = administrador_Sistema.dui;
            
        }

        private void btnNuevo_Click_1(object sender, RoutedEventArgs e)
        {
            btnLimpiar.IsEnabled = true;
            agregando = true;
            editando = false;
            del = false;
            bloquearBotones();
            bloquearCajas(true);
            Limpiar();
        }
#endregion EVENTOS

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            Limpiar();
            btnLimpiar.IsEnabled = false;
            agregando = false;
            editando = true;
            del = false;
            bloquearBotones();
            bloquearCajas(false);
        }
    }
}
