﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SistemaDeNotas.DAO;
using SistemaDeNotas.Model;

namespace SistemaDeNotas.Formularios.Cambio_Contraseña
{
    /// <summary>
    /// Lógica de interacción para UserControl1.xaml
    /// </summary>
    public partial class FrmCambioContraseña : UserControl
    {
        public FrmCambioContraseña()
        {
            InitializeComponent();
            cargarComboRol();
        }


        public void cargarComboRol()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            DaoRol daoRol = new DaoRol();
            List<Model.Rol_Usuario> listadoRol = new List<Model.Rol_Usuario>();


            cmbRol.ItemsSource = null;
            cmbRol.Items.Clear();


            try
            {
                listadoRol = daoRol.MostrarRol();


                foreach (var p in listadoRol)
                {
                    lista.Add(p.id_rol, p.rol);
                }

                cmbRol.ItemsSource = lista;
                cmbRol.DisplayMemberPath = "Value";
                cmbRol.SelectedValuePath = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }

        public void limpiar()
        {
            txtConfirmarContra.Clear();
            txtContraseña.Clear();
            txtUsuario.Clear();
            cmbRol.SelectedIndex = -1;
        }

        public void modificarAdmin()
        {
            Model.Administrador_Sistema admin = new Model.Administrador_Sistema();
            Model.Rol_Usuario rol = new Model.Rol_Usuario();
            DaoEncrypt encry = new DaoEncrypt();

            rol.id_rol = int.Parse(cmbRol.SelectedValue.ToString());
            admin.username = txtUsuario.Text;           
            admin.password = encry.Encriptar(txtContraseña.Password.ToString());

            DAO.DaoAdminitrador daoAdmin = new DAO.DaoAdminitrador();
            daoAdmin.actualizarContraAdmin(admin);
            tbData.Text = "Los datos fueron actalizados";

        }

        public void modificarDocente()
        {
            Model.Docente docente = new Model.Docente();

            docente.usuario = txtUsuario.Text;
            docente.password = txtContraseña.Password.ToString();

            DAO.DaoDocente daoDocente = new DAO.DaoDocente();
            daoDocente.actualizarPassword(docente);
            tbData.Text = "Los datos fueron actalizados";

        }


        #region
        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            if(txtContraseña.Password.ToString() == txtConfirmarContra.Password.ToString())
            {
                if (!cmbRol.Text.Equals(""))
                {
                    if (int.Parse(cmbRol.SelectedValue.ToString()) == 1)
                    {
                        DaoAdminitrador daoAdm = new DaoAdminitrador();
                        Model.Administrador_Sistema admin = new Model.Administrador_Sistema();

                        admin.username = txtUsuario.Text;

                        List<Model.Administrador_Sistema> list = daoAdm.buscarUsernameAdmin(admin);

                        if (list.Count() > 0)
                        {
                            if (!txtConfirmarContra.Password.Equals("") && !txtContraseña.Password.Equals(""))
                            {
                                modificarAdmin();
                                limpiar();
                            }
                            else
                            {
                                tbData.Text = "Los campos no pueden ir vacios!";
                            }
                        }
                        else
                        {
                            tbData.Text = "El usuario seleccionado no existe";
                            limpiar();
                        }
                    }
                    else
                    {
                        DaoDocente daoDocente = new DaoDocente();
                        Model.Docente docente = new Model.Docente();

                        docente.usuario = txtUsuario.Text;
                        List<Model.Docente> list = daoDocente.buscarUsernameDocente(docente);

                        if (list.Count() > 0)
                        {
                            if (!txtConfirmarContra.Password.Equals("") && !txtContraseña.Password.Equals(""))
                            {
                                modificarDocente();
                                limpiar();
                            }
                            else
                            {
                                tbData.Text = "Los campos no pueden ir vacios!";
                            }
                        }
                        else
                        {
                            tbData.Text = "El usuario seleccionado no existe";
                            limpiar();
                        }

                    }
                }
                else
                {
                    tbData.Text = "Debe de seleccionar un rol!";
                }
            }
            else
            {
                tbData.Text = "La contraseña no coincide!";

            }
            

        }

        private void btnAceptarHidden_Click(object sender, RoutedEventArgs e)
        {


                
                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
                limpiar();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnCancelar.Visibility = Visibility.Hidden;
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptarHidden.Visibility = Visibility.Hidden;
        }
        #endregion EVENTOS

    }
}
