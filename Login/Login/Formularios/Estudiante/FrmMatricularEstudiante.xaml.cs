﻿using SistemaDeNotas.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SistemaDeNotas.Formularios.Estudiante
{
    /// <summary>
    /// Lógica de interacción para FrmMatricularEstudiante.xaml
    /// </summary>
    public partial class FrmMatricularEstudiante : UserControl
    {

        bool editando = true;
        bool agregando = false;
        bool del = false;
        int idGrado = 0;

        public FrmMatricularEstudiante(int _idGrado)
        {
            InitializeComponent();
            idGrado = _idGrado;
            mostrarDatagrid();
            cargarCombo();
            bloquearCajas(false);
            bloquearBotones();
            btnLimpiar.IsEnabled = false;

        }

        #region METODOS

        public void mostrarDatagrid()
        {
            DaoEstudiante estudiante = new DaoEstudiante();
            dgDatos.ItemsSource = estudiante.MostrarEstudiantesWhereGrado(idGrado);

        }

        public bool validarCantidadEstudiantes(int grado)
        {
            DaoEstudiante Daoestudiante = new DaoEstudiante();
            DaoGrado Daogrado = new DaoGrado();
            List<Model.Estudiante> estudiante = new List<Model.Estudiante>();
            int cant = 0;
            var ingreso = false;

            estudiante = Daoestudiante.MostrarEstudiantesWhereGrado(grado);
            cant = Daogrado.CantidadEstudiantesxGradoById(grado);

            if (estudiante.Count() >= cant)
            {
                ingreso = false;
            }
            else
            {
                ingreso = true;
            }

            return ingreso;

        }
        public void cargarCombo()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            DaoGrado daoGrado = new DaoGrado();
            List<Model.Grado> listadoGrados = new List<Model.Grado>();
            try
            {
                listadoGrados = daoGrado.MostrarGradoById(idGrado);

                foreach (var p in listadoGrados)
                {
                    lista.Add(p.id_grado, p.grado + ' ' + p.id_seccion.seccion);
                }

                cmbGrado.ItemsSource = lista;
                cmbGrado.DisplayMemberPath = "Value";
                cmbGrado.SelectedValuePath = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }

        public void bloquearCajas(bool valor)
        {
            txtID.IsEnabled = false;
            txtNombre.IsEnabled = valor;
            txtApellido.IsEnabled = valor;
            txtDireccion.IsEnabled = valor;
            txtResponsable.IsEnabled = valor;
            txtTelefono.IsEnabled = valor;
            cmbGrado.IsEnabled = valor;

        }

        public void bloquearBotones()
        {
            if (agregando)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }

            if (editando)
            {

                btnEliminar.IsEnabled = true;
                btnGuardar.IsEnabled = false;
                btnModificar.IsEnabled = true;
                btnNuevo.IsEnabled = true;
            }
            if (del)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }


        }

        public void limpiar()
        {
            txtID.Clear();
            txtNombre.Clear();
            txtApellido.Clear();
            txtDireccion.Clear();
            txtResponsable.Clear();
            txtTelefono.Clear();
            cmbGrado.SelectedIndex = -1;
        }

        public bool cajasVacias()
        {
            if (txtNombre.Text.Equals("") || txtApellido.Text.Equals("") || txtDireccion.Text.Equals("") || txtResponsable.Text.Equals("") || !txtTelefono.IsMaskCompleted || cmbGrado.SelectedIndex == -1)
            {
                tbData.Text = "¡Debe completar todos los campos!";
                return false;
            }


            return true;
        }

        public void agregarEstudiante()
        {
            try
            {
                Model.Estudiante estudiante = new Model.Estudiante();
                Model.Grado grado = new Model.Grado();
                estudiante.nombre = txtNombre.Text;
                estudiante.apellido = txtApellido.Text;
                estudiante.direccion = txtDireccion.Text;
                estudiante.padreResponsable = txtResponsable.Text;
                estudiante.telefono = txtTelefono.Text;
                grado.id_grado = int.Parse(cmbGrado.SelectedValue.ToString());
                estudiante.id_grado = grado;

                DAO.DaoEstudiante daoEstudiante = new DAO.DaoEstudiante();
                var response = daoEstudiante.InsertarEstudiante(estudiante);

                if (response != 0)
                {
                    tbData.Text = "¡El registro se ha ingresado correctamente!";
                }
                else
                {
                    tbData.Text = "Ha ocurrido un error en la inserción del nuevo registro\n";
                }


            }
            catch (Exception error)
            {
                tbData.Text = "Ha ocurrido un error:\n" + error.Message;
            }

        }

        public void modificarEstudiante()
        {
            Model.Estudiante estudiante = new Model.Estudiante();
            Model.Grado grado = new Model.Grado();
            estudiante.nombre = txtNombre.Text;
            estudiante.apellido = txtApellido.Text;
            estudiante.direccion = txtDireccion.Text;
            estudiante.padreResponsable = txtResponsable.Text;
            estudiante.telefono = txtTelefono.Text;
            grado.id_grado = int.Parse(cmbGrado.SelectedValue.ToString());
            estudiante.id_grado = grado;
            estudiante.id_estudiante = int.Parse(txtID.Text);

            DAO.DaoEstudiante daoEstudiante = new DAO.DaoEstudiante();
            var response = daoEstudiante.ActualizarEstudiante(estudiante);

            if (response != 0)
            {
                tbData.Text = "¡El registro se ha actualizado correctamente!";
            }
            else
            {
                tbData.Text = "Ha ocurrido un error en la actualización del nuevo registro\n";
            }

        }

        public void editar()
        {
            btnGuardar.IsEnabled = true;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            btnNuevo.IsEnabled = false;
        }

        private void dgDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Model.Estudiante es = (Model.Estudiante)dgDatos.SelectedItem;
            if (es == null)
            {
                return;
            }

            txtID.Text = es.id_estudiante.ToString();
            txtNombre.Text = es.nombre;
            txtApellido.Text = es.apellido;
            txtDireccion.Text = es.direccion;
            txtResponsable.Text = es.padreResponsable;
            txtTelefono.Text = es.telefono;
            cmbGrado.SelectedValue = es.id_grado.id_grado;
        }

        #endregion


        private void btnAceptarHidden_Click(object sender, RoutedEventArgs e)
        {
            if (del)
            {
                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
                Model.Estudiante estudiante = new Model.Estudiante();
                DAO.DaoEstudiante daoEstudiante = new DAO.DaoEstudiante();
                estudiante.id_estudiante = int.Parse(txtID.Text);
                daoEstudiante.EliminarEstudiante(estudiante);
                mostrarDatagrid();
                limpiar();
            }
            else if (editando)
            {
                editar();
                bloquearCajas(true);
                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnLimpiar.IsEnabled = false;
            btnCancelar.Visibility = Visibility.Hidden;
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptarHidden.Visibility = Visibility.Hidden;
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            btnLimpiar.IsEnabled = true;
            agregando = true;
            editando = false;
            del = false;
            bloquearBotones();
            bloquearCajas(true);
            limpiar();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (agregando)
            {
                if (cajasVacias())
                {

                    if (validarCantidadEstudiantes(int.Parse(cmbGrado.SelectedValue.ToString())))
                    {
                        agregarEstudiante();
                        editando = true;
                        agregando = false;
                        bloquearBotones();
                        bloquearCajas(false);
                        limpiar();
                        btnLimpiar.IsEnabled = false;
                        mostrarDatagrid();
                    }
                    else
                    {
                        tbData.Text = "No se puede agregar el estudiante, limite excedido\n";
                    }
                }
                else
                {
                    return;
                }
            }
            else if (editando)
            {
                if (cajasVacias() && !txtID.Text.Equals(""))
                {
                    modificarEstudiante();
                    editando = true;
                    agregando = false;
                    bloquearBotones();
                    bloquearCajas(false);
                    limpiar();
                    btnLimpiar.IsEnabled = false;
                    mostrarDatagrid();

                }
                else
                {
                    return;
                }
            }
            else if (del)
            {

            }
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            agregando = true;
            editando = false;
            del = false;

            Model.Estudiante estudiante = (Model.Estudiante)dgDatos.SelectedItem;
            if (estudiante == null)
            {
                tbData.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }

            if (txtID.Text.Equals(""))
            {
                tbData.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }
            else
            {
                tbData.Text = "¿Seguro que desea modificar el registro?";
                btnLimpiar.IsEnabled = true;
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                editando = true;
                agregando = false;
                del = false;
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            agregando = true;
            editando = false;
            del = false;
            Model.Estudiante estudiante = (Model.Estudiante)dgDatos.SelectedItem;
            if (estudiante == null)
            {
                tbData.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                //bloquearBotones();
                return;
            }

            if (txtID.Text.Equals(""))
            {
                tbData.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                //bloquearBotones();
                return;
            }
            else
            {
                tbData.Text = "¿Seguro que desea eliminar el registro?";
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                editando = false;
                agregando = false;
                del = true;
            }
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnLimpiar.IsEnabled = false;
            agregando = false;
            editando = true;
            del = false;
            bloquearBotones();
            bloquearCajas(false);
        }
    }
}
