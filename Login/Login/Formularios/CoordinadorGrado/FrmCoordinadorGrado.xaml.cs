﻿using SistemaDeNotas.DAO;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace SistemaDeNotas.Formularios.CoordinadorGrado
{
    /// <summary>
    /// Lógica de interacción para UserControl1.xaml
    /// </summary>
    public partial class FrmCoordinadorGrado : UserControl
    {
        public FrmCoordinadorGrado()
        {
            InitializeComponent();
            mostrarDatagrid();
            bloquearCajas(false);
            bloquearBotones();
            cargarComboDocente();
            cargarComboGrado();
            btnLimpiar.IsEnabled = false;

        }
        bool editando = true;
        bool agregando = false;
        bool del = false;
        public int _idDocente = 0;

        public void mostrarDatagrid()
        {
            DaoGradoDocente mat = new DaoGradoDocente();
            dgDatos.ItemsSource = mat.MostrarDocentesGrado();
            cargarComboDocente();
            cargarComboGrado();
        }

        public void cargarComboDocente()
        {
            cmbDocente.ItemsSource = null;
            cmbDocente.Items.Clear();
            Dictionary<int, string> lista = new Dictionary<int, string>();
            DaoGradoDocente daoGradoDocente = new DaoGradoDocente();
            List<Model.Docente> listadoDocente = new List<Model.Docente>();


            cmbDocente.ItemsSource = null;
            cmbDocente.Items.Clear();


            try
            {
                listadoDocente = daoGradoDocente.MostrarDocentesNotIn();


                foreach (var p in listadoDocente)
                {
                    lista.Add(p.id_docente, p.nombre + ' ' + p.apellido);
                }

                cmbDocente.ItemsSource = lista;
                cmbDocente.DisplayMemberPath = "Value";
                cmbDocente.SelectedValuePath = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }

        public void cargarComboGrado()
        {
            cmbGrado.ItemsSource = null;
            cmbGrado.Items.Clear();
            Dictionary<int, string> lista = new Dictionary<int, string>();
            DaoGradoDocente daoGradoDocente = new DaoGradoDocente();
            List<Model.Grado> listadoGrado = new List<Model.Grado>();


            cmbGrado.ItemsSource = null;
            cmbGrado.Items.Clear();


            try
            {
                listadoGrado = daoGradoDocente.MostrarGradoNotIn();


                foreach (var p in listadoGrado)
                {
                    lista.Add(p.id_grado, p.grado + ' ' + p.id_seccion.seccion);
                }

                cmbGrado.ItemsSource = lista;
                cmbGrado.DisplayMemberPath = "Value";
                cmbGrado.SelectedValuePath = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }



        #region
        public void bloquearCajas(bool valor)
        {

            cmbDocente.IsEnabled = valor;
            cmbGrado.IsEnabled = valor;
            //cmbSeccion.IsEnabled = valor;


        }

        public void bloquearBotones()
        {
            if (agregando)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnNuevo.IsEnabled = false;
            }

            if (editando)
            {

                btnEliminar.IsEnabled = true;
                btnGuardar.IsEnabled = false;
                btnNuevo.IsEnabled = true;
            }
            if (del)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnNuevo.IsEnabled = false;
            }


        }

        public void limpiar()
        {
            txtApellido.Clear();
            txtGrado.Clear();
            txtIdDocenteGrado.Clear();
            txtNombre.Clear();
            txtSeccion.Clear();

        }

        public bool cajasVacias()
        {
            if (cmbDocente.Text.Equals("") || cmbGrado.Text.Equals(""))
            {
                tbDatamateria.Text = "¡Las cajas no pueden ir vacias!\nPor favor valide cada una";
                return false;
            }



            return true;
        }

        public void agregarDocenteGrado()
        {

            try
            {
                Model.Docente_Grado docenteGrado = new Model.Docente_Grado();
                Model.Grado grado = new Model.Grado();
                Model.Docente docente = new Model.Docente();

                grado.id_grado = int.Parse(cmbGrado.SelectedValue.ToString());
                docente.id_docente = int.Parse(cmbDocente.SelectedValue.ToString());

                docenteGrado.id_docente = docente;
                docenteGrado.id_grado = grado;

                DAO.DaoGradoDocente daoGradoDocente = new DAO.DaoGradoDocente();
                List<Model.Docente_Grado> doc = new List<Model.Docente_Grado>();
                doc = daoGradoDocente.buscarDocenteGrado(docenteGrado);
                Model.Docente_Grado DocenteGradoDatos = new Model.Docente_Grado();

                foreach (Model.Docente_Grado a in doc)
                {
                    DocenteGradoDatos.idDocente_Grado = a.idDocente_Grado;
                }

                if (docenteGrado.idDocente_Grado == 0)
                {
                    daoGradoDocente.insertarDocenteGrado(docenteGrado);
                    tbDatamateria.Text = "¡El registro se ha ingresado correctamente!";
                }
                else
                {
                    tbDatamateria.Text = "¡los datos ya existen!, favor actualicelo";
                    return;
                }

            }
            catch (Exception error)
            {
                tbDatamateria.Text = "Ha ocurrido un error:\n" + error.Message;
            }

        }

        public void modificarDocenteGrado()
        {
            Model.Docente_Grado docenteGrado = new Model.Docente_Grado();
            //docenteGrado.idDocente_Grado = int.Parse(cmbDocente.Text);
            // docenteGrado.id_grado = int.Parse(cmbGrado.Text);

            Model.Grado grado = new Model.Grado();
            Model.Docente docente = new Model.Docente();

            docenteGrado.idDocente_Grado = int.Parse(txtIDDocenteGrupo.Text);

            grado.id_grado = int.Parse(cmbGrado.SelectedValue.ToString());
            docente.id_docente = int.Parse(cmbDocente.SelectedValue.ToString());


            docenteGrado.id_docente = docente;
            docenteGrado.id_grado = grado;

            DAO.DaoGradoDocente daoGradoDocente = new DAO.DaoGradoDocente();
            daoGradoDocente.actualizarDocenteGrado(docenteGrado);
            tbDatamateria.Text = "¡El registro se ha actualizado correctamente!";

        }

        public void eliminarDocenteGrado()
        {
            Model.Docente_Grado docenteGrado = new Model.Docente_Grado();
            DaoGradoDocente daoGradoDocente = new DaoGradoDocente();

            docenteGrado.idDocente_Grado = int.Parse(txtIdDocenteGrado.Text);
            daoGradoDocente.eliminarDocenteGrado(docenteGrado);
        }


        public void editar()
        {
            btnGuardar.IsEnabled = true;
            btnEliminar.IsEnabled = false;
            btnNuevo.IsEnabled = false;
        }

        #endregion
        #region
        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            btnLimpiar.IsEnabled = true;
            agregando = true;
            editando = false;
            del = false;
            bloquearBotones();
            bloquearCajas(true);
            limpiar();

        }


        private void dgDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Model.Docente_Grado doc = (Model.Docente_Grado)dgDatos.SelectedItem;

            if (doc == null)
            {
                return;
            }

            txtNombre.Text = doc.id_docente.nombre;
            txtApellido.Text = doc.id_docente.apellido;
            txtGrado.Text = doc.id_grado.grado;
            txtSeccion.Text = doc.id_grado.id_seccion.seccion;
            txtIdDocenteGrado.Text = doc.idDocente_Grado.ToString();
        }
        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {

            if (agregando)
            {
                if (cajasVacias())
                {

                    agregarDocenteGrado();
                    editando = true;
                    agregando = false;
                    bloquearBotones();
                    bloquearCajas(false);
                    limpiar();
                    btnLimpiar.IsEnabled = false;
                    mostrarDatagrid();
                }
                else
                {
                    return;
                }
            }
            else if (editando)
            {
                if (!txtNombre.Text.Equals("") && !txtApellido.Text.Equals(""))
                {
                    modificarDocenteGrado();
                    editando = true;
                    agregando = false;
                    bloquearBotones();
                    bloquearCajas(false);
                    limpiar();
                    btnLimpiar.IsEnabled = false;
                    mostrarDatagrid();

                }
                else
                {
                    return;
                }
            }
            else if (del)
            {
                if (!txtIdDocenteGrado.Text.Equals(""))
                {
                    eliminarDocenteGrado();
                    limpiar();
                }
                else
                {
                    tbDatamateria.Text = "Seleccione un registro para empezar con los cambios";
                    return;
                }
            }
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {

            agregando = true;
            editando = false;
            del = false;

            Model.Docente_Grado doc = (Model.Docente_Grado)dgDatos.SelectedItem;
            if (doc == null)
            {
                tbDatamateria.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }

            if (txtIDDocenteGrupo.Equals(""))
            {
                tbDatamateria.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }
            else
            {
                tbDatamateria.Text = "¿Seguro que desea modificar el registro?";
                btnLimpiar.IsEnabled = true;
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                editando = true;
                agregando = false;
                del = false;
            }

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            agregando = true;
            editando = false;
            del = false;
            Model.Docente_Grado doc = (Model.Docente_Grado)dgDatos.SelectedItem;
            if (doc == null)
            {
                tbDatamateria.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                //bloquearBotones();
                return;
            }

            if (txtIdDocenteGrado.Text.Equals(""))
            {
                tbDatamateria.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                return;
            }
            else
            {
                tbDatamateria.Text = "¿Seguro que desea eliminar el registro?";
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                editando = false;
                agregando = false;
                del = true;
            }
        }


        private void btnAceptarHidden_Click(object sender, RoutedEventArgs e)
        {
            if (del)
            {

                bloquearCajas(true);
                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
                eliminarDocenteGrado();
                mostrarDatagrid();
                limpiar();

            }
            else if (editando)
            {
                editar();
                bloquearCajas(true);

                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnLimpiar.IsEnabled = false;
            btnCancelar.Visibility = Visibility.Hidden;
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptarHidden.Visibility = Visibility.Hidden;
        }

        public void Limpiar()
        {
            txtNombre.Clear();
            txtApellido.Clear();
            txtGrado.Clear();
            txtSeccion.Clear();
            cmbDocente.SelectedIndex = -1;
            cmbGrado.SelectedIndex = -1;
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            Limpiar();
            btnLimpiar.IsEnabled = false;
            agregando = false;
            editando = true;
            del = false;
            bloquearBotones();
            bloquearCajas(false);
        }


        #endregion


    }
}