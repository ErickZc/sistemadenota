﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SistemaDeNotas.Formularios.DocenteMateria
{
    /// <summary>
    /// Lógica de interacción para AgregarMateriaAdocente.xaml
    /// </summary>
    public partial class AgregarMateriaAdocente : UserControl
    {
        public AgregarMateriaAdocente()
        {
            InitializeComponent();
            
            mostrarDatagrid();
            bloquearBotones();
            bloquearCajas(false);
            cargarComboDocente();
            cargarComboGrado();
            cargarComboMateria();
            btnLimpiar.IsEnabled = false;
        }
        bool editando = true;
        bool agregando = false;
        bool del = false;
        int caja = 0;

        #region
        public void bloquearCajas(bool valor)
        {
            txtId.IsEnabled = false;
            cmbDocente.IsEnabled = valor;
            cmbGrado.IsEnabled = valor;
            cmbMateria.IsEnabled = valor;


        }
        public void bloquearBotones()
        {
            if (agregando)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }

            if (editando)
            {

                btnEliminar.IsEnabled = true;
                btnGuardar.IsEnabled = false;
                btnModificar.IsEnabled = true;
                btnNuevo.IsEnabled = true;
            }
            if (del)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }


        }
        public void limpiar()
        {
            txtId.Clear();
            cmbMateria.SelectedIndex = -1;
            cmbDocente.SelectedIndex = -1;
            cmbGrado.SelectedIndex = -1;
        }
        public bool cajasVacias()
        {
            if (cmbDocente.SelectedIndex == -1 || cmbMateria.SelectedIndex == -1 || cmbGrado.SelectedIndex == -1)
            {
                tbData.Text = "¡Las cajas no pueden ir vacias!\nPor favor valide cada una";
                return false;
            }


            return true;
        }
        public void agregarMateria()
        {
            try
            {
                Model.Docente_Materia_Grado materia_Grado = new Model.Docente_Materia_Grado();
                Model.Grado grado = new Model.Grado();
                Model.Docente docente = new Model.Docente();
                Model.Materia materia = new Model.Materia();
                docente.id_docente = int.Parse(cmbDocente.SelectedValue.ToString());
                grado.id_grado = int.Parse(cmbGrado.SelectedValue.ToString());
                materia.id_materia = int.Parse(cmbMateria.SelectedValue.ToString());


                DAO.DaoDocenteMateriaG daoDocenteMG = new DAO.DaoDocenteMateriaG();
                var response = daoDocenteMG.InsertarMateriasGrados(materia_Grado);

                if (response != 0)
                {
                    tbData.Text = "¡Asignación realizada con exito!";
                }

                else
                {
                    tbData.Text = "¡Ha ocurrido un error en la inserción!";
                }


            }
            catch (Exception error)
            {
                tbData.Text = "Ha ocurrido un error:\n" + error.Message;
            }

        }
        public void modificarAgregar()
        {
            Model.Docente_Materia_Grado materia_Grado = new Model.Docente_Materia_Grado();
            Model.Grado grado = new Model.Grado();
            Model.Docente docente = new Model.Docente();
            Model.Materia materia = new Model.Materia();
            docente.id_docente = int.Parse(cmbDocente.SelectedValue.ToString());
            grado.id_grado = int.Parse(cmbGrado.SelectedValue.ToString());
            materia.id_materia = int.Parse(cmbMateria.SelectedValue.ToString());
            materia_Grado.idDocente_Materia = int.Parse(txtId.Text);


            DAO.DaoDocenteMG daoDocenteMG = new DAO.DaoDocenteMG();
            var response = daoDocenteMG.ActualizarEstudiante(materia_Grado);

            if (response != 0)
            {
                tbData.Text = "¡Registro actualizado correctamente!";
            }
            else
            {
                tbData.Text = "Ha ocurrido un error en la actualización del registro\n";
            }
        }



        public void editar()
        {
            btnGuardar.IsEnabled = true;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            btnNuevo.IsEnabled = false;
        }
        public void cargarComboGrado()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            DAO.DaoGrado daoGrado = new DAO.DaoGrado();
            List<Model.Grado> listadoGrados = new List<Model.Grado>();
            try
            {
                listadoGrados = daoGrado.MostrarGrados();

                foreach (var p in listadoGrados)
                {
                    lista.Add(p.id_grado, p.grado + ' ' + p.id_seccion.seccion);
                }

                cmbGrado.ItemsSource = lista;
                cmbGrado.DisplayMemberPath = "Value";
                cmbGrado.SelectedValuePath = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }
        public void cargarComboDocente()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            DAO.DaoDocente docente = new DAO.DaoDocente();
            List<Model.Docente> listadoDocente = new List<Model.Docente>();
            try
            {
                listadoDocente = docente.MostrarDocentes();

                foreach (var p in listadoDocente)
                {
                    lista.Add(p.id_docente, p.nombre + ' ' + p.apellido);
                }

                cmbDocente.ItemsSource = lista;
                cmbDocente.DisplayMemberPath = "Value";
                cmbDocente.SelectedValuePath = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }
        public void cargarComboMateria()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            DAO.DaoMateria daoMateria = new DAO.DaoMateria();
            List<Model.Materia> listadoMaterias = new List<Model.Materia>();
            try
            {
                listadoMaterias = daoMateria.MostrarMaterias();

                foreach (var p in listadoMaterias)
                {
                    lista.Add(p.id_materia, p.nombre_materia);
                }

                cmbMateria.ItemsSource = lista;
                cmbMateria.DisplayMemberPath = "Value";
                cmbMateria.SelectedValuePath = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }
        public void mostrarDatagrid()
        {
            DAO.DaoDocenteMateriaG daoDocenteMG = new DAO.DaoDocenteMateriaG();
            dgDatos.ItemsSource = daoDocenteMG.mostrarMateriasGrado();

        }
        public void AgregarMaterias()

        {
            try
            {
                Model.Docente_Materia_Grado estudiante = new Model.Docente_Materia_Grado();
                Model.Grado grado = new Model.Grado();
                Model.Docente docente = new Model.Docente();
                Model.Materia materia = new Model.Materia();

                grado.id_grado = int.Parse(cmbGrado.SelectedValue.ToString());
                estudiante.id_grado = grado;
                docente.id_docente = int.Parse(cmbDocente.SelectedValue.ToString());
                estudiante.id_docente = docente;
                materia.id_materia = int.Parse(cmbMateria.SelectedValue.ToString());
                estudiante.id_materia = materia;

                DAO.DaoDocenteMateriaG daoDocenteMateriaG = new DAO.DaoDocenteMateriaG();

                bool bandera = daoDocenteMateriaG.listarMateriasxGrado(materia.id_materia,grado.id_grado);

                if (bandera)
                {
                    var response = daoDocenteMateriaG.InsertarMateriasGrados(estudiante);

                    if (response != 0)
                    {
                        tbData.Text = "¡Asignación realizada con exito!";
                    }
                    else
                    {
                        tbData.Text = "Ha ocurrido un error en la inserción del nuevo registro\n";
                    }
                }
                else
                {
                    tbData.Text = "¡Ya existe un docente impartiendo esa materia!\n";
                }


            }
            catch (Exception error)
            {
                tbData.Text = "Ha ocurrido un error:\n" + error.Message;
            }


        }
        public void ModificarAd()
        {
            Model.Docente_Materia_Grado estudiante = new Model.Docente_Materia_Grado();
            Model.Grado grado = new Model.Grado();
            Model.Docente docente = new Model.Docente();
            Model.Materia materia = new Model.Materia();
            grado.id_grado = int.Parse(cmbGrado.SelectedValue.ToString());
            estudiante.id_grado = grado;
            docente.id_docente = int.Parse(cmbDocente.SelectedValue.ToString());
            estudiante.id_docente = docente;
            materia.id_materia = int.Parse(cmbMateria.SelectedValue.ToString());
            estudiante.id_materia = materia;
            estudiante.idDocente_Materia = int.Parse(txtId.Text);

            DAO.DaoDocenteMateriaG daoEstudiante = new DAO.DaoDocenteMateriaG();

            if (caja != int.Parse(cmbDocente.SelectedValue.ToString()))
            {
                bool bandera = daoEstudiante.listarMateriasxGradoNotIn(materia.id_materia, grado.id_grado, caja);

                if (bandera)
                {
                    var response = daoEstudiante.ActualizarMaterias(estudiante);
                    if (response != 0)
                    {
                        tbData.Text = "¡Registro actualizado correctamente!";
                    }
                    else
                    {
                        tbData.Text = "Ha ocurrido un error en la actualización del nuevo registro\n";
                    }
                }
                else
                {
                    tbData.Text = "¡Ya existe un docente impartiendo esa materia!\n";
                }

            }
            else
            {
                
                bool bandera = daoEstudiante.listarMateriasxGradoNotIn(materia.id_materia, grado.id_grado, docente.id_docente);

                if (bandera)
                {
                    var response = daoEstudiante.ActualizarMaterias(estudiante);
                    if (response != 0)
                    {
                        tbData.Text = "¡Registro actualizado correctamente!";
                    }
                    else
                    {
                        tbData.Text = "Ha ocurrido un error en la actualización del nuevo registro\n";
                    }
                }
                else
                {
                    tbData.Text = "¡Ya existe un docente impartiendo esa materia!\n";
                }

                
            }

        }
        public void MostrarDataGrid()
        {
            DAO.DaoDocenteMateriaG docenteMateriaG = new DAO.DaoDocenteMateriaG();
            Model.Docente_Materia_Grado _Sistema = new Model.Docente_Materia_Grado();
            dgDatos.ItemsSource = docenteMateriaG.mostrarMateriasGrado();
        }
        #endregion




        #region
        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            btnLimpiar.IsEnabled = true;
            agregando = true;
            editando = false;
            del = false;
            bloquearBotones();
            bloquearCajas(true);
            limpiar();
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            agregando = true;
            editando = false;
            del = false;
            caja = int.Parse(cmbDocente.SelectedValue.ToString());

            Model.Docente_Materia_Grado estudiante = (Model.Docente_Materia_Grado)dgDatos.SelectedItem;
            if (estudiante == null)
            {
                tbData.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }

            if (txtId.Text.Equals(""))
            {
                tbData.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }
            else
            {
                tbData.Text = "¿Seguro que desea actualilzar el registro?";
                btnLimpiar.IsEnabled = true;
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                btnLimpiar.IsEnabled = true;
                editando = true;
                agregando = false;
                del = false;
            }
        }
       
       

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {

            if (agregando)
            {
                if (cajasVacias())
                {

                    AgregarMaterias();
                    editando = true;
                    agregando = false;
                    bloquearBotones();
                    bloquearCajas(false);
                    limpiar();
                    btnLimpiar.IsEnabled = false;
                    MostrarDataGrid();
                }
                else
                {
                    return;
                }
            }
            else if (editando)
            {
                if (cajasVacias() && !txtId.Text.Equals(""))
                {
                    ModificarAd();
                    editando = true;
                    agregando = false;
                    bloquearBotones();
                    bloquearCajas(false);
                    limpiar();
                    btnLimpiar.IsEnabled = false;
                    MostrarDataGrid();

                }
                else
                {
                    return;
                }
            }
            else if (del)
            {

            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            agregando = true;
            editando = false;
            del = false;
            Model.Docente_Materia_Grado estudiante = (Model.Docente_Materia_Grado)dgDatos.SelectedItem;
            if (estudiante == null)
            {
                tbData.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                //bloquearBotones();
                return;
            }

            if (txtId.Text.Equals(""))
            {
                tbData.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                //bloquearBotones();
                return;
            }
            else
            {
                tbData.Text = "¿Seguro que desea eliminar el registro?";
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                editando = false;
                agregando = false;
                del = true;
            }
        }

        private void dgDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Model.Docente_Materia_Grado docente_Materia_ = (Model.Docente_Materia_Grado)dgDatos.SelectedItem;
            if (docente_Materia_ == null)
            {
                return;
            }

            txtId.Text = docente_Materia_.idDocente_Materia.ToString();
            cmbGrado.SelectedValue = docente_Materia_.id_grado.id_grado;
            cmbMateria.SelectedValue = docente_Materia_.id_materia.id_materia;
            cmbDocente.SelectedValue = docente_Materia_.id_docente.id_docente;
            

        }
        private void btnAceptarHidden_Click(object sender, RoutedEventArgs e)
        {
            if (del)
            {

                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
                Model.Docente_Materia_Grado estudiante = new Model.Docente_Materia_Grado();
                DAO.DaoDocenteMateriaG daoEstudiante = new DAO.DaoDocenteMateriaG();
                estudiante.idDocente_Materia = int.Parse(txtId.Text);
                daoEstudiante.EliminarRegistro(estudiante);
                mostrarDatagrid();
                limpiar();
            }
            else if (editando)
            {
                editar();
                bloquearCajas(true);

                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnLimpiar.IsEnabled = false;
            btnCancelar.Visibility = Visibility.Hidden;
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptarHidden.Visibility = Visibility.Hidden;
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnLimpiar.IsEnabled = false;
            agregando = false;
            editando = true;
            del = false;
            bloquearBotones();
            bloquearCajas(false);
        }

        #endregion
    }
}
