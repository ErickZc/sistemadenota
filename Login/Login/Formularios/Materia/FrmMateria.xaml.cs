﻿using SistemaDeNotas.DAO;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;


namespace SistemaDeNotas.Formularios.Materia
{
    /// <summary>
    /// Lógica de interacción para UserControl1.xaml
    /// </summary>
    public partial class FrmMateria : UserControl
    {
        public FrmMateria()
        {
            InitializeComponent();
            mostrarDatagrid();
            bloquearCajas(false);
            bloquearBotones();
            btnLimpiar.IsEnabled = false;
        }

        bool editando = true;
        bool agregando = false;
        bool del = false;





        #region
        public void mostrarDatagrid()
        {
            DaoMateria mat = new DaoMateria();
            dgDatosMateria.ItemsSource = mat.MostrarMaterias();

        }
        public void bloquearCajas(bool valor)
        {

            txtNombreMateria.IsEnabled = valor;


        }

        public void bloquearBotones()
        {
            if (agregando)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }

            if (editando)
            {

                btnEliminar.IsEnabled = true;
                btnGuardar.IsEnabled = false;
                btnModificar.IsEnabled = true;
                btnNuevo.IsEnabled = true;
            }
            if (del)
            {
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnModificar.IsEnabled = false;
                btnNuevo.IsEnabled = false;
            }


        }

        public void limpiar()
        {
            txtIdMateria.Clear();
            txtNombreMateria.Clear();



        }

        public bool cajasVacias()
        {
            if (txtNombreMateria.Text.Equals(""))
            {
                tbDatamateria.Text = "¡Las cajas no pueden ir vacias!\nPor favor valide cada una";
                return false;
            }



            return true;
        }

        public void agregarMateria()
        {
            try
            {
                Model.Materia materia = new Model.Materia();
                materia.nombre_materia = txtNombreMateria.Text;



                DAO.DaoMateria daoMateria = new DAO.DaoMateria();
                List<Model.Materia> doc = new List<Model.Materia>();
                doc = daoMateria.buscarMateria(materia);
                Model.Materia materiaDatos = new Model.Materia();

                foreach (Model.Materia a in doc)
                {
                    materiaDatos.id_materia = a.id_materia;
                }

                if (materiaDatos.id_materia == 0)
                {
                    daoMateria.InsertarMaterias(materia);
                    tbDatamateria.Text = "¡El registro se ha ingresado correctamente!";
                }
                else
                {
                    tbDatamateria.Text = "¡La materia ya existe!, favor actualicelo";
                    return;
                }

            }
            catch (Exception error)
            {
                tbDatamateria.Text = "Ha ocurrido un error:\n" + error.Message;
            }

        }

        public void modificarMateria()
        {
            Model.Materia materia = new Model.Materia();
            materia.id_materia = int.Parse(txtIdMateria.Text);
            materia.nombre_materia = txtNombreMateria.Text;





            DAO.DaoMateria daoMateria = new DAO.DaoMateria();
            daoMateria.actualizarMaterias(materia);
            tbDatamateria.Text = "¡El registro se ha actualizado correctamente!";

        }

        public void editar()
        {
            btnGuardar.IsEnabled = true;
            btnModificar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            btnNuevo.IsEnabled = false;
        }

        #endregion
        #region EVENTOS
        private void dgDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Model.Materia doc = (Model.Materia)dgDatosMateria.SelectedItem;
            if (doc == null)
            {
                return;
            }


            txtIdMateria.Text = doc.id_materia.ToString();
            txtNombreMateria.Text = doc.nombre_materia.ToString();





        }
        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            btnLimpiar.IsEnabled = true;
            agregando = true;
            editando = false;
            del = false;
            bloquearBotones();
            bloquearCajas(true);
            limpiar();

        }



        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {

            if (agregando)
            {
                if (cajasVacias())
                {

                    agregarMateria();
                    editando = true;
                    agregando = false;
                    bloquearBotones();
                    bloquearCajas(false);
                    limpiar();
                    btnLimpiar.IsEnabled = false;
                    mostrarDatagrid();
                }
                else
                {
                    return;
                }
            }
            else if (editando)
            {
                if (cajasVacias() && !txtNombreMateria.Text.Equals(""))
                {
                    modificarMateria();
                    editando = true;
                    agregando = false;
                    bloquearBotones();
                    bloquearCajas(false);
                    limpiar();
                    btnLimpiar.IsEnabled = false;
                    mostrarDatagrid();

                }
                else
                {
                    return;
                }
            }
            else if (del)
            {

            }
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {

            agregando = true;
            editando = false;
            del = false;

            Model.Materia doc = (Model.Materia)dgDatosMateria.SelectedItem;
            if (doc == null)
            {
                tbDatamateria.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }

            if (txtNombreMateria.Text.Equals(""))
            {
                tbDatamateria.Text = "Seleccione un registro para empezar con los cambios";
                editando = true;
                agregando = false;
                bloquearBotones();
                bloquearCajas(false);
                return;
            }
            else
            {
                tbDatamateria.Text = "¿Seguro que desea modificar el registro?";
                btnLimpiar.IsEnabled = true;
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                editando = true;
                agregando = false;
                del = false;
            }

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            agregando = true;
            editando = false;
            del = false;
            Model.Materia doc = (Model.Materia)dgDatosMateria.SelectedItem;
            if (doc == null)
            {
                tbDatamateria.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                //bloquearBotones();
                return;
            }

            if (txtNombreMateria.Text.Equals(""))
            {
                tbDatamateria.Text = "Seleccione un registro para eliminarlo";
                bloquearCajas(false);
                agregando = false;
                editando = true;
                del = false;
                return;
            }
            else
            {
                tbDatamateria.Text = "¿Seguro que desea eliminar el registro?";
                btnAceptarHidden.Visibility = Visibility.Visible;
                btnCancelar.Visibility = Visibility.Visible;
                btnAceptar.Visibility = Visibility.Hidden;
                editando = false;
                agregando = false;
                del = true;
            }
        }


        private void btnAceptarHidden_Click(object sender, RoutedEventArgs e)
        {
            if (del)
            {

                bloquearCajas(true);
                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
                Model.Materia materia = new Model.Materia();
                DAO.DaoMateria daoMarteria = new DAO.DaoMateria();
                materia.id_materia = int.Parse(txtIdMateria.Text);
                daoMarteria.eliminarMaterias(materia);
                mostrarDatagrid();
                limpiar();

            }
            else if (editando)
            {
                editar();
                bloquearCajas(true);

                btnCancelar.Visibility = Visibility.Hidden;
                btnAceptar.Visibility = Visibility.Visible;
                btnAceptarHidden.Visibility = Visibility.Hidden;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnLimpiar.IsEnabled = false;
            btnCancelar.Visibility = Visibility.Hidden;
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptarHidden.Visibility = Visibility.Hidden;
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnLimpiar.IsEnabled = false;
            agregando = false;
            editando = true;
            del = false;
            bloquearBotones();
            bloquearCajas(false);
        }
        #endregion
    }
}