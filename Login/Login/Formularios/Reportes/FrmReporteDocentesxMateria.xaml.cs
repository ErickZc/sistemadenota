﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Login.rpt;

namespace SistemaDeNotas.Formularios.Reportes
{
    /// <summary>
    /// Lógica de interacción para FrmReporteDocentesxMateria.xaml
    /// </summary>
    public partial class FrmReporteDocentesxMateria : UserControl
    {
        public FrmReporteDocentesxMateria()
        {
            InitializeComponent();
            mostrarReporte();
        }

        public void mostrarReporte()
        {
            rptMateriaDocente reporteNotas = new rptMateriaDocente();
            // reporteNotas.Load("@reporteNotas");
            Report.ViewerCore.ReportSource = reporteNotas;
        }
    }
}
