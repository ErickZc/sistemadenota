﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SistemaDeNotas.DAO;
using CrystalDecisions.Shared;
using Login.rpt;

namespace SistemaDeNotas.Formularios.Reportes
{
    /// <summary>
    /// Lógica de interacción para FrmReporteEstudiantesMatriculados.xaml
    /// </summary>
    public partial class FrmReporteEstudiantesMatriculados : UserControl
    {
        int limpiando = 0;

        public FrmReporteEstudiantesMatriculados()
        {
            InitializeComponent();
            cargarComboGrado();
            limpiar();
            cmbGrados.SelectionChanged += new SelectionChangedEventHandler(cmbGrados_SelectionChanged);
        }

        public void mostrarReporte()
        {
            ParameterFields parametros = new ParameterFields();
            ParameterField miParametro1 = new ParameterField();
            ParameterDiscreteValue valor1 = new ParameterDiscreteValue();

            try
            {
                miParametro1.ParameterValueType =
                ParameterValueKind.NumberParameter;
                miParametro1.Name = "@idGrado";
                miParametro1.CurrentValues.Clear();
                valor1.Value = int.Parse(cmbGrados.SelectedValue.ToString());
                miParametro1.CurrentValues.Add(valor1);
                parametros.Add(miParametro1);
                rptEstudianteMatriculado reporte = new rptEstudianteMatriculado();
                Report.ViewerCore.ReportSource = reporte;
                Report.ViewerCore.ParameterFieldInfo = parametros;
                Report.ViewerCore.Zoom(75);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }

        public void cargarComboGrado()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            DaoGrado daoGrado = new DaoGrado();
            List<Model.Grado> listadoGrados = new List<Model.Grado>();
            try
            {
                listadoGrados = daoGrado.MostrarGrados();

                foreach (var p in listadoGrados)
                {
                    lista.Add(p.id_grado, p.grado + ' ' + p.id_seccion.seccion);
                }

                cmbGrados.ItemsSource = lista;
                cmbGrados.DisplayMemberPath = "Value";
                cmbGrados.SelectedValuePath = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }

        public void limpiar()
        {
            cmbGrados.SelectedIndex = -1;
            limpiando = 0;
        }

        public bool cajasVacias()
        {
            if (cmbGrados.SelectedIndex == -1)
            {
                tbData.Text = "¡Debe completar todos los campos!";
                return false;
            }

            return true;
        }

        private void cmbGrados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (limpiando == 1)
            {
                btnBuscar.Visibility = Visibility.Hidden;
                btnBuscar2.Visibility = Visibility.Visible;
                return;
            }
            else
            {
                if (cajasVacias())
                {
                    btnBuscar.Visibility = Visibility.Visible;
                    btnBuscar2.Visibility = Visibility.Hidden;
                }
                else
                {
                    btnBuscar.Visibility = Visibility.Hidden;
                    btnBuscar2.Visibility = Visibility.Visible;
                }
            }
        }

        private void btnAceptarHidden_Click(object sender, RoutedEventArgs e)
        {
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptarHidden.Visibility = Visibility.Hidden;
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            mostrarReporte();
        }

        private void btnBuscar2_Click(object sender, RoutedEventArgs e)
        {
            if (cajasVacias())
            {
            }
            else
            {
                return;
            }
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            limpiando = 1;
            limpiar();

        }
    }
}
