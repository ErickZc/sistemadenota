﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SistemaDeNotas.DAO;
using SistemaDeNotas.Model;
using CrystalDecisions.Shared;
using Login.rpt;

namespace SistemaDeNotas.Formularios.Reportes
{
    /// <summary>
    /// Lógica de interacción para FrmReporteNotas.xaml
    /// </summary>
    public partial class FrmReporteNotas : UserControl
    {
        int limpiando = 0;

        public FrmReporteNotas()
        {
            InitializeComponent();
            cargarComboGrado();
            cargarComboMateria();
            cargarComboPeriodo();
        }
        public void mostrarReporte()
        {
            ParameterFields parametros = new ParameterFields();
            ParameterField miParametro1 = new ParameterField();
            ParameterDiscreteValue valor1 = new ParameterDiscreteValue();
            ParameterField miParametro2 = new ParameterField();
            ParameterDiscreteValue valor2 = new ParameterDiscreteValue();
            ParameterField miParametro3 = new ParameterField();
            ParameterDiscreteValue valor3 = new ParameterDiscreteValue();

            try
            {
                miParametro1.ParameterValueType =
                ParameterValueKind.NumberParameter;
                miParametro1.Name = "@idMateria";
                miParametro1.CurrentValues.Clear();
                valor1.Value = int.Parse(cmbMateria.SelectedValue.ToString());
                miParametro1.CurrentValues.Add(valor1);
                parametros.Add(miParametro1);

                miParametro2.ParameterValueType =
                ParameterValueKind.NumberParameter;
                miParametro2.Name = "@idGrado";
                miParametro2.CurrentValues.Clear();
                valor2.Value = int.Parse(cmbGrado.SelectedValue.ToString());
                miParametro2.CurrentValues.Add(valor2);
                parametros.Add(miParametro2);

                miParametro3.ParameterValueType =
                ParameterValueKind.NumberParameter;
                miParametro3.Name = "@idPeriodo";
                miParametro3.CurrentValues.Clear();
                valor3.Value = int.Parse(cmbPeriodo.SelectedValue.ToString());
                miParametro3.CurrentValues.Add(valor3);
                parametros.Add(miParametro3);

                rptNotaPorMateria reporte = new rptNotaPorMateria();
                Report.ViewerCore.ReportSource = reporte;
                Report.ViewerCore.ParameterFieldInfo = parametros;
                Report.ViewerCore.Zoom(75);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }


        public void cargarComboMateria()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            DaoMateria daoDMG = new DaoMateria();
            List<Model.Materia> listadoMateria = new List<Model.Materia>();
            try
            {
                listadoMateria = daoDMG.MostrarMaterias();

                foreach (var p in listadoMateria)
                {
                    lista.Add(p.id_materia, p.nombre_materia);
                }

                cmbMateria.ItemsSource = lista;
                cmbMateria.DisplayMemberPath = "Value";
                cmbMateria.SelectedValuePath = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }

        public void cargarComboGrado()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            DaoGrado daoGrado = new DaoGrado();
            List<Model.Grado> listadoGrados = new List<Model.Grado>();

            cmbGrado.ItemsSource = null;
            cmbPeriodo.ItemsSource = null;
            cmbGrado.Items.Clear();
            cmbPeriodo.Items.Clear();

            try
            {
                listadoGrados = daoGrado.MostrarGrados();

                foreach (var p in listadoGrados)
                {
                    lista.Add(p.id_grado, p.grado + ' ' + p.id_seccion.seccion);
                }

                cmbGrado.ItemsSource = lista;
                cmbGrado.DisplayMemberPath = "Value";
                cmbGrado.SelectedValuePath = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }

        public void cargarComboPeriodo()
        {
            cmbPeriodo.ItemsSource = null;
            cmbPeriodo.Items.Clear();

             DaoPeriodo daoPeriodo = new DaoPeriodo();
             Dictionary<int, string> lista = new Dictionary<int, string>();
             List<Periodo> listaPeriodo = new List<Periodo>();

             try
             {
                    listaPeriodo = daoPeriodo.MostrarPeriodos();
                    foreach (var p in listaPeriodo)
                    {
                        lista.Add(p.id_periodo, p.periodo);
                    }
                    cmbPeriodo.ItemsSource = lista;
                    cmbPeriodo.DisplayMemberPath = "Value";
                    cmbPeriodo.SelectedValuePath = "Key";

              }
              catch
              {

              }
            
        }
        private void btnAceptarHidden_Click(object sender, RoutedEventArgs e)
        {
            btnAceptar.Visibility = Visibility.Visible;
            btnAceptarHidden.Visibility = Visibility.Hidden;
        }

        public void limpiar()
        {
            cmbGrado.SelectedIndex = -1;
            cmbMateria.SelectedIndex = -1;
            cmbPeriodo.SelectedIndex = -1;
            limpiando = 0;
        }

        public bool cajasVacias()
        {
            if (cmbGrado.SelectedIndex == -1 || cmbMateria.SelectedIndex == -1 || cmbPeriodo.SelectedIndex == -1)
            {
                tbData.Text = "¡Debe completar todos los campos!";
                return false;
            }

            return true;
        }


        private void cmbMateria_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (limpiando == 1)
            {
                btnBuscar.Visibility = Visibility.Hidden;
                btnBuscar2.Visibility = Visibility.Visible;
                return;

            }
            else
            {

                if (cajasVacias())
                {
                    btnBuscar.Visibility = Visibility.Visible;
                    btnBuscar2.Visibility = Visibility.Hidden;
                }
                else
                {
                    btnBuscar.Visibility = Visibility.Hidden;
                    btnBuscar2.Visibility = Visibility.Visible;
                }
            }
           
        }

        private void cmbGrado_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (limpiando == 1)
            {
                btnBuscar.Visibility = Visibility.Hidden;
                btnBuscar2.Visibility = Visibility.Visible;
                return;

            }
            else
            {

                if (cajasVacias())
                {
                    btnBuscar.Visibility = Visibility.Visible;
                    btnBuscar2.Visibility = Visibility.Hidden;
                }
                else
                {
                    btnBuscar.Visibility = Visibility.Hidden;
                    btnBuscar2.Visibility = Visibility.Visible;
                }
            }
           
        }

        private void cmbPeriodo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (limpiando == 1)
            {
                btnBuscar.Visibility = Visibility.Hidden;
                btnBuscar2.Visibility = Visibility.Visible;
                return;

            }
            else
            {

                if (cajasVacias())
                {
                    btnBuscar.Visibility = Visibility.Visible;
                    btnBuscar2.Visibility = Visibility.Hidden;
                }
                else
                {
                    btnBuscar.Visibility = Visibility.Hidden;
                    btnBuscar2.Visibility = Visibility.Visible;
                }
            }
           
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            limpiando = 1;
            limpiar();
        }

        private void btnBuscar2_Click(object sender, RoutedEventArgs e)
        {
            if (cajasVacias())
            {
            }
            else
            {
                return;
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            mostrarReporte();
        }
    }
}
