﻿using SistemaDeNotas.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Windows;

namespace SistemaDeNotas.DAO
{
    class DaoSeccion
    {
        public DaoSeccion()
        {

        }
        public List<Seccion> MostrarSeccion()
        {
            List<Seccion> lstSeccion = new List<Seccion>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM Seccion where estado = 'OPEN'";

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Seccion seccion = new Seccion();
                                    Grado grado = new Grado();
                                    seccion.id_seccion = int.Parse(dr["id_seccion"].ToString());
                                    seccion.seccion = dr["seccion"].ToString();
                                    grado.id_seccion = seccion;
                                    lstSeccion.Add(seccion);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstSeccion;
        }
    }
}
