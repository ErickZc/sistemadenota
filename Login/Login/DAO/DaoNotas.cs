﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaDeNotas.Model;
using System.Data.SqlClient;
using System.Windows;
using System.Data;
using System.Data.Common;

namespace SistemaDeNotas.DAO
{
    class DaoNotas
    {
        public DaoNotas()
        {

        }

        public List<Nota> verNotasDocente(int _periodo, int _materia, int _grado)
        {
            List<Nota> lstNotas = new List<Nota>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT n.*,e.id_estudiante,e.nombre,e.apellido FROM NOTA n INNER JOIN Estudiante e on n.id_estudiante = e.id_estudiante where e.estado = 'OPEN' AND n.id_periodo = @PERIODO and n.id_materia = @MATERIA and e.id_grado = @GRADO";
                        command.Parameters.AddWithValue("@PERIODO", _periodo);
                        command.Parameters.AddWithValue("@MATERIA", _materia);
                        command.Parameters.AddWithValue("@GRADO", _grado);
                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Estudiante estudiante = new Estudiante();
                                    Nota nota = new Nota();
                                    nota.id_nota = int.Parse(dr["id_nota"].ToString());
                                    estudiante.id_estudiante = int.Parse(dr["id_estudiante"].ToString());
                                    estudiante.apellido = dr["apellido"].ToString();
                                    estudiante.nombre = dr["nombre"].ToString();
                                    nota.actividad1 = decimal.Parse(dr["actividad1"].ToString());
                                    nota.actividad2 = decimal.Parse(dr["actividad2"].ToString());
                                    nota.actividad3 = decimal.Parse(dr["actividad3"].ToString());
                                    nota.examen = decimal.Parse(dr["examen"].ToString());
                                    nota.promedio = decimal.Parse(dr["promedio"].ToString());
                                    nota.id_estudiante = estudiante;
                                    lstNotas.Add(nota);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstNotas;

        }

        public List<Nota> verNotasDocenteById(int _periodo, int _materia, int _grado, int valor, string val)
        {
            List<Nota> lstNotas = new List<Nota>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;

                        if (valor == 1)
                        {
                            command.CommandText = "SELECT n.*,e.id_estudiante,e.nombre,e.apellido FROM NOTA n INNER JOIN Estudiante e on n.id_estudiante = e.id_estudiante where e.estado = 'OPEN' AND n.id_periodo = 1 and n.id_materia = 1 and e.id_grado = 1 and e.id_estudiante = @ID_ESTUDIANTE";
                            command.Parameters.AddWithValue("@ID_ESTUDIANTE", int.Parse(val));
                        }
                        else if (valor ==2)
                        {
                            command.CommandText = "SELECT n.*,e.id_estudiante,e.nombre,e.apellido FROM NOTA n INNER JOIN Estudiante e on n.id_estudiante = e.id_estudiante where e.estado = 'OPEN' AND n.id_periodo = 1 and n.id_materia = 1 and e.id_grado = 1 and e.nombre = @NOMBRE";
                            command.Parameters.AddWithValue("@NOMBRE", val);
                        }
                        else
                        {
                            command.CommandText = "SELECT n.*,e.id_estudiante,e.nombre,e.apellido FROM NOTA n INNER JOIN Estudiante e on n.id_estudiante = e.id_estudiante where e.estado = 'OPEN' AND n.id_periodo = 1 and n.id_materia = 1 and e.id_grado = 1 and e.apellido = @APELLIDO";
                            command.Parameters.AddWithValue("@APELLIDO", val);
                        }
                        command.Parameters.AddWithValue("@PERIODO", _periodo);
                        command.Parameters.AddWithValue("@MATERIA", _materia);
                        command.Parameters.AddWithValue("@GRADO", _grado);
                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Estudiante estudiante = new Estudiante();
                                    Nota nota = new Nota();
                                    nota.id_nota = int.Parse(dr["id_nota"].ToString());
                                    estudiante.id_estudiante = int.Parse(dr["id_estudiante"].ToString());
                                    estudiante.apellido = dr["apellido"].ToString();
                                    estudiante.nombre = dr["nombre"].ToString();
                                    nota.actividad1 = decimal.Parse(dr["actividad1"].ToString());
                                    nota.actividad2 = decimal.Parse(dr["actividad2"].ToString());
                                    nota.actividad3 = decimal.Parse(dr["actividad3"].ToString());
                                    nota.examen = decimal.Parse(dr["examen"].ToString());
                                    nota.promedio = decimal.Parse(dr["promedio"].ToString());
                                    nota.id_estudiante = estudiante;
                                    lstNotas.Add(nota);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstNotas;

        }

        public int InsertarNota(Nota nota)
        {
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "INSERT INTO NOTA VALUES (@ID_ESTUDIANTE,@ID_MATERIA,@ACTIVIDAD1,@ACTIVIDAD1,@ACTIVIDAD1,@EXAMEN,@PROMEDIO,@ID_PERIODO)";
                        command.Parameters.AddWithValue("@ID_ESTUDIANTE", nota.id_estudiante.id_estudiante);
                        command.Parameters.AddWithValue("@ID_MATERIA", nota.id_materia.id_materia);
                        command.Parameters.AddWithValue("@ACTIVIDAD1", nota.actividad1);
                        command.Parameters.AddWithValue("@ACTIVIDAD2", nota.actividad2);
                        command.Parameters.AddWithValue("@ACTIVIDAD3", nota.actividad3);
                        command.Parameters.AddWithValue("@EXAMEN", nota.examen);
                        command.Parameters.AddWithValue("@PROMEDIO", nota.promedio);
                        command.Parameters.AddWithValue("@ID_PERIODO", nota.id_periodo.id_periodo);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public int actualizarNota(Nota nota)
        {
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE NOTA SET id_estudiante = @ID_ESTUDIANTE,id_materia = @ID_MATERIA, actividad1 = @ACTIVIDAD1, actividad2 = @ACTIVIDAD2, actividad3 = @ACTIVIDAD3, examen = @EXAMEN, promedio = @PROMEDIO, id_periodo = @ID_PERIODO where id_nota = @ID_NOTA;";
                        command.Parameters.AddWithValue("@ID_ESTUDIANTE", nota.id_estudiante.id_estudiante);
                        command.Parameters.AddWithValue("@ID_MATERIA", nota.id_materia.id_materia);
                        command.Parameters.AddWithValue("@ACTIVIDAD1", nota.actividad1);
                        command.Parameters.AddWithValue("@ACTIVIDAD2", nota.actividad2);
                        command.Parameters.AddWithValue("@ACTIVIDAD3", nota.actividad3);
                        command.Parameters.AddWithValue("@EXAMEN", nota.examen);
                        command.Parameters.AddWithValue("@PROMEDIO", nota.promedio);
                        command.Parameters.AddWithValue("@ID_PERIODO", nota.id_periodo.id_periodo);
                        command.Parameters.AddWithValue("@ID_NOTA", nota.id_nota);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public int eliminarNota(Nota nota)
        {
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "DELETE FROM Nota WHERE ID_NOTA = @ID_NOTA";
                        command.Parameters.AddWithValue("@ID_NOTA", nota.id_nota);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

    }
}
