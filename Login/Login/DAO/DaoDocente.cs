﻿using SistemaDeNotas.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Windows;

namespace SistemaDeNotas.DAO
{
    class DaoDocente
    {
        public DaoDocente()
        {

        }

        public List<Docente> MostrarDocente()
        {
            List<Docente> lstDocente = new List<Docente>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        //command.CommandType = CommandType.StoredProcedure;
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM DOCENTE where estado = 'OPEN' order by id_docente desc";

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente docente = new Docente();
                                    docente.id_docente = int.Parse(dr["id_docente"].ToString());
                                    docente.nombre = dr["nombre"].ToString();
                                    docente.apellido = dr["apellido"].ToString();
                                    docente.direccion = dr["direccion"].ToString();
                                    docente.telefono = dr["telefono"].ToString();
                                    docente.dui = dr["dui"].ToString();
                                    docente.nit = dr["nit"].ToString();
                                    docente.correo = dr["correo"].ToString();
                                    docente.fecha_creacion = dr["fecha_creacion"].ToString();
                                    docente.estado = dr["estado"].ToString();
                                    docente.usuario = dr["usuario"].ToString();
                                    docente.password = dr["password"].ToString();
                                    docente.genero = dr["genero"].ToString();
                                    //docente.id_rol.id_rol = int.Parse(dr["id_rol"].ToString());
                                    lstDocente.Add(docente);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstDocente;
        }
        public List<Docente> MostrarDocentes()
        {
            List<Docente> listaDocente = new List<Docente>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT  d.id_docente,d.apellido,d.nombre,d.correo,d.direccion,d.estado,d.dui,d.genero FROM Docente d  WHERE ESTADO = 'OPEN'";

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente docente = new Docente();

                                    docente.id_docente = int.Parse(dr["id_docente"].ToString());
                                    docente.apellido = dr["apellido"].ToString();
                                    docente.nombre = dr["nombre"].ToString();
                                    //seccion.seccion = dr["seccion"].ToString();
                                    docente.estado = dr["estado"].ToString();
                                    listaDocente.Add(docente);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return listaDocente;
        }

        public List<Docente> getDocenteLogin(string user, string pass)
        {
            List<Docente> lstDocente = new List<Docente>();
            DaoEncrypt cifrado = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        //command.CommandType = CommandType.StoredProcedure;
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT TOP 1 * FROM DOCENTE where usuario = @username and password = @pass and estado = 'OPEN'";
                        command.Parameters.AddWithValue("@username", user);
                        command.Parameters.AddWithValue("@pass", cifrado.Encriptar(pass));

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente docente = new Docente();
                                    docente.id_docente = int.Parse(dr["id_docente"].ToString());
                                    docente.nombre = dr["nombre"].ToString();
                                    docente.apellido = dr["apellido"].ToString();
                                    docente.direccion = dr["direccion"].ToString();
                                    docente.telefono = dr["telefono"].ToString();
                                    docente.dui = dr["dui"].ToString();
                                    docente.nit = dr["nit"].ToString();
                                    docente.correo = dr["correo"].ToString();
                                    docente.fecha_creacion = dr["fecha_creacion"].ToString();
                                    docente.estado = dr["estado"].ToString();
                                    docente.usuario = dr["usuario"].ToString();
                                    docente.password = dr["password"].ToString();
                                    docente.genero = dr["genero"].ToString();
                                    //docente.id_rol.id_rol = int.Parse(dr["id_rol"].ToString());
                                    lstDocente.Add(docente);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstDocente;
        }

        public int InsertarDocente(Docente docente)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "INSERT INTO DOCENTE(NOMBRE,APELLIDO,DIRECCION,TELEFONO,DUI,NIT,CORREO,FECHA_CREACION,ESTADO,USUARIO,PASSWORD,ID_ROL,GENERO) values(@NOMBRE,@APELLIDO,@DIRECCION,@TELEFONO,@DUI,@NIT,@CORREO,@FECHA_CREACION,'OPEN',@USUARIO,@PASSWORD,2,@GENERO)";
                        command.Parameters.AddWithValue("@NOMBRE", docente.nombre);
                        command.Parameters.AddWithValue("@APELLIDO", docente.apellido);
                        command.Parameters.AddWithValue("@DIRECCION", docente.direccion);
                        command.Parameters.AddWithValue("@TELEFONO", docente.telefono);
                        command.Parameters.AddWithValue("@DUI", docente.dui);
                        command.Parameters.AddWithValue("@NIT", docente.nit);
                        command.Parameters.AddWithValue("@CORREO", docente.correo);
                        command.Parameters.AddWithValue("@FECHA_CREACION", DateTime.Now.ToString("dd/MM/yyyy"));
                        command.Parameters.AddWithValue("@USUARIO", docente.usuario);
                        command.Parameters.AddWithValue("@PASSWORD", encrypt.Encriptar("1234"));
                        command.Parameters.AddWithValue("@GENERO", docente.genero);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public int actualizarDocente(Docente docente)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "update Docente SET NOMBRE = @NOMBRE, APELLIDO = @APELLIDO,DIRECCION = @DIRECCION, TELEFONO = @TELEFONO, DUI = @DUI, NIT = @NIT, CORREO = @CORREO, GENERO = @GENERO WHERE ID_DOCENTE = @ID_DOCENTE";
                        command.Parameters.AddWithValue("@NOMBRE", docente.nombre);
                        command.Parameters.AddWithValue("@APELLIDO", docente.apellido);
                        command.Parameters.AddWithValue("@DIRECCION", docente.direccion);
                        command.Parameters.AddWithValue("@TELEFONO", docente.telefono);
                        command.Parameters.AddWithValue("@DUI", docente.dui);
                        command.Parameters.AddWithValue("@NIT", docente.nit);
                        command.Parameters.AddWithValue("@CORREO", docente.correo);
                        //command.Parameters.AddWithValue("@USUARIO", docente.usuario);
                        command.Parameters.AddWithValue("@GENERO", docente.genero);
                        command.Parameters.AddWithValue("@ID_DOCENTE", docente.id_docente);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public int actualizarContraDocente(Docente docente)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;

                        command.CommandText = "update Docente SET PASSWORD = @PASSWORD, ID_ROL = @ID_ROL WHERE USUARIO = @USUARIO";
                        // command.Parameters.AddWithValue("@NOMBRE", docente.nombre);
                        //command.Parameters.AddWithValue("@APELLIDO", docente.apellido);
                        //command.Parameters.AddWithValue("@DIRECCION", docente.direccion);
                        //command.Parameters.AddWithValue("@TELEFONO", docente.telefono);
                        //command.Parameters.AddWithValue("@DUI", docente.dui);
                        //command.Parameters.AddWithValue("@NIT", docente.nit);
                        //command.Parameters.AddWithValue("@CORREO", docente.correo);
                        command.Parameters.AddWithValue("@USUARIO", docente.usuario);
                        //command.Parameters.AddWithValue("@GENERO", docente.genero);
                        //command.Parameters.AddWithValue("@ID_DOCENTE", docente.id_docente);

                        command.CommandText = "update Docente SET PASSWORD = @PASSWORD WHERE ID_DOCENTE = @ID_DOCENTE";                   
                        command.Parameters.AddWithValue("@ID_DOCENTE", docente.id_docente);

                        command.Parameters.AddWithValue("@PASSWORD", docente.password);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }
        public int eliminarDocente(Docente docente)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE DOCENTE SET ESTADO = 'LOCKED' WHERE ID_DOCENTE = @ID_DOCENTE";
                        command.Parameters.AddWithValue("@ID_DOCENTE", docente.id_docente);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public int actualizarPassword(Docente docente)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE DOCENTE SET password = @password WHERE usuario = @usuario";
                        command.Parameters.AddWithValue("@usuario", docente.usuario);
                        command.Parameters.AddWithValue("@password", encrypt.Encriptar(docente.password));
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public List<Docente> buscarUsernameDocente(Docente doc)
        {
            List<Docente> lstDocente = new List<Docente>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        //command.CommandType = CommandType.StoredProcedure;
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT TOP 1 * FROM DOCENTE WHERE USUARIO = @USUARIO";
                        //command.Parameters.AddWithValue("@ID_DOCENTE", doc.id_docente);
                        command.Parameters.AddWithValue("@USUARIO", doc.usuario);

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente docente = new Docente();
                                    docente.id_docente = int.Parse(dr["id_docente"].ToString());
                                    docente.nombre = dr["nombre"].ToString();
                                    docente.apellido = dr["apellido"].ToString();
                                    docente.direccion = dr["direccion"].ToString();
                                    docente.telefono = dr["telefono"].ToString();
                                    docente.dui = dr["dui"].ToString();
                                    docente.nit = dr["nit"].ToString();
                                    docente.correo = dr["correo"].ToString();
                                    docente.fecha_creacion = dr["fecha_creacion"].ToString();
                                    docente.estado = dr["estado"].ToString();
                                    docente.usuario = dr["usuario"].ToString();
                                    docente.password = dr["password"].ToString();
                                    docente.genero = dr["genero"].ToString();
                                    //docente.id_rol.id_rol = int.Parse(dr["id_rol"].ToString());
                                    lstDocente.Add(docente);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstDocente;
        }

    }
}
