﻿using SistemaDeNotas.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Windows;

namespace SistemaDeNotas.DAO
{
    class DaoEstudiante
    {
        public DaoEstudiante()
        {

        }

        public List<Estudiante> MostrarEstudiantes()
        {
            List<Estudiante> lstEstudiante = new List<Estudiante>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT e.id_estudiante,e.nombre,e.apellido,e.direccion,e.padreResponsable,e.telefono,e.estado_promedio,e.fecha_creacion,g.id_grado,g.grado FROM ESTUDIANTE e INNER JOIN GRADO g on g.id_grado=e.id_grado WHERE e.ESTADO = 'OPEN' ORDER BY e.ID_ESTUDIANTE DESC";

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Estudiante estudiante = new Estudiante();
                                    Grado grado = new Grado();
                                    estudiante.id_estudiante = int.Parse(dr["id_estudiante"].ToString());
                                    estudiante.nombre = dr["nombre"].ToString();
                                    estudiante.apellido = dr["apellido"].ToString();
                                    estudiante.direccion = dr["direccion"].ToString();
                                    estudiante.padreResponsable = dr["padreResponsable"].ToString();
                                    estudiante.telefono = dr["telefono"].ToString();
                                    estudiante.estado_promedio = dr["estado_promedio"].ToString();
                                    estudiante.fecha_creacion = dr["fecha_creacion"].ToString();
                                    grado.id_grado = int.Parse(dr["id_grado"].ToString());
                                    grado.grado = dr["grado"].ToString();
                                    estudiante.id_grado = grado;
                                    lstEstudiante.Add(estudiante);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstEstudiante;
        }

        public List<Estudiante> MostrarEstudiantesWhereGrado(int id_grado)
        {
            List<Estudiante> lstEstudiante = new List<Estudiante>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT e.id_estudiante,e.nombre,e.apellido,e.direccion,e.padreResponsable,e.telefono,e.estado_promedio,e.fecha_creacion,g.id_grado,g.grado FROM ESTUDIANTE e INNER JOIN GRADO g on g.id_grado=e.id_grado INNER JOIN SECCION s on s.id_seccion=g.id_seccion WHERE e.ESTADO = 'OPEN' AND g.estado='OPEN' and s.estado='OPEN' and e.id_grado=@ID_GRADO ORDER BY e.ID_ESTUDIANTE DESC";
                        command.Parameters.AddWithValue("@ID_GRADO", id_grado);
                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Estudiante estudiante = new Estudiante();
                                    Grado grado = new Grado();
                                    estudiante.id_estudiante = int.Parse(dr["id_estudiante"].ToString());
                                    estudiante.nombre = dr["nombre"].ToString();
                                    estudiante.apellido = dr["apellido"].ToString();
                                    estudiante.direccion = dr["direccion"].ToString();
                                    estudiante.padreResponsable = dr["padreResponsable"].ToString();
                                    estudiante.telefono = dr["telefono"].ToString();
                                    estudiante.estado_promedio = dr["estado_promedio"].ToString();
                                    estudiante.fecha_creacion = dr["fecha_creacion"].ToString();
                                    grado.id_grado = int.Parse(dr["id_grado"].ToString());
                                    grado.grado = dr["grado"].ToString();
                                    estudiante.id_grado = grado;
                                    lstEstudiante.Add(estudiante);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstEstudiante;
        }


        public int InsertarEstudiante(Estudiante estudiante)
        {
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "INSERT INTO ESTUDIANTE(NOMBRE,APELLIDO,DIRECCION,PADRERESPONSABLE,TELEFONO,ESTADO_PROMEDIO,FECHA_CREACION,ESTADO,ID_GRADO) values(@NOMBRE,@APELLIDO,@DIRECCION,@PADRERESPONSABLE,@TELEFONO,'EN CURSO',@FECHA_CREACION,'OPEN',@ID_GRADO)";
                        command.Parameters.AddWithValue("@NOMBRE", estudiante.nombre);
                        command.Parameters.AddWithValue("@APELLIDO", estudiante.apellido);
                        command.Parameters.AddWithValue("@DIRECCION", estudiante.direccion);
                        command.Parameters.AddWithValue("@PADRERESPONSABLE", estudiante.padreResponsable);
                        command.Parameters.AddWithValue("@TELEFONO", estudiante.telefono);
                        command.Parameters.AddWithValue("@FECHA_CREACION", DateTime.Now.ToString("dd/MM/yyyy"));
                        command.Parameters.AddWithValue("@ID_GRADO", estudiante.id_grado.id_grado);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return response;
        }

        public int ActualizarEstudiante(Estudiante estudiante)
        {
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE ESTUDIANTE SET NOMBRE = @NOMBRE, APELLIDO = @APELLIDO,DIRECCION = @DIRECCION,PADRERESPONSABLE = @PADRERESPONSABLE,TELEFONO = @TELEFONO, ID_GRADO = @ID_GRADO WHERE ID_ESTUDIANTE = @ID_ESTUDIANTE";
                        command.Parameters.AddWithValue("@NOMBRE", estudiante.nombre);
                        command.Parameters.AddWithValue("@APELLIDO", estudiante.apellido);
                        command.Parameters.AddWithValue("@DIRECCION", estudiante.direccion);
                        command.Parameters.AddWithValue("@PADRERESPONSABLE", estudiante.padreResponsable);
                        command.Parameters.AddWithValue("@TELEFONO", estudiante.telefono);
                        command.Parameters.AddWithValue("@ID_GRADO", estudiante.id_grado.id_grado);
                        command.Parameters.AddWithValue("@ID_ESTUDIANTE", estudiante.id_estudiante);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public int EliminarEstudiante(Estudiante estudiante)
        {
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE ESTUDIANTE SET ESTADO = 'LOCKED' WHERE ID_ESTUDIANTE = @ID_ESTUDIANTE";
                        command.Parameters.AddWithValue("@ID_ESTUDIANTE", estudiante.id_estudiante);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public List<Estudiante> MostrarEstudiantesNotIn(int _periodo, int _materia, int _grado)
        {
            List<Estudiante> lstEstudiante = new List<Estudiante>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM Estudiante e where e.estado = 'OPEN' AND e.id_grado = @GRADO and id_estudiante not in ((SELECT e.id_estudiante FROM NOTA n INNER JOIN Estudiante e on n.id_estudiante = e.id_estudiante where e.estado = 'OPEN' AND n.id_periodo = @PERIODO and n.id_materia = @MATERIA and e.id_grado = @GRADO)) ORDER BY nombre ASC";
                                                                                                                                                    
                        command.Parameters.AddWithValue("@GRADO", _grado);
                        command.Parameters.AddWithValue("@PERIODO", _periodo);
                        command.Parameters.AddWithValue("@MATERIA", _materia);

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Estudiante estudiante = new Estudiante();
                                    Grado grado = new Grado();
                                    estudiante.id_estudiante = int.Parse(dr["id_estudiante"].ToString());
                                    estudiante.nombre = dr["nombre"].ToString();
                                    estudiante.apellido = dr["apellido"].ToString();
                                    lstEstudiante.Add(estudiante);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstEstudiante;
        }

    }
}
