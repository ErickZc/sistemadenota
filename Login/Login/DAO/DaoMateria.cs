﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Windows;
using SistemaDeNotas.Model;



namespace SistemaDeNotas.DAO
{
    class DaoMateria
    {
        public DaoMateria()
        {

        }
        public List<Model.Materia> GetMaterias()
        {
            List<Model.Materia> listamaterias = new List<Model.Materia>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM Materia where estado ='OPEN'";

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Model.Materia materia = new Model.Materia();

                                    materia.id_materia = int.Parse(dr["id_materia"].ToString());
                                    materia.nombre_materia = dr["nombre_materia"].ToString();
                                    materia.estado = dr["estado"].ToString();

                                    listamaterias.Add(materia);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return listamaterias;
        }


        public List<Materia> MostrarMaterias()
        {
            List<Materia> lstMaterias = new List<Materia>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        //command.CommandType = CommandType.StoredProcedure;
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM MATERIA where estado = 'OPEN' order by id_materia desc";

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Materia materia = new Materia();
                                    materia.id_materia = int.Parse(dr["id_materia"].ToString());
                                    materia.nombre_materia = dr["nombre_materia"].ToString();
                                    materia.estado = dr["estado"].ToString();

                                    lstMaterias.Add(materia);


                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstMaterias;
        }



        public int InsertarMaterias(Materia materia)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "INSERT INTO MATERIA(NOMBRE_MATERIA,ESTADO) values(@NOMBRE_MATERIA,'OPEN')";

                        command.Parameters.AddWithValue("@NOMBRE_MATERIA", materia.nombre_materia);

                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public int actualizarMaterias(Materia materia)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "update Materia SET NOMBRE_MATERIA = @NOMBRE_MATERIA WHERE ID_MATERIA = @ID_MATERIA";
                        command.Parameters.AddWithValue("@ID_MATERIA", materia.id_materia);
                        command.Parameters.AddWithValue("@NOMBRE_MATERIA", materia.nombre_materia);

                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public int eliminarMaterias(Materia materia)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE MATERIA SET ESTADO = 'LOCKED' WHERE ID_MATERIA = @ID_MATERIA";
                        command.Parameters.AddWithValue("@ID_MATERIA", materia.id_materia);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public List<Materia> buscarMateria(Materia mat)
        {
            List<Materia> lstMaterias = new List<Materia>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {

                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT TOP 1 * FROM MATERIA WHERE NOMBRE_MATERIA = @NOMBRE_MATERIA";
                        command.Parameters.AddWithValue("@ID_MATERIA", mat.id_materia);
                        command.Parameters.AddWithValue("@NOMBRE_MATERIA", mat.nombre_materia);

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Materia materia = new Materia();
                                    materia.id_materia = int.Parse(dr["id_docente"].ToString());
                                    materia.nombre_materia = dr["nombre_materia"].ToString();

                                    lstMaterias.Add(materia);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstMaterias;
        }
    }
}








