﻿using SistemaDeNotas.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Windows;
using Login;

namespace SistemaDeNotas.DAO
{
    class DaoAdminitrador
    {
        public DaoAdminitrador() { }

        public List<Administrador_Sistema> getAdministrador(string user, string pass)
        {
            List<Administrador_Sistema> lstAdmin = new List<Administrador_Sistema>();
            DaoEncrypt cifrado = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM Administrador_Sistema WHERE USERNAME = @username and password = @pass and estado = 'OPEN'";
                        command.Parameters.AddWithValue("@username", user);
                        command.Parameters.AddWithValue("@pass", cifrado.Encriptar(pass));

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Administrador_Sistema administrador = new Administrador_Sistema();
                                    administrador.id_admin = int.Parse(dr["id_admin"].ToString());
                                    administrador.nombre = dr["nombre"].ToString();
                                    administrador.apellido = dr["apellido"].ToString();
                                    administrador.telefono = dr["telefono"].ToString();
                                    administrador.dui = dr["dui"].ToString();
                                    administrador.correo = dr["correo"].ToString();
                                    administrador.estado = dr["estado"].ToString();
                                    administrador.username = dr["username"].ToString();
                                    lstAdmin.Add(administrador);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstAdmin;
        }

        public int actualizarContraAdmin(Administrador_Sistema administrador)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "update Administrador_Sistema SET PASSWORD = @PASSWORD WHERE USERNAME = @USERNAME";
                        command.Parameters.AddWithValue("@USERNAME", administrador.username);
                        command.Parameters.AddWithValue("@PASSWORD", administrador.password);
                        

                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public List<Administrador_Sistema> MostrarAdmin()
        {
            List<Administrador_Sistema> lstAdmin = new List<Administrador_Sistema>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM Administrador_Sistema where estado ='OPEN'";

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Administrador_Sistema administrador = new Administrador_Sistema();
                                    administrador.id_admin = int.Parse(dr["id_admin"].ToString());
                                    administrador.nombre = dr["nombre"].ToString();
                                    administrador.apellido = dr["apellido"].ToString();
                                    administrador.telefono = dr["telefono"].ToString();
                                    administrador.dui = dr["dui"].ToString();
                                    administrador.correo = dr["correo"].ToString();
                                    administrador.estado = dr["estado"].ToString();
                                    administrador.username = dr["username"].ToString();
                                    administrador.password = dr["password"].ToString();
                                    lstAdmin.Add(administrador);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstAdmin;
        }
        public int InsertarAdmin(Administrador_Sistema administrador_)
        {
            DAO.DaoEncrypt en = new DaoEncrypt();
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        
                        command.CommandType = CommandType.Text;
                        command.CommandText = "INSERT INTO Administrador_Sistema(NOMBRE,APELLIDO,USERNAME,PASSWORD,TELEFONO,CORREO,DUI,ESTADO) values(@NOMBRE,@APELLIDO,@USERNAME,@PASSWORD,@TELEFONO,@CORREO,@DUI,'OPEN')";
                        command.Parameters.AddWithValue("@NOMBRE", administrador_.nombre);
                        command.Parameters.AddWithValue("@APELLIDO", administrador_.apellido);
                        command.Parameters.AddWithValue("@USERNAME", administrador_.username);
                        command.Parameters.AddWithValue("@PASSWORD", administrador_.password);
                        command.Parameters.AddWithValue("@TELEFONO", administrador_.telefono);
                        command.Parameters.AddWithValue("@CORREO", administrador_.correo);
                        command.Parameters.AddWithValue("@DUI", administrador_.dui);
                        //command.Parameters.AddWithValue("@ESTADO", administrador_.estado);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return response;
        }


        public int ActualizarAdmin(Administrador_Sistema administrador_Sistema)
        {
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE Administrador_Sistema SET NOMBRE = @NOMBRE, APELLIDO = @APELLIDO,USERNAME = @USERNAME,TELEFONO = @TELEFONO, CORREO = @CORREO,DUI = @DUI WHERE ID_ADMIN = @ID_ADMIN";
                        command.Parameters.AddWithValue("@NOMBRE", administrador_Sistema.nombre);
                        command.Parameters.AddWithValue("@APELLIDO", administrador_Sistema.apellido);
                        command.Parameters.AddWithValue("@USERNAME", administrador_Sistema.username);                        
                        command.Parameters.AddWithValue("@TELEFONO", administrador_Sistema.telefono);
                        command.Parameters.AddWithValue("@CORREO", administrador_Sistema.correo);
                        command.Parameters.AddWithValue("@DUI", administrador_Sistema.dui);                       
                        command.Parameters.AddWithValue("@ID_ADMIN", administrador_Sistema.id_admin);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }
        public int EliminarAdmin(Administrador_Sistema administrador)
        {
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE Administrador_Sistema SET ESTADO = 'LOCKED' WHERE id_admin = @id_admin";
                        command.Parameters.AddWithValue("@id_admin", administrador.id_admin);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public List<Administrador_Sistema> buscarUsernameAdmin(Administrador_Sistema doc)
        {
            List<Administrador_Sistema> lstAdmin = new List<Administrador_Sistema>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT TOP 1 * FROM ADMINISTRADOR_SISTEMA WHERE USERNAME = @USUARIO";
                        //command.Parameters.AddWithValue("@ID_DOCENTE", doc.id_docente);
                        command.Parameters.AddWithValue("@USUARIO", doc.username);

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Administrador_Sistema ad = new Administrador_Sistema();
                                    ad.id_admin = int.Parse(dr["id_admin"].ToString());
                                    ad.nombre = dr["nombre"].ToString();
                                    ad.apellido = dr["apellido"].ToString();
                                    ad.telefono = dr["telefono"].ToString();
                                    ad.dui = dr["dui"].ToString();
                                    ad.correo = dr["correo"].ToString();
                                    ad.estado = dr["estado"].ToString();
                                    ad.password = dr["password"].ToString();
                                    //docente.id_rol.id_rol = int.Parse(dr["id_rol"].ToString());
                                    lstAdmin.Add(ad);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstAdmin;
        }


    }
}
