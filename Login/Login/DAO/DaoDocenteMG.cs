﻿using SistemaDeNotas.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Windows;

namespace SistemaDeNotas.DAO
{
    class DaoDocenteMG
    {
        public List<Docente_Materia_Grado> MostrarDocenteMateriaByID(int doc)
        {
            List<Docente_Materia_Grado> lstGradoDocente = new List<Docente_Materia_Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        //command.CommandType = CommandType.StoredProcedure;
                        command.CommandType = CommandType.Text;

                        command.CommandText = "SELECT * FROM DOCENTE_MATERIA_GRADO where estado = 'OPEN' = AND id_docente = @id_docente";
                        command.Parameters.AddWithValue("@docente", doc);


                        command.CommandText = "SELECT * FROM DOCENTE_MATERIA_GRADO where estado = 'OPEN' AND id_docente = @id_docente";
                        command.Parameters.AddWithValue("@id_docente", doc);
                        


                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente_Materia_Grado docente = new Docente_Materia_Grado();
                                    docente.idDocente_Materia = int.Parse(dr["idDocente_Materia"].ToString());
                                    //docente.id_docente.id_docente = int.Parse(dr["id_docente"].ToString());
                                    //docente.id_grado.id_grado = int.Parse(dr["id_grado"].ToString());
                                    docente.fecha = dr["fecha"].ToString();
                                    docente.estado = dr["estado"].ToString();
                                    lstGradoDocente.Add(docente);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstGradoDocente;
        }
        public List<Docente_Materia_Grado> MostrarDocenteMateriaBy()
        {
            List<Docente_Materia_Grado> lstGradoDocente = new List<Docente_Materia_Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        //command.CommandType = CommandType.StoredProcedure;
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM DOCENTE_MATERIA_GRADO where estado = 'OPEN'";


                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente_Materia_Grado docente = new Docente_Materia_Grado();
                                    Docente docente1 = new Docente();
                                    Grado grado = new Grado();
                                    Materia materia = new Materia();
                                    docente.idDocente_Materia = int.Parse(dr["idDocente_Materia"].ToString());
                                    grado.id_grado = int.Parse(dr["id_grado"].ToString());
                                    docente1.id_docente = int.Parse(dr["id_docente"].ToString());
                                    materia.id_materia = int.Parse(dr["id_materia"].ToString());
                                   // docente.id_grado = grado;
                                    //docente.id_docente = docente1;
                                    //docente.id_materia = materia;
                                    docente.fecha = dr["fecha"].ToString();
                                    docente.estado = dr["estado"].ToString();
                                    lstGradoDocente.Add(docente);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstGradoDocente;
        }

        public int InsertarMaterias(Model.Docente_Materia_Grado docente_Materia_)
        {
            int response = 0;
            Model.Docente doc = new Docente();
            Materia mat = new Materia();
            Grado grado = new Grado();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "INSERT INTO DOCENTE_MATERIA_GRADO(id_docente,id_materia,id_grado,fecha,estado) values(@id_docente,@id_materia,@id_grado,,'OPEN')";
                        command.Parameters.AddWithValue("@id_docente", docente_Materia_.id_docente);
                        command.Parameters.AddWithValue("@id_materia", docente_Materia_.id_materia);
                        command.Parameters.AddWithValue("@id_grado", docente_Materia_.id_grado) ;
                        command.Parameters.AddWithValue("@fecha",DateTime.Now.ToString());
                        
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return response;
        }

        public int ActualizarEstudiante(Docente_Materia_Grado docente_Materia_)
        {
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE DOCENTE_MATERIA_GRADO SET id_docente = @id_docente, id_materia = @id_materia,id_grado = @id_grado,fecha = @fecha,estado = @estado WHERE idDocente_Materia = @idDocente_Materia";
                        command.Parameters.AddWithValue("@id_docente", docente_Materia_.id_docente);
                        command.Parameters.AddWithValue("@id_materia", docente_Materia_.id_materia);
                        command.Parameters.AddWithValue("@id_grado", docente_Materia_.id_grado);
                        command.Parameters.AddWithValue("@fecha", docente_Materia_.fecha);
                        command.Parameters.AddWithValue("@estado", docente_Materia_.estado);

                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public int EliminarEstudiante(Docente_Materia_Grado materia_Grado)
        {
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE DOCENTE_MATERIA_GRADO SET ESTADO = 'LOCKED' WHERE idDocente_Materia = @idDocente_Materia";
                        command.Parameters.AddWithValue("@idDocente_Materia", materia_Grado.idDocente_Materia);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }


    }
}
