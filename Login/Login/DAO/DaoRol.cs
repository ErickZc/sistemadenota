﻿using SistemaDeNotas.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Windows;

namespace SistemaDeNotas.DAO
{
    class DaoRol
    {
        public DaoRol() { }

        public List<Rol_Usuario> MostrarRol()
        {
            List<Rol_Usuario> lstRol = new List<Rol_Usuario>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        //command.CommandType = CommandType.StoredProcedure;
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM ROL_USUARIO";

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Rol_Usuario rol = new Rol_Usuario();
                                    rol.id_rol = int.Parse(dr["id_rol"].ToString());
                                    rol.rol = dr["rol"].ToString();
                                    lstRol.Add(rol);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstRol;
        }

    }
}
