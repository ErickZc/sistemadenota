﻿using SistemaDeNotas.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Windows;

namespace SistemaDeNotas.DAO
{
    class DaoPeriodo
    {
        public DaoPeriodo()
        {

        }

        public List<Periodo> MostrarPeriodos()
        {
            List<Periodo> lstPeriodo = new List<Periodo>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM PERIODO WHERE ESTADO = 'OPEN'";

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Periodo periodo = new Periodo();
                                    periodo.id_periodo = int.Parse(dr["id_periodo"].ToString());
                                    periodo.periodo = dr["periodo"].ToString();
                                    lstPeriodo.Add(periodo);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstPeriodo;
        }

    }
}
