﻿using SistemaDeNotas.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Windows;

namespace SistemaDeNotas.DAO
{
    class DaoGradoDocente
    {
        public DaoGradoDocente() { }

        public List<Docente_Grado> MostrarGradoDocenteByID(int doc)
        {
            List<Docente_Grado> lstGradoDocente = new List<Docente_Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        //command.CommandType = CommandType.StoredProcedure;
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM DOCENTE_GRADO where estado = 'OPEN' AND id_docente = @docente";
                        command.Parameters.AddWithValue("@docente", doc);

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente_Grado docente = new Docente_Grado();
                                    Grado gra = new Grado();
                                    docente.idDocente_Grado = int.Parse(dr["idDocente_Grado"].ToString());
                                    //docente.id_docente.id_docente = int.Parse(dr["id_docente"].ToString());
                                    gra.id_grado = int.Parse(dr["id_grado"].ToString());
                                    docente.fecha = dr["fecha"].ToString();
                                    docente.estado = dr["estado"].ToString();
                                    docente.id_grado = gra;
                                    lstGradoDocente.Add(docente);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstGradoDocente;
        }


        public List<Docente> MostrarDocenteByIdgradodocente()
        {
            List<Docente> lstdocente = new List<Docente>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM docente";


                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {

                                    Docente docente = new Docente();

                                    docente.nombre = dr["nombre"].ToString();
                                    docente.apellido = dr["apellido"].ToString();
                                    docente.id_docente = int.Parse(dr["id_docente"].ToString());

                                    lstdocente.Add(docente);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstdocente;
        }
        public List<Grado> MostrarGradoByIdgradodocente()
        {
            List<Grado> lstgrado = new List<Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM grado g inner join seccion s on g.id_seccion = s.id_seccion where g.estado = 'OPEN' ";


                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {

                                    Grado grado = new Grado();
                                    Seccion seccion = new Seccion();

                                    grado.grado = dr["grado"].ToString();
                                    grado.id_grado = int.Parse(dr["id_grado"].ToString());

                                    seccion.id_seccion = int.Parse(dr["id_seccion"].ToString());
                                    seccion.seccion = dr["seccion"].ToString();

                                    grado.id_seccion = seccion;
                                    //grado.id_seccion.seccion = seccion;


                                    lstgrado.Add(grado);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstgrado;
        }

        public List<Seccion> MostrarSeccionByIdgradodocente()
        {
            List<Seccion> lstseccion = new List<Seccion>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM seccion";


                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {

                                    Seccion seccion = new Seccion();

                                    seccion.seccion = dr["seccion"].ToString();
                                    seccion.id_seccion = int.Parse(dr["id_seccion"].ToString());

                                    lstseccion.Add(seccion);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstseccion;
        }




        public List<Docente_Grado> MostrarDocentesGrado()
        {
            List<Docente_Grado> lstDocenteGrado = new List<Docente_Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM DOCENTE_GRADO dg INNER JOIN Grado g on g.id_grado = dg.id_grado INNER JOIN Docente d on d.id_docente = dg.id_docente inner join seccion s on g.id_seccion = s.id_seccion WHERE dg.ESTADO = 'OPEN' order by idDocente_Grado desc";

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente_Grado docentegrado = new Docente_Grado();
                                    Grado grado = new Grado();
                                    Seccion seccion = new Seccion();
                                    Docente docente = new Docente();



                                    docentegrado.idDocente_Grado = int.Parse(dr["idDocente_Grado"].ToString());

                                    docente.id_docente = int.Parse(dr["id_docente"].ToString());
                                    docente.nombre = dr["nombre"].ToString();
                                    docente.apellido = dr["apellido"].ToString();



                                    docentegrado.id_docente = docente;




                                    docentegrado.idDocente_Grado = int.Parse(dr["idDocente_Grado"].ToString());                                
                                    docente.id_docente = int.Parse(dr["id_docente"].ToString());
                                    docente.nombre = dr["nombre"].ToString();
                                    docente.apellido = dr["apellido"].ToString();
                                    docentegrado.id_docente = docente;

                                    grado.id_grado = int.Parse(dr["id_grado"].ToString());
                                    grado.grado = dr["grado"].ToString();
                                    docentegrado.id_grado = grado;
                                    seccion.id_seccion = int.Parse(dr["id_seccion"].ToString());
                                    seccion.seccion = dr["seccion"].ToString();
                                    docentegrado.id_grado.id_seccion = seccion;











                                    lstDocenteGrado.Add(docentegrado);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstDocenteGrado;
        }



        public bool insertarDocenteGrado(Docente_Grado docentegrado)
        {
            int res = 0;
            try
            {
                using (var conn = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conn.Open();
                    using (var command = conn.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(@idDocente,@idGrado,' ','OPEN');";
                        command.Parameters.AddWithValue("@idDocente", docentegrado.id_docente.id_docente);
                        command.Parameters.AddWithValue("@idGrado", docentegrado.id_grado.id_grado);

                        res = command.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public List<Docente_Grado> buscarDocenteGrado(Docente_Grado mat)
        {
            List<Docente_Grado> lstDocenteGrado = new List<Docente_Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {

                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT TOP 1 * FROM DOCENTE_GRADO WHERE IDDOCENTE_GRADO = @IDDOCENTE_GRADO";
                        command.Parameters.AddWithValue("@IDDocente_grado", mat.idDocente_Grado);
                        ///command.Parameters.AddWithValue("@ID_DOCENTE", mat.id_docente);
                        //command.Parameters.AddWithValue("@ID_GRADO", mat.id_grado);

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente_Grado DocenteGrado = new Docente_Grado();
                                    DocenteGrado.idDocente_Grado = int.Parse(dr["idDocente_Grado"].ToString());
                                    // DocenteGrado.id_grado = int.Parse(dr["id_grado"].ToString());

                                    lstDocenteGrado.Add(DocenteGrado);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstDocenteGrado;
        }


        public int actualizarDocenteGrado(Docente_Grado docente_Grado)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "update Docente_Grado SET id_grado = @ID_GRADO, id_docente = @ID_DOCENTE WHERE IDDOCENTE_GRADO = @IDDOCENTE_GRADO";
                        command.Parameters.AddWithValue("@IDDOCENTE_GRADO", docente_Grado.idDocente_Grado);
                        command.Parameters.AddWithValue("@ID_DOCENTE", docente_Grado.id_docente.id_docente);
                        command.Parameters.AddWithValue("@ID_GRADO", docente_Grado.id_grado.id_grado);

                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public int eliminarGradDocente(Docente_Grado docente_Grado)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE DOCENTE_GRADO SET ESTADO = 'LOCKED' WHERE IDDOCENTE_GRADO = @IDDOCENTE_GRADO";

                        command.Parameters.AddWithValue("@IDDOCENTE_GRADO", docente_Grado.idDocente_Grado);
                        //command.Parameters.AddWithValue("@ID_DOCENTE", docente_Grado.id_docente.id_docente);
                        //command.Parameters.AddWithValue("@ID_GRADO", docente_Grado.id_grado.id_grado);

                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public List<Docente> MostrarDocentesNotIn()
        {
            List<Docente> lstDocente = new List<Docente>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM docente d where d.estado = 'OPEN' AND d.id_docente not in ((SELECT dg.id_docente FROM DOCENTE_GRADO dg  INNER JOIN Docente d on dg.id_docente = d.id_docente  WHERE dg.ESTADO = 'OPEN')) ORDER BY nombre ASC";
                       

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente docente = new Docente();

                                    docente.nombre = dr["nombre"].ToString();
                                    docente.apellido = dr["apellido"].ToString();
                                    docente.id_docente = int.Parse(dr["id_docente"].ToString());

                                    lstDocente.Add(docente);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstDocente;
        }



        public List<Grado> MostrarGradoNotIn()
        {
            List<Grado> lstGrado = new List<Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM grado g inner join seccion s on g.id_seccion = s.id_seccion where g.estado = 'OPEN' AND g.id_grado not in ((SELECT dg.id_grado FROM DOCENTE_GRADO dg INNER JOIN Grado g on g.id_grado = dg.id_grado WHERE dg.ESTADO = 'OPEN')) ORDER BY id_grado ASC";


                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Grado grado = new Grado();
                                    Seccion seccion = new Seccion();


                                    grado.id_grado = int.Parse(dr["id_grado"].ToString());
                                    grado.grado = dr["grado"].ToString();
                                    seccion.id_seccion = int.Parse(dr["id_seccion"].ToString());
                                    seccion.seccion = dr["seccion"].ToString();

                                    grado.id_seccion = seccion;

                                    lstGrado.Add(grado);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstGrado;
        }

        public int eliminarDocenteGrado(Docente_Grado docenteGrado)
        {
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "DELETE FROM DOCENTE_GRADO WHERE idDocente_Grado = @ID_dm";
                        command.Parameters.AddWithValue("@ID_dm", docenteGrado.idDocente_Grado);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }


    }
}
