﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaDeNotas.Model;
using System.Data.SqlClient;
using System.Windows;
using System.Data;
using System.Data.Common;

namespace SistemaDeNotas.DAO
{
    class DaoGrado
    {
        public DaoGrado()
        {

        }


        public List<Grado> MostrarGrados()
        {
            List<Grado> lstGrado = new List<Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM GRADO g INNER JOIN Seccion s on s.id_seccion=g.id_seccion WHERE g.ESTADO = 'OPEN' ORDER BY g.GRADO ASC";

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Grado grado = new Grado();
                                    Seccion seccion = new Seccion();
                                    grado.id_grado = int.Parse(dr["id_grado"].ToString());
                                    grado.grado = dr["grado"].ToString();
                                    grado.cantidad_alumno = int.Parse(dr["cantidad_alumno"].ToString());
                                    seccion.seccion = dr["seccion"].ToString();
                                    seccion.id_seccion = int.Parse(dr["id_seccion"].ToString());
                                    grado.id_seccion = seccion;
                                    lstGrado.Add(grado);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstGrado;
        }

        public List<Grado> MostrarGradosById(int idMateria, int idDocente)
        {
            List<Grado> lstGrado = new List<Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText =
                            "SELECT * FROM Docente_Materia_Grado dmg INNER JOIN Grado g on dmg.id_grado = g.id_grado INNER JOIN  Seccion s on g.id_seccion = s.id_seccion where dmg.id_materia = @ID_MATERIA AND dmg.id_docente = @ID_DOCENTE AND dmg.estado = 'OPEN' and g.estado = 'OPEN' and s.estado = 'OPEN'";
                        command.Parameters.AddWithValue("@ID_MATERIA", idMateria);
                        command.Parameters.AddWithValue("@ID_DOCENTE", idDocente);
                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Grado grado = new Grado();
                                    Seccion seccion = new Seccion();
                                    grado.id_grado = int.Parse(dr["id_grado"].ToString());
                                    grado.grado = dr["grado"].ToString();
                                    seccion.seccion = dr["seccion"].ToString();
                                    grado.id_seccion = seccion;
                                    lstGrado.Add(grado);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstGrado;
        }

        public int CantidadEstudiantesxGradoById(int id_grado)
        {
            int cantidad = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM GRADO g INNER JOIN Seccion s on s.id_seccion=g.id_seccion WHERE g.ESTADO = 'OPEN' AND s.estado='OPEN' AND g.id_grado=@ID_GRADO ORDER BY g.GRADO ASC";
                        command.Parameters.AddWithValue("@ID_GRADO", id_grado);

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    cantidad = int.Parse(dr["cantidad_alumno"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return cantidad;
        }

        public List<Grado> MostrarGradoById(int id_grado)
        {
            List<Grado> lstGrado = new List<Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM GRADO g INNER JOIN Seccion s on s.id_seccion=g.id_seccion WHERE g.ESTADO = 'OPEN' AND s.estado='OPEN' AND g.id_grado=@ID_GRADO ORDER BY g.GRADO ASC";
                        command.Parameters.AddWithValue("@ID_GRADO", id_grado);
                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Grado grado = new Grado();
                                    Seccion seccion = new Seccion();
                                    grado.id_grado = int.Parse(dr["id_grado"].ToString());
                                    grado.grado = dr["grado"].ToString();
                                    seccion.seccion = dr["seccion"].ToString();
                                    grado.id_seccion = seccion;
                                    lstGrado.Add(grado);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstGrado;
        }

        public List<Seccion> MostrarSeccionByIdgrado()
        {
            List<Seccion> lstSeccion = new List<Seccion>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM seccion";


                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {

                                    Seccion seccion = new Seccion();

                                    seccion.seccion = dr["seccion"].ToString();
                                    seccion.id_seccion = int.Parse(dr["id_seccion"].ToString());

                                    lstSeccion.Add(seccion);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstSeccion;
        }

        public int InsertarGrado(Grado grado)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "INSERT INTO grado(GRADO,ID_SECCION,CANTIDAD_ALUMNO,ESTADO) values(@GRADO,@ID_SECCION,@CANTIDAD_ALUMNO,'OPEN')";

                        command.Parameters.AddWithValue("@GRADO", grado.grado);
                        command.Parameters.AddWithValue("@ID_SECCION", grado.id_seccion.id_seccion);
                        command.Parameters.AddWithValue("@CANTIDAD_ALUMNO", grado.cantidad_alumno);


                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public int actualizarGrado(Grado grado)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "update grado SET grado = @GRADO, id_seccion=@SECCION, cantidad_alumno=@CANTIDAD_ALUMNOS  WHERE ID_GRADO = @ID_GRADO";
                        command.Parameters.AddWithValue("@ID_GRADO", grado.id_grado);
                        command.Parameters.AddWithValue("@GRADO", grado.grado);
                        command.Parameters.AddWithValue("@SECCION", grado.id_seccion.id_seccion);
                        command.Parameters.AddWithValue("@CANTIDAD_ALUMNOS", grado.cantidad_alumno);



                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public int eliminarGrado(Grado grado)
        {
            int response = 0;
            DaoEncrypt encrypt = new DaoEncrypt();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE grado SET ESTADO = 'LOCKED' WHERE ID_GRADO = @ID_GRADO";
                        command.Parameters.AddWithValue("@ID_GRADO", grado.id_grado);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public List<Grado> buscarGrado(Grado mat)
        {
            List<Grado> lstgrado = new List<Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {

                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT TOP 1 * FROM GRADO WHERE GRADO = @GRADO";
                        command.Parameters.AddWithValue("@ID_GRADO", mat.id_grado);
                        command.Parameters.AddWithValue("@GRADO", mat.grado);

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Grado grado = new Grado();
                                    grado.id_grado = int.Parse(dr["id_grado"].ToString());
                                    grado.grado = dr["grado"].ToString();

                                    lstgrado.Add(grado);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstgrado;
        }



    }
}