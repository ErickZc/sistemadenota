﻿using SistemaDeNotas.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Windows;

namespace SistemaDeNotas.DAO
{
    class DaoDocenteMateriaG
    {
        public DaoDocenteMateriaG()
        {

        }

        public List<Docente_Materia_Grado> listarMateriaById(int idDocente)
        {
            List<Docente_Materia_Grado> lstMateria = new List<Docente_Materia_Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT DISTINCT  m.id_materia,m.nombre_materia FROM Docente_Materia_Grado dmg INNER JOIN Materia m on dmg.id_materia =  m.id_materia where dmg.id_docente = @ID_DOCENTE and m.estado = 'OPEN' and dmg.estado = 'OPEN';";
                        command.Parameters.AddWithValue("@ID_DOCENTE", idDocente);
                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente_Materia_Grado dmg = new Docente_Materia_Grado();
                                    Materia materia = new Materia();
                                    //dmg.idDocente_Materia = int.Parse(dr["idDocente_Materia"].ToString());
                                    materia.id_materia = int.Parse(dr["id_materia"].ToString());
                                    materia.nombre_materia = dr["nombre_materia"].ToString();
                                    dmg.id_materia = materia;
                                    lstMateria.Add(dmg);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstMateria;
        }
        public List<Docente_Materia_Grado> mostrarMateriasGrado()
        {
            List<Docente_Materia_Grado> listamateria = new List<Docente_Materia_Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * from  Docente_Materia_Grado dmg inner join Docente d on d.id_docente =dmg.id_docente inner join Materia m on m.id_materia = dmg.id_materia inner join grado g on g.id_grado = dmg.id_grado inner join seccion s on g.id_seccion = s.id_seccion where dmg.estado = 'OPEN'";


                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente_Materia_Grado dmg = new Docente_Materia_Grado();
                                    Materia materia = new Materia();
                                    Grado grado = new Grado();
                                    Docente docente = new Docente();
                                    Seccion seccion = new Seccion();
                                    dmg.idDocente_Materia = int.Parse(dr["idDocente_Materia"].ToString());
                                    materia.id_materia = int.Parse(dr["id_materia"].ToString());
                                    materia.nombre_materia = dr["nombre_materia"].ToString();
                                    grado.id_grado = int.Parse(dr["id_grado"].ToString());
                                    grado.grado = dr["grado"].ToString();
                                    docente.id_docente = int.Parse(dr["id_docente"].ToString());
                                    docente.nombre = dr["nombre"].ToString();
                                    docente.apellido = dr["apellido"].ToString();
                                    seccion.id_seccion = int.Parse(dr["id_seccion"].ToString());
                                    seccion.seccion = dr["seccion"].ToString();
                                    dmg.id_grado = grado;
                                    dmg.id_docente = docente;
                                    dmg.id_materia = materia;
                                    dmg.id_grado.id_seccion = seccion;
                                    
                                    listamateria.Add(dmg);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return listamateria;
        }
        public int InsertarMateriasGrados(Docente_Materia_Grado docente_Materia_Grado)
        {
            DAO.DaoEncrypt en = new DaoEncrypt();
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) values(@id_docente,@id_materia,@id_grado,'18/09/2021','OPEN')";
                        command.Parameters.AddWithValue("@id_docente", docente_Materia_Grado.id_docente.id_docente);
                        command.Parameters.AddWithValue("@id_materia", docente_Materia_Grado.id_materia.id_materia);
                        command.Parameters.AddWithValue("@id_grado", docente_Materia_Grado.id_grado.id_grado);
                        
                        
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return response;
        }
        public int ActualizarMaterias(Docente_Materia_Grado docente_Materia_Grado)
        {
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE Docente_Materia_Grado SET id_docente = @id_docente, id_materia = @id_materia,id_grado = @id_grado WHERE idDocente_Materia = @idDocente_Materia";
                        command.Parameters.AddWithValue("@id_docente", docente_Materia_Grado.id_docente.id_docente);
                        command.Parameters.AddWithValue("@id_materia", docente_Materia_Grado.id_materia.id_materia);
                        command.Parameters.AddWithValue("@id_grado", docente_Materia_Grado.id_grado.id_grado);
                        command.Parameters.AddWithValue("@idDocente_Materia", docente_Materia_Grado.idDocente_Materia);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }
        public int EliminarRegistro(Docente_Materia_Grado estudiante)
        {
            int response = 0;
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();
                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE DOCENTE_MATERIA_GRADO SET ESTADO = 'LOCKED' WHERE idDocente_Materia = @idDocente_Materia";
                        command.Parameters.AddWithValue("@idDocente_Materia", estudiante.idDocente_Materia);
                        response = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return response;
        }

        public List<Docente_Materia_Grado> buscarDocenteGrado(Docente_Materia_Grado mat)
        {
            List<Docente_Materia_Grado> lstDocenteGrado = new List<Docente_Materia_Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {

                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT TOP 1 * FROM DOCENTE_GRADO WHERE idDocente_Materia = @idDocente_Materia";
                        command.Parameters.AddWithValue("@idDocente_Materia", mat.idDocente_Materia);
                        ///command.Parameters.AddWithValue("@ID_DOCENTE", mat.id_docente);
                        //command.Parameters.AddWithValue("@ID_GRADO", mat.id_grado);

                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                     Docente_Materia_Grado docente = new Docente_Materia_Grado ();
                                    docente.idDocente_Materia = int.Parse(dr["idDocente_Materia"].ToString());
                                    // DocenteGrado.id_grado = int.Parse(dr["id_grado"].ToString());

                                    lstDocenteGrado.Add(docente);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return lstDocenteGrado;
        }

        public bool listarMateriasxGrado(int _materia, int _grado)
        {
            List<Docente_Materia_Grado> lstMateriaxGrado = new List<Docente_Materia_Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM Docente_Materia_Grado WHERE id_materia = @id_materia AND id_grado = @id_grado AND ESTADO = 'OPEN';";
                        command.Parameters.AddWithValue("@id_materia", _materia);
                        command.Parameters.AddWithValue("@id_grado", _grado);
                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente_Materia_Grado dmg = new Docente_Materia_Grado();
                                    
                                    dmg.idDocente_Materia = int.Parse(dr["idDocente_Materia"].ToString());
                                    lstMateriaxGrado.Add(dmg);
                                }
                            }
                        }

                        if (lstMateriaxGrado.Count > 0)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                        

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return false;
        }

        public bool listarMateriasxGradoNotIn(int _materia, int _grado, int _idDocente)
        {
            List<Docente_Materia_Grado> lstMateriaxGrado = new List<Docente_Materia_Grado>();
            try
            {
                using (var conection = new SqlConnection(Login.Properties.Settings.Default.conexionDB))
                {
                    conection.Open();

                    using (var command = conection.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM Docente_Materia_Grado WHERE id_materia = @id_materia AND id_grado = @id_grado AND ESTADO = 'OPEN' and id_docente NOT IN (@id_doc);";
                        command.Parameters.AddWithValue("@id_materia", _materia);
                        command.Parameters.AddWithValue("@id_grado", _grado);
                        command.Parameters.AddWithValue("@id_doc", _idDocente);
                        using (DbDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    Docente_Materia_Grado dmg = new Docente_Materia_Grado();

                                    dmg.idDocente_Materia = int.Parse(dr["idDocente_Materia"].ToString());
                                    lstMateriaxGrado.Add(dmg);
                                }
                            }
                        }

                        if (lstMateriaxGrado.Count > 0)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return false;
        }

    }
}
