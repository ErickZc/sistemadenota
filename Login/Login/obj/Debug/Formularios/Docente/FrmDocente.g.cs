﻿#pragma checksum "..\..\..\..\Formularios\Docente\FrmDocente.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "216F02F32BC6DF4F73C39BD67D603F67879AD317"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using RootLibrary.WPF.Localization;
using SistemaDeNotas.Formularios.Docente;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Converters;
using Xceed.Wpf.Toolkit.Core;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Mag.Converters;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace SistemaDeNotas.Formularios.Docente {
    
    
    /// <summary>
    /// FrmDocente
    /// </summary>
    public partial class FrmDocente : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 16 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tbData;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAceptarHidden;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAceptar;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancelar;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLimpiar;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdNuevo4;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image5;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy1;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy2;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy3;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy4;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy5;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy6;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy7;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy8;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy9;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy11;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtCorreo;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtUsuario;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtApellido;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtID;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtDireccion;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNombre;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rdMasculino;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rdFemenino;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNuevo;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdNuevo;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image2;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnModificar;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdNuevo1;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image1;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEliminar;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdNuevo2;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image3;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuardar;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdNuevo3;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image4;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgDatos;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy12;
        
        #line default
        #line hidden
        
        
        #line 125 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy10;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.MaskedTextBox txtDui;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.MaskedTextBox txtNit;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.MaskedTextBox txtTelefono;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Login;component/formularios/docente/frmdocente.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.tbData = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.btnAceptarHidden = ((System.Windows.Controls.Button)(target));
            
            #line 19 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
            this.btnAceptarHidden.Click += new System.Windows.RoutedEventHandler(this.btnAceptarHidden_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btnAceptar = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.btnCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
            this.btnCancelar.Click += new System.Windows.RoutedEventHandler(this.btnCancelar_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnLimpiar = ((System.Windows.Controls.Button)(target));
            
            #line 31 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
            this.btnLimpiar.Click += new System.Windows.RoutedEventHandler(this.btnLimpiar_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.grdNuevo4 = ((System.Windows.Controls.Grid)(target));
            return;
            case 7:
            this.image5 = ((System.Windows.Controls.Image)(target));
            return;
            case 8:
            this.label_Copy1 = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.label_Copy2 = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.label_Copy3 = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.label_Copy4 = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.label_Copy5 = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.label_Copy6 = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.label_Copy7 = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.label_Copy8 = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.label_Copy9 = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.label_Copy11 = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.txtCorreo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 19:
            this.txtUsuario = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.txtApellido = ((System.Windows.Controls.TextBox)(target));
            return;
            case 21:
            this.txtID = ((System.Windows.Controls.TextBox)(target));
            return;
            case 22:
            this.txtDireccion = ((System.Windows.Controls.TextBox)(target));
            return;
            case 23:
            this.txtNombre = ((System.Windows.Controls.TextBox)(target));
            return;
            case 24:
            this.rdMasculino = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 25:
            this.rdFemenino = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 26:
            this.btnNuevo = ((System.Windows.Controls.Button)(target));
            
            #line 62 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
            this.btnNuevo.Click += new System.Windows.RoutedEventHandler(this.btnNuevo_Click);
            
            #line default
            #line hidden
            return;
            case 27:
            this.grdNuevo = ((System.Windows.Controls.Grid)(target));
            return;
            case 28:
            this.image2 = ((System.Windows.Controls.Image)(target));
            return;
            case 29:
            this.btnModificar = ((System.Windows.Controls.Button)(target));
            
            #line 77 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
            this.btnModificar.Click += new System.Windows.RoutedEventHandler(this.btnModificar_Click);
            
            #line default
            #line hidden
            return;
            case 30:
            this.grdNuevo1 = ((System.Windows.Controls.Grid)(target));
            return;
            case 31:
            this.image1 = ((System.Windows.Controls.Image)(target));
            return;
            case 32:
            this.btnEliminar = ((System.Windows.Controls.Button)(target));
            
            #line 88 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
            this.btnEliminar.Click += new System.Windows.RoutedEventHandler(this.btnEliminar_Click);
            
            #line default
            #line hidden
            return;
            case 33:
            this.grdNuevo2 = ((System.Windows.Controls.Grid)(target));
            return;
            case 34:
            this.image3 = ((System.Windows.Controls.Image)(target));
            return;
            case 35:
            this.btnGuardar = ((System.Windows.Controls.Button)(target));
            
            #line 99 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
            this.btnGuardar.Click += new System.Windows.RoutedEventHandler(this.btnGuardar_Click);
            
            #line default
            #line hidden
            return;
            case 36:
            this.grdNuevo3 = ((System.Windows.Controls.Grid)(target));
            return;
            case 37:
            this.image4 = ((System.Windows.Controls.Image)(target));
            return;
            case 38:
            this.dgDatos = ((System.Windows.Controls.DataGrid)(target));
            
            #line 110 "..\..\..\..\Formularios\Docente\FrmDocente.xaml"
            this.dgDatos.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dgDatos_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 39:
            this.label_Copy12 = ((System.Windows.Controls.Label)(target));
            return;
            case 40:
            this.label_Copy10 = ((System.Windows.Controls.Label)(target));
            return;
            case 41:
            this.txtDui = ((Xceed.Wpf.Toolkit.MaskedTextBox)(target));
            return;
            case 42:
            this.txtNit = ((Xceed.Wpf.Toolkit.MaskedTextBox)(target));
            return;
            case 43:
            this.txtTelefono = ((Xceed.Wpf.Toolkit.MaskedTextBox)(target));
            return;
            case 44:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 45:
            this.label_Copy = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

