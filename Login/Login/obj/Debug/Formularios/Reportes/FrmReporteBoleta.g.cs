﻿#pragma checksum "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "A63246BFF82CEB8F9B0E3CF12114430C7BE00AD5"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using RootLibrary.WPF.Localization;
using SAPBusinessObjects.WPF.Viewer;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SistemaDeNotas.Formularios.Reportes {
    
    
    /// <summary>
    /// FrmReporteBoleta
    /// </summary>
    public partial class FrmReporteBoleta : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 16 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbGrados;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbEstudiantes;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBuscar;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdNuevo3;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image4;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SAPBusinessObjects.WPF.Viewer.CrystalReportsViewer Report;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBuscar2;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdNuevo1;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image1;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLimpiar;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdNuevo2;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image2;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy1;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy2;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tbData;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAceptarHidden;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAceptar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Login;component/formularios/reportes/frmreporteboleta.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.label_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.cmbGrados = ((System.Windows.Controls.ComboBox)(target));
            
            #line 18 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
            this.cmbGrados.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cmbGrados_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.cmbEstudiantes = ((System.Windows.Controls.ComboBox)(target));
            
            #line 19 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
            this.cmbEstudiantes.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cmbEstudiantes_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnBuscar = ((System.Windows.Controls.Button)(target));
            
            #line 20 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
            this.btnBuscar.Click += new System.Windows.RoutedEventHandler(this.btnBuscar_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.grdNuevo3 = ((System.Windows.Controls.Grid)(target));
            return;
            case 7:
            this.image4 = ((System.Windows.Controls.Image)(target));
            return;
            case 8:
            this.Report = ((SAPBusinessObjects.WPF.Viewer.CrystalReportsViewer)(target));
            return;
            case 9:
            this.btnBuscar2 = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
            this.btnBuscar2.Click += new System.Windows.RoutedEventHandler(this.btnBuscar2_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.grdNuevo1 = ((System.Windows.Controls.Grid)(target));
            return;
            case 11:
            this.image1 = ((System.Windows.Controls.Image)(target));
            return;
            case 12:
            this.btnLimpiar = ((System.Windows.Controls.Button)(target));
            
            #line 44 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
            this.btnLimpiar.Click += new System.Windows.RoutedEventHandler(this.btnLimpiar_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.grdNuevo2 = ((System.Windows.Controls.Grid)(target));
            return;
            case 14:
            this.image2 = ((System.Windows.Controls.Image)(target));
            return;
            case 15:
            this.label_Copy1 = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.label_Copy2 = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.tbData = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 18:
            this.btnAceptarHidden = ((System.Windows.Controls.Button)(target));
            
            #line 67 "..\..\..\..\Formularios\Reportes\FrmReporteBoleta.xaml"
            this.btnAceptarHidden.Click += new System.Windows.RoutedEventHandler(this.btnAceptarHidden_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.btnAceptar = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

