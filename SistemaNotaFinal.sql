create database SistemaNota;
use SistemaNota
go

--drop database SistemaNota;

create table Rol_Usuario(
id_rol int identity(1,1),
rol varchar(50)
);

create table Docente(
id_docente int identity(1,1),
nombre varchar(50),
apellido varchar(50),
direccion varchar(100),
telefono varchar(15),
dui varchar(15),
nit varchar(20),
correo varchar(50),
fecha_creacion varchar(50),
estado varchar(25),
usuario varchar(50),
password varchar(150),
id_rol int,
genero varchar(25)
);

create table Administrador_Sistema(
id_admin int identity(1,1),
nombre varchar(60),
apellido varchar(60),
username varchar(50),
password varchar(150),
telefono varchar(10),
correo varchar(50),
dui varchar(10),
estado varchar(20)
);

create table Docente_Materia_Grado(
idDocente_Materia int identity(1,1),
id_docente int,
id_materia int,
id_grado int,
fecha varchar(50),
estado varchar(20)
);

create table Docente_Grado(
idDocente_Grado int identity(1,1),
id_docente int,
id_grado int,
fecha varchar(50),
estado varchar(20)
);

create table Grado(
id_grado int identity(1,1),
grado varchar(100),
id_seccion int,
cantidad_alumno int,
estado varchar(20)
);

create table Seccion(
id_seccion int identity(1,1),
seccion varchar(10),
estado varchar(20)
);

create table Estudiante(
id_estudiante int identity(1,1),
nombre varchar(50),
apellido varchar(50),
direccion varchar(50),
padreResponsable varchar(100),
telefono varchar(15),
estado_promedio varchar(20),
fecha_creacion varchar(50),
estado varchar(50),
id_grado int
);

create table Nota(
id_nota int identity(1,1),
id_estudiante int,
id_materia int,
actividad1 decimal(4,2),
actividad2 decimal(4,2),
actividad3 decimal(4,2),
examen decimal(4,2),
promedio decimal(4,2),
id_periodo int
);

create table Periodo(
id_periodo int identity(1,1),
periodo varchar(50),
estado varchar(50)
);

create table Materia(
id_materia int identity(1,1),
nombre_materia varchar(50),
estado varchar(50)
);

create table ConfiguracionSistema(
idConfiguracion int identity(1,1),
institucion varchar(100),
director varchar(50),
fecha_actual varchar(50),
fecha_siguiente varchar(50),
imagen varchar(50)
);

-- RELACIONES
-- LLAVES PRIMARIAS
ALTER TABLE ROL_USUARIO ADD CONSTRAINT PK_ID_ROL_USUARIO PRIMARY KEY(id_rol);
ALTER TABLE DOCENTE ADD CONSTRAINT PK_ID_DOCENTE PRIMARY KEY(id_docente);
ALTER TABLE DOCENTE_GRADO ADD CONSTRAINT PK_ID_DOCENTE_GRADO PRIMARY KEY(idDocente_Grado);
ALTER TABLE GRADO ADD CONSTRAINT PK_ID_GRADO PRIMARY KEY(id_grado);
ALTER TABLE ESTUDIANTE ADD CONSTRAINT PK_ID_ESTUDIANTE PRIMARY KEY(id_estudiante);
ALTER TABLE NOTA ADD CONSTRAINT PK_ID_NOTA PRIMARY KEY(id_nota);
ALTER TABLE MATERIA ADD CONSTRAINT PK_ID_MATERIA PRIMARY KEY(id_materia);
ALTER TABLE PERIODO ADD CONSTRAINT PK_ID_PERIODO PRIMARY KEY(id_periodo);
ALTER TABLE DOCENTE_MATERIA_GRADO ADD CONSTRAINT PK_ID_DOCENTE_MATERIA_GRADO PRIMARY KEY(idDocente_Materia);
ALTER TABLE SECCION ADD CONSTRAINT PK_ID_SECCION PRIMARY KEY(id_seccion);

-- LLAVES FORANEAS
ALTER TABLE DOCENTE ADD FOREIGN KEY (id_rol) references ROL_USUARIO(id_rol);
ALTER TABLE DOCENTE_GRADO ADD FOREIGN KEY (id_docente) references DOCENTE(id_docente);
ALTER TABLE DOCENTE_GRADO ADD FOREIGN KEY (id_grado) references GRADO(id_grado);
ALTER TABLE ESTUDIANTE ADD FOREIGN KEY (id_grado) references GRADO(id_grado);
ALTER TABLE NOTA ADD FOREIGN KEY (id_estudiante) references ESTUDIANTE(id_estudiante);
ALTER TABLE NOTA ADD FOREIGN KEY (id_materia) references MATERIA(id_materia);
ALTER TABLE NOTA ADD FOREIGN KEY (id_periodo) references PERIODO(id_periodo);
ALTER TABLE DOCENTE_MATERIA_GRADO ADD FOREIGN KEY (id_materia) references MATERIA(id_materia);
ALTER TABLE DOCENTE_MATERIA_GRADO ADD FOREIGN KEY (id_docente) references DOCENTE(id_docente);
ALTER TABLE DOCENTE_MATERIA_GRADO ADD FOREIGN KEY (id_grado) references GRADO(id_grado);
ALTER TABLE GRADO ADD FOREIGN KEY (id_seccion) references SECCION(id_seccion);


-- INSERT

--ROL_USUARIO

INSERT INTO ROL_USUARIO VALUES('Administrador');
INSERT INTO ROL_USUARIO VALUES('Docente');

--Administrador_Sistema

insert Administrador_Sistema(nombre,apellido,username,password,telefono,correo,dui,estado) values('ERICK','ZALDANA','admin', 'MQAyADMANAA=','2318-2451','ecruz@gmail.com','12345678-9','OPEN');

--DOCENTE
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('CRISTIAN','ABDIN TATJÈ','MORAGUES , 10','2269-2231','52531986-9','860-353753-151-1','romeo@intercom.es','17/11/2020','OPEN','romeo','MQAyADMANAA=',2,'Masculino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('GUILLEM','CANELLAS GOMEZ','PROL. PADRÓ , 1, 3R., 2A.','2320-2837','54360946-8','1999-457888-217-9','dubai@gmail.com','17/11/2020','OPEN','dubai','MQAyADMANAA=',2,'Masculino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('DIMAS','HIDALGO ALTIMIRAS','FRANCESC DE VITÒRIA , 11, 4T 2A','2823-2926','97412353-4','4301-218757-273-9','ronc@cataloniamail.com','17/11/2020','OPEN','ronc','MQAyADMANAA=',2,'Masculino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('ANA INÉS','BASTARDAS FRANCH','ALBÉNIZ , 22, 2N.','2247-2921','28607322-6','3378-875280-276-4','sabrosa@hotmail.com','17/11/2020','OPEN','sabrosa','MQAyADMANAA=',2,'Femenino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('IVET','ABADIAS MASANA','TRES ROURES , 10, 4T 2A','2326-2908','55542216-4','4183-985092-242-8','salsa@cataloniamail.com','17/11/2020','OPEN','salsa','MQAyADMANAA=',2,'Femenino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('JÚLIA','AREVALO SANCHEZ','PROL. PADRÓ , 1, 2N., 1A.','2876-2338','57156098-6','5810-628770-795-2','omg@gmail.com','17/11/2020','OPEN','omg','MQAyADMANAA=',2,'Femenino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('DANIEL','ALINS MULET','ALBÉNIZ , 13, 2N., 1A.','2855-2618','5339115-6','4803-145899-203-4','sabrosito@gmail.com','17/11/2020','OPEN','sabrosito','MQAyADMANAA=',2,'Masculino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('ABEL','GARCIA GONZÁLEZ','FONT DEL GAT , 1','2952-2221','43063088-5','7734-670305-970-1','sincer@altecom.es','17/11/2020','OPEN','sincer','MQAyADMANAA=',2,'Masculino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('IRENE','ALVAREZ PARCERISA','MONTCAU , 1','2688-2311','6239555-4','6692-697695-523-1','sincera@hotmail.com','17/11/2020','OPEN','sincera','MQAyADMANAA=',2,'Masculino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('ADRIÀ','CASAS ANDRÉS','MONTURIOL , 1','2525-2512','10347350-7','1726-428277-805-7','suau@hotmail.com','17/11/2020','OPEN','suau','MQAyADMANAA=',2,'Femenino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('JAIRO','MORALES GESE','VILATORRADA , 6','2972-2598','89036656-3','9138-892230-956-5','superman@altecom.es','17/11/2020','OPEN','superman','MQAyADMANAA=',2,'Masculino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('CRISTINA','BARALDÉS MARTORELL','JAUME GALOBART , 11','2730-2972','91897599-1','8990-276778-794-6','superwoman@wandoo.es','17/11/2020','OPEN','superwoman','MQAyADMANAA=',2,'Femenino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('DAVID','AROCA GÓMEZ','SANT GENÍS , 25','2425-2741','36666735-5','2391-642271-640-4','tablon@hotmail.com','17/11/2020','OPEN','tablon','MQAyADMANAA=',2,'Masculino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('ADRIÀ','RUEDA ALVAREZ','VERGE DE FÀTIMA , 2, 3R., 1A.','2986-2722','32740538-2','5576-374840-708-2','tendre@terra.es','17/11/2020','OPEN','tendre','MQAyADMANAA=',2,'Femenino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('LUCIA','ALVAREZ DOMENECH','JAUME I , 10','2493-2845','74676153-1','1435-852413-812-1','teta@intercom.es','17/11/2020','OPEN','teta','MQAyADMANAA=',2,'Femenino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('CARLA','BOIX GONZÁLEZ','SANT JOAN , 0, C, 1R., B','2487-2844','1916948-5','4362-716835-434-6','tomasa@hotmail.com','17/11/2020','OPEN','tomasa','MQAyADMANAA=',2,'Femenino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('ADRIÀ','BARALDÉS MONRÓS','JACINT VERDAGUER , 13','2584-2945','63959466-8','2358-990278-34-1','labestia@gmail.com','17/11/2020','OPEN','labestia','MQAyADMANAA=',2,'Femenino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('MARTA','AGUILERA MERINO','SANT BENET , 28, 2N., 2A.','2453-2678','80111288-7','3153-266007-27-5','tremenda@altecom.es','17/11/2020','OPEN','tremenda','MQAyADMANAA=',2,'Femenino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('MARC','BAREA DHAENE','FONERIA , 12','2285-2235','88716667-5','7005-570655-990-1','tripa@intercom.es','17/11/2020','OPEN','tripa','MQAyADMANAA=',2,'Masculino');
insert into Docente(nombre,apellido,direccion,telefono,dui,nit,correo,fecha_creacion,estado,usuario,password,id_rol,genero) values('ALEX','BARROSO DHAENE','ALFONS XII , 9, 4T 1A','2858-2309','7607816-7','9646-794987-546-4','verruga@hotmail.com','17/11/2020','OPEN','verruga','MQAyADMANAA=',2,'Masculino');

-- PERIODO
INSERT INTO Periodo(periodo,estado) VALUES('PRIMER PERIODO','OPEN');
INSERT INTO Periodo(periodo,estado) VALUES('SEGUNDO PERIODO','OPEN');
INSERT INTO Periodo(periodo,estado) VALUES('TERCER PERIODO','OPEN');

-- SECCION
INSERT INTO Seccion(seccion,estado) VALUES('A','OPEN');
INSERT INTO Seccion(seccion,estado) VALUES('B','OPEN');

-- MATERIA
INSERT INTO Materia(nombre_materia,estado) VALUES('MATEMATICA','OPEN');
INSERT INTO Materia(nombre_materia,estado) VALUES('ESTUDIOS SOCIALES','OPEN');
INSERT INTO Materia(nombre_materia,estado) VALUES('CIENCIAS NATURALES','OPEN');
INSERT INTO Materia(nombre_materia,estado) VALUES('LENGUAJE Y LITERATURA','OPEN');
INSERT INTO Materia(nombre_materia,estado) VALUES('INGLES','OPEN');
INSERT INTO Materia(nombre_materia,estado) VALUES('EDUCACION FISICA','OPEN');

-- GRADO
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('PRIMERO',1,18,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('SEGUNDO',1,28,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('TERCERO',1,22,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('CUARTO',1,30,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('QUINTO',1,23,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('SEXTO',1,27,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('SEPTIMO',1,25,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('OCTAVO',1,15,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('NOVENO',1,20,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('PRIMERO',2,18,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('SEGUNDO',2,27,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('TERCERO',2,15,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('CUARTO',2,27,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('QUINTO',2,21,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('SEXTO',2,25,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('SEPTIMO',2,24,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('OCTAVO',2,19,'OPEN');
INSERT INTO Grado(grado,id_seccion,cantidad_alumno,estado) VALUES('NOVENO',2,17,'OPEN');


-- ESTUDIANTE
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('DANIEL','ACEVEDO JHONG','PADRÓ , 109','ACEVEDO JHONG PADRÓ , 109','2772-2437','EN CURSO','17/11/2020','OPEN',1);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MIGUELVICENTE','AGURTO RONDOY','CASA CORDELLAS , ','AGURTO RONDOY CASA CORDELLAS , ','2861-2970','EN CURSO','17/11/2020','OPEN',1);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CHRISTIAN NELSON','ALCALÁ NEGRÓN','DOCTOR FLEMING , 11','ALCALÁ NEGRÓN DOCTOR FLEMING , 11','2896-2400','EN CURSO','17/11/2020','OPEN',1);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('RAUL EDUARDO','ALMORA HERNANDEZ','BERTRAND I SERRA , 11, 3R.','ALMORA HERNANDEZ BERTRAND I SERRA , 11, 3R.','2343-2784','EN CURSO','17/11/2020','OPEN',1);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JORGE ','ALOSILLA VELAZCO VERA','CARRIÓ , 12, 5È A','ALOSILLA VELAZCO VERA CARRIÓ , 12, 5È A','2382-2371','EN CURSO','17/11/2020','OPEN',1);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('VICTOR','ALVA CAMPOS','PIRINEUS , 10','ALVA CAMPOS PIRINEUS , 10','2516-2937','EN CURSO','17/11/2020','OPEN',1);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JAVIER','AREVALO LOPEZ','JACINT VERDAGUER , 43','AREVALO LOPEZ JACINT VERDAGUER , 43','2540-2977','EN CURSO','17/11/2020','OPEN',1);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ROSARIO','ARIAS HERNANDEZ','NOU , 9, 2N.','ARIAS HERNANDEZ NOU , 9, 2N.','2516-2518','EN CURSO','17/11/2020','OPEN',1);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('EFRAÍN ','ARROYO RAMÍREZ','JACINT VERDAGUER , 52, 3R, 1A.','ARROYO RAMÍREZ JACINT VERDAGUER , 52, 3R, 1A.','2836-2752','EN CURSO','17/11/2020','OPEN',1);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARCO TULIO','ALOCEN BARRERA','JOAN MIRÓ , 10','ALOCEN BARRERA JOAN MIRÓ , 10','2263-2710','EN CURSO','17/11/2020','OPEN',1);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CESAR','BAIOCCHI URETA','JAUME GALOBART , 12','BAIOCCHI URETA JAUME GALOBART , 12','2762-2983','EN CURSO','17/11/2020','OPEN',1);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ISELA FLOR','BAYLÓN ROJAS','PINTOR SERT , 12, 1R., 1A.','BAYLÓN ROJAS PINTOR SERT , 12, 1R., 1A.','2856-2402','EN CURSO','17/11/2020','OPEN',1);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('LEONCIA','BEDOYA CASTILLO','BELLAVISTA , 30','BEDOYA CASTILLO BELLAVISTA , 30','2455-2615','EN CURSO','17/11/2020','OPEN',1);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('LUZ MARINA','BEDREGAL CANALES','MONTURIOL , 10, 1R.','BEDREGAL CANALES MONTURIOL , 10, 1R.','2597-2914','EN CURSO','17/11/2020','OPEN',2);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('RAMIRO ALBERTO','BEJAR TORRES','JACINT VERDAGUER , 52, 2N., 4A.','BEJAR TORRES JACINT VERDAGUER , 52, 2N., 4A.','2781-2778','EN CURSO','17/11/2020','OPEN',2);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JAVIER','BENAVIDES ESPEJO','CASA NOVA , ','BENAVIDES ESPEJO CASA NOVA , ','2878-2640','EN CURSO','17/11/2020','OPEN',2);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('NELSON','BOZA SOLIS','DE LA CAÇA , 12, 1R., C','BOZA SOLIS DE LA CAÇA , 12, 1R., C','2363-2977','EN CURSO','17/11/2020','OPEN',2);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CIELITO MERCEDES','CALLE BETANCOURT','PINTOR SERT , 12, 2N., 1A.','CALLE BETANCOURT PINTOR SERT , 12, 2N., 1A.','2228-2990','EN CURSO','17/11/2020','OPEN',2);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ISABEL FLORISA','CARAZA VILLEGAS','CASA SARA , ','CARAZA VILLEGAS CASA SARA , ','2853-2314','EN CURSO','17/11/2020','OPEN',2);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GIZELLA','CARRERA ABANTO','ARTÈS , 1, 1R, 1A.','CARRERA ABANTO ARTÈS , 1, 1R, 1A.','2776-2614','EN CURSO','17/11/2020','OPEN',2);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ESTALINS','CARRILLO SEGURA','GENERAL PRIM , 11, 2N.','CARRILLO SEGURA GENERAL PRIM , 11, 2N.','2377-2361','EN CURSO','17/11/2020','OPEN',2);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JORGE AUGUSTO','CARRIÓN NEIRA','CAU DE LA GUINEU , 4','CARRIÓN NEIRA CAU DE LA GUINEU , 4','2566-2417','EN CURSO','17/11/2020','OPEN',2);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GUILLERMO ','CASAPIA VALDIVIA','JOAN SANMARTÍ , 12, 2N., 2A.','CASAPIA VALDIVIA JOAN SANMARTÍ , 12, 2N., 2A.','2810-2405','EN CURSO','17/11/2020','OPEN',2);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ZARITA','CHANCOS MENDOZA','PROL. PADRÓ , 1, 3R. 1A.','CHANCOS MENDOZA PROL. PADRÓ , 1, 3R. 1A.','2826-2720','EN CURSO','17/11/2020','OPEN',2);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CARLOS','CHIRINOS LACOTERA','SALLENT , 22, 2N.','CHIRINOS LACOTERA SALLENT , 22, 2N.','2822-2386','EN CURSO','17/11/2020','OPEN',2);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('DORIS','CORES MORENO','JOAN MIRÓ , 3','CORES MORENO JOAN MIRÓ , 3','2997-2360','EN CURSO','17/11/2020','OPEN',2);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARIBEL CORINA','CORTEZ LOZANO','LLUÍS CASTELLS , 12, 2N.','CORTEZ LOZANO LLUÍS CASTELLS , 12, 2N.','2288-2296','EN CURSO','17/11/2020','OPEN',3);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ANGEL','CRISPIN QUISPE','SANT VALENTÍ , 12, 1R.','CRISPIN QUISPE SANT VALENTÍ , 12, 1R.','2927-2311','EN CURSO','17/11/2020','OPEN',3);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ANTONIO ','DE LOAYZA CONTERNO','ÀNGEL GUIMERÀ , 43, 2N','DE LOAYZA CONTERNO ÀNGEL GUIMERÀ , 43, 2N','2267-2372','EN CURSO','17/11/2020','OPEN',3);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ANA MARIA','DIAZ SALINAS','JAUME GALOBART , 11','DIAZ SALINAS JAUME GALOBART , 11','2274-2509','EN CURSO','17/11/2020','OPEN',3);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ANTONIO ','DUEÑAS ARISTISABAL','AVINGUDA TRES , 3, 1R., 1A.','DUEÑAS ARISTISABAL AVINGUDA TRES , 3, 1R., 1A.','2975-2930','EN CURSO','17/11/2020','OPEN',3);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('YULIANA','ESPINOZA ARANA','JACINT VERDAGUER , 52, 2N., 1A.','ESPINOZA ARANA JACINT VERDAGUER , 52, 2N., 1A.','2966-2378','EN CURSO','17/11/2020','OPEN',3);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CARLOS ENRIQUE','FERNANDEZ GUZMAN','DIPUTACIÓ , 10','FERNANDEZ GUZMAN DIPUTACIÓ , 10','2942-2249','EN CURSO','17/11/2020','OPEN',3);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ESTHER AURORA','FERNANDEZ MATTA','VIC , 39, 1R., 2A.','FERNANDEZ MATTA VIC , 39, 1R., 2A.','2979-2903','EN CURSO','17/11/2020','OPEN',3);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('OLGA','FERRO SALAS','GERMAN DURAN , 21','FERRO SALAS GERMAN DURAN , 21','2205-2388','EN CURSO','17/11/2020','OPEN',3);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('EDWIN','FLORES ROMERO','BELLAVISTA , 30','FLORES ROMERO BELLAVISTA , 30','2811-2276','EN CURSO','17/11/2020','OPEN',3);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ROBERTO','GAMARRA ASTETE','NOU , 7, 2N.','GAMARRA ASTETE NOU , 7, 2N.','2660-2423','EN CURSO','17/11/2020','OPEN',3);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GLORIA','GAMIO LOZANO','MANELIC , 1','GAMIO LOZANO MANELIC , 1','2961-2550','EN CURSO','17/11/2020','OPEN',3);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MIRIAM','GARCÍA PERALTA','DE LA PESCA , 1, 1R., 1A.','GARCÍA PERALTA DE LA PESCA , 1, 1R., 1A.','2544-2594','EN CURSO','17/11/2020','OPEN',3);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ARTURO','GONZALES DEL VALLE MAGUIÑO','PIRINEUS , 34','GONZALES DEL VALLE MAGUIÑO PIRINEUS , 34','2221-2824','EN CURSO','17/11/2020','OPEN',4);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARLENE VICTORIA','GONZALES HUILCA','PROL. JACINT VERDAGUER , 1, 2N., 2A.','GONZALES HUILCA PROL. JACINT VERDAGUER , 1, 2N., 2A.','2413-2952','EN CURSO','17/11/2020','OPEN',4);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ELSA PATRICIA','GONZALES MEDINA','MALLORCA , 11','GONZALES MEDINA MALLORCA , 11','2438-2904','EN CURSO','17/11/2020','OPEN',4);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JAVIER','GUTIERREZ VELEZ','SANT BENET , 12, 2N.','GUTIERREZ VELEZ SANT BENET , 12, 2N.','2418-2235','EN CURSO','17/11/2020','OPEN',4);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ELENA ROSAVELT','GUZMAN CHINAG','PROL. PADRÓ , 1, 2N., 2A.','GUZMAN CHINAG PROL. PADRÓ , 1, 2N., 2A.','2590-2424','EN CURSO','17/11/2020','OPEN',4);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CLARA','GUZMAN QUISPE','ARTÈS , 1, 2N., 2A.','GUZMAN QUISPE ARTÈS , 1, 2N., 2A.','2638-2311','EN CURSO','17/11/2020','OPEN',4);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MILAGROS SUSAN ','HERRERA CARBAJAL','JOAN XXIII , 12, 1R, 2A.','HERRERA CARBAJAL JOAN XXIII , 12, 1R, 2A.','2915-2809','EN CURSO','17/11/2020','OPEN',4);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GUILLERMO','HORRUITINER MARTINEZ','BERTRAND I SERRA , 11, 3R.','HORRUITINER MARTINEZ BERTRAND I SERRA , 11, 3R.','2903-2879','EN CURSO','17/11/2020','OPEN',4);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('LOURDES','HUAMANI FLORES','LA SARDANA , 1','HUAMANI FLORES LA SARDANA , 1','2831-2594','EN CURSO','17/11/2020','OPEN',4);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('LUIS ARMANDO','HUAPAYA RAYGADA','GALILEU , 12','HUAPAYA RAYGADA GALILEU , 12','2957-2409','EN CURSO','17/11/2020','OPEN',4);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARCOS','HUARCAYA QUISPE','SANT VALENTÍ , 11','HUARCAYA QUISPE SANT VALENTÍ , 11','2876-2626','EN CURSO','17/11/2020','OPEN',4);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('WALTER DAVID','HUAYTAN SAUÑE','JOAN XXIII , 43','HUAYTAN SAUÑE JOAN XXIII , 43','2312-2457','EN CURSO','17/11/2020','OPEN',4);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ELBA MERCEDES ','LA ROSA FABIAN','DE LA PAU , 1','LA ROSA FABIAN DE LA PAU , 1','2393-2308','EN CURSO','17/11/2020','OPEN',4);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('PEDRO GUILLERMO','LANDA GINOCCHIO','SANT ANTONI MARIA CLARET , 11','LANDA GINOCCHIO SANT ANTONI MARIA CLARET , 11','2554-2283','EN CURSO','17/11/2020','OPEN',5);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ROBERTO JULIAN','LLAJA TAFUR','AVINGUDA TRES , 1, 3R., 1A.','LLAJA TAFUR AVINGUDA TRES , 1, 3R., 1A.','2844-2892','EN CURSO','17/11/2020','OPEN',5);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ORFELINA','LLENPEN NUÑEZ','PROL. PADRÓ , 1, 2N., 1A.','LLENPEN NUÑEZ PROL. PADRÓ , 1, 2N., 1A.','2771-2349','EN CURSO','17/11/2020','OPEN',5);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('HECTOR','LUJAN VENEGAS','SANT JOAN , 0, C, 3R. A','LUJAN VENEGAS SANT JOAN , 0, C, 3R. A','2587-2786','EN CURSO','17/11/2020','OPEN',5);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GISSELA','MAGUIÑA SAN YEN MAN','PROL. JACINT VERDAGUER , 1, 1R., 1A.','MAGUIÑA SAN YEN MAN PROL. JACINT VERDAGUER , 1, 1R., 1A.','2737-2638','EN CURSO','17/11/2020','OPEN',5);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('COSME ADOLFO','MALDONADO QUISPE','JOAN XXIII , 11, 1R., 1A.','MALDONADO QUISPE JOAN XXIII , 11, 1R., 1A.','2357-2283','EN CURSO','17/11/2020','OPEN',5);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('SANDRA MONICA','MALDONADO TINCO','LLUÍS CASTELLS , 12, 1R.','MALDONADO TINCO LLUÍS CASTELLS , 12, 1R.','2934-2518','EN CURSO','17/11/2020','OPEN',5);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JENNY MARIA','MALLQUI CELESTINO','PADRÓ , 83','MALLQUI CELESTINO PADRÓ , 83','2509-2805','EN CURSO','17/11/2020','OPEN',5);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('SANTIAGO','MAMANI UCHASARA','SANT ISCLE , 1','MAMANI UCHASARA SANT ISCLE , 1','2206-2798','EN CURSO','17/11/2020','OPEN',5);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MAGDA JANETH','MARAVI NAVARRO','MONTSERRAT , 10','MARAVI NAVARRO MONTSERRAT , 10','2807-2590','EN CURSO','17/11/2020','OPEN',5);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARTIN','MARTINEZ MARQUEZ','PROL. JACINT VERDAGUER , 1, 1R., 2A.','MARTINEZ MARQUEZ PROL. JACINT VERDAGUER , 1, 1R., 2A.','2422-2541','EN CURSO','17/11/2020','OPEN',5);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('OSCAR ENRIQUE','MEDINA ZUTA','TRABUCAIRES , 12','MEDINA ZUTA TRABUCAIRES , 12','2621-2524','EN CURSO','17/11/2020','OPEN',5);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CARLOS P','MELGAREJO VIBES','JACINT VERDAGUER , 49, 4T., 2A.','MELGAREJO VIBES JACINT VERDAGUER , 49, 4T., 2A.','2450-2826','EN CURSO','17/11/2020','OPEN',5);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ELIZABETH','MIGUEL HOLGADO','MANELIC , 1','MIGUEL HOLGADO MANELIC , 1','2840-2784','EN CURSO','17/11/2020','OPEN',6);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MANUEL ANTONIO','MORI RAMIREZ','VERGE DE FÀTIMA , 6, BX., 2A.','MORI RAMIREZ VERGE DE FÀTIMA , 6, BX., 2A.','2974-2386','EN CURSO','17/11/2020','OPEN',6);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CARLOS ALBERTO','NUÑEZ HUAYANAY','SANT JOAN , 0, D, 3R. A','NUÑEZ HUAYANAY SANT JOAN , 0, D, 3R. A','2952-2516','EN CURSO','17/11/2020','OPEN',6);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('OLGA','ORE REYES','GALILEU , 12','ORE REYES GALILEU , 12','2222-2965','EN CURSO','17/11/2020','OPEN',6);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JOSUE','ORRILLO ORTIZ','ESPORTS , 12','ORRILLO ORTIZ ESPORTS , 12','2324-2950','EN CURSO','17/11/2020','OPEN',6);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JOSUÉ VICTOR','ORRILLO ORTIZ','JOSEP BOIXADERAS , 1','ORRILLO ORTIZ JOSEP BOIXADERAS , 1','2299-2488','EN CURSO','17/11/2020','OPEN',6);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CARMEN ROSA','PARDAVE CAMACHO','CERVANTES , 1, 1R.','PARDAVE CAMACHO CERVANTES , 1, 1R.','2430-2496','EN CURSO','17/11/2020','OPEN',6);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('SANTIAGO VICTOR','PAREDES JARAMILLO','CERVANTES , 9, 1R.','PAREDES JARAMILLO CERVANTES , 9, 1R.','2384-2618','EN CURSO','17/11/2020','OPEN',6);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ARTURO','PASTOR PORRAS','JOAN XXIII , 39','PASTOR PORRAS JOAN XXIII , 39','2632-2406','EN CURSO','17/11/2020','OPEN',6);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ENRIQUE','PINEDO NUÑEZ','DOCTOR BARNARD , 10','PINEDO NUÑEZ DOCTOR BARNARD , 10','2550-2957','EN CURSO','17/11/2020','OPEN',6);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('SONIA','PRADA VILCHEZ','ESPORTS , 12','PRADA VILCHEZ ESPORTS , 12','2693-2353','EN CURSO','17/11/2020','OPEN',6);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GERARDO DAVID','RIEGA CALLE','DE LA PAU , 8','RIEGA CALLE DE LA PAU , 8','2300-2530','EN CURSO','17/11/2020','OPEN',6);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('FREDDY','RIOS LIMA','SANT VALENTÍ , 11','RIOS LIMA SANT VALENTÍ , 11','2461-2266','EN CURSO','17/11/2020','OPEN',6);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('TERESA','RIOS LIMA','PROL. PADRÓ , 1, 2N., 2A.','RIOS LIMA PROL. PADRÓ , 1, 2N., 2A.','2827-2328','EN CURSO','17/11/2020','OPEN',7);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JUAN ELVIS','RIQUELME MIRANDA','VIC , 30 (TORROELLA)','RIQUELME MIRANDA VIC , 30 (TORROELLA)','2322-2381','EN CURSO','17/11/2020','OPEN',7);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GEORGINA ESPERANZA','ROA YANAC','SANT ISCLE , 6','ROA YANAC SANT ISCLE , 6','2873-2516','EN CURSO','17/11/2020','OPEN',7);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ROSA LILIANA','ROBLES VALVERDE','JAUME BALMES , 70, 3R, 1A.','ROBLES VALVERDE JAUME BALMES , 70, 3R, 1A.','2855-2486','EN CURSO','17/11/2020','OPEN',7);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ROSA JOSEFA','RODRIGUEZ FARIAS','GERMAN DURAN , 27, 3R., 1A.','RODRIGUEZ FARIAS GERMAN DURAN , 27, 3R., 1A.','2459-2644','EN CURSO','17/11/2020','OPEN',7);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARIA DE FATIMA','ROJAS VALDIVIA','SANT JOAN , 11','ROJAS VALDIVIA SANT JOAN , 11','2628-2688','EN CURSO','17/11/2020','OPEN',7);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ROSA MARIA','ROMERO GOMEZ SANCHEZ','PUIG , 1','ROMERO GOMEZ SANCHEZ PUIG , 1','2947-2907','EN CURSO','17/11/2020','OPEN',7);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CARINA MAGNOLIA','ROSALES FLORES','PROL. JACINT VERDAGUER , 1, 2N., 2A.','ROSALES FLORES PROL. JACINT VERDAGUER , 1, 2N., 2A.','2425-2966','EN CURSO','17/11/2020','OPEN',7);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CARLOS JOSE','ROSAS BONIFAZ','RAMON I CAJAL , 81, 2N.','ROSAS BONIFAZ RAMON I CAJAL , 81, 2N.','2871-2516','EN CURSO','17/11/2020','OPEN',7);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('AIDA CRISTINA','RUIZ DE CASTILLA BRITTO','MORAGUES , 10','RUIZ DE CASTILLA BRITTO MORAGUES , 10','2988-2434','EN CURSO','17/11/2020','OPEN',7);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CELIN','SALCEDO DEL PINO','PROL. PADRÓ , 1, 3R., 2A.','SALCEDO DEL PINO PROL. PADRÓ , 1, 3R., 2A.','2392-2956','EN CURSO','17/11/2020','OPEN',7);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('VIOLETA MARILU','SALINAS PUCCIO','FRANCESC DE VITÒRIA , 11, 4T 2A','SALINAS PUCCIO FRANCESC DE VITÒRIA , 11, 4T 2A','2944-2667','EN CURSO','17/11/2020','OPEN',7);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('AUGUSTO','SANCHEZ ARONE','ALBÉNIZ , 22, 2N.','SANCHEZ ARONE ALBÉNIZ , 22, 2N.','2819-2711','EN CURSO','17/11/2020','OPEN',7);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('PEDRO MANUEL','SANTA CRUZ BENSSA','TRES ROURES , 10, 4T 2A','SANTA CRUZ BENSSA TRES ROURES , 10, 4T 2A','2323-2453','EN CURSO','17/11/2020','OPEN',8);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ANGEL','SOLANO VARGAS','PROL. PADRÓ , 1, 2N., 1A.','SOLANO VARGAS PROL. PADRÓ , 1, 2N., 1A.','2757-2642','EN CURSO','17/11/2020','OPEN',8);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JOSE ALBERTO','TEJEDO LUNA','ALBÉNIZ , 13, 2N., 1A.','TEJEDO LUNA ALBÉNIZ , 13, 2N., 1A.','2449-2342','EN CURSO','17/11/2020','OPEN',8);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ANGEL ','TENORIO DAVILA','FONT DEL GAT , 1','TENORIO DAVILA FONT DEL GAT , 1','2763-2326','EN CURSO','17/11/2020','OPEN',8);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MIGUEL ANGEL ','TORRES GASPAR','MONTCAU , 1','TORRES GASPAR MONTCAU , 1','2325-2743','EN CURSO','17/11/2020','OPEN',8);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JACQUELIN','TRUJILLO PARODI','MONTURIOL , 1','TRUJILLO PARODI MONTURIOL , 1','2419-2486','EN CURSO','17/11/2020','OPEN',8);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('RUTH NORICILA','VEGA CARREAZO','VILATORRADA , 6','VEGA CARREAZO VILATORRADA , 6','2864-2694','EN CURSO','17/11/2020','OPEN',8);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GUILLERMO JONATHAN','VELASQUEZ RAMOS','JAUME GALOBART , 11','VELASQUEZ RAMOS JAUME GALOBART , 11','2231-2873','EN CURSO','17/11/2020','OPEN',8);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ALEJANDRO','VERA SILVA','SANT GENÍS , 25','VERA SILVA SANT GENÍS , 25','2633-2728','EN CURSO','17/11/2020','OPEN',8);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('BLANCA KATTY','VILCA LUCERO','VERGE DE FÀTIMA , 2, 3R., 1A.','VILCA LUCERO VERGE DE FÀTIMA , 2, 3R., 1A.','2369-2658','EN CURSO','17/11/2020','OPEN',8);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ENRIQUE GODOFREDO','VILGOSO ALVARADO','JAUME I , 10','VILGOSO ALVARADO JAUME I , 10','2942-2626','EN CURSO','17/11/2020','OPEN',8);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CECILIA','YAMAWAKI ONAGA','SANT JOAN , 0, C, 1R., B','YAMAWAKI ONAGA SANT JOAN , 0, C, 1R., B','2667-2869','EN CURSO','17/11/2020','OPEN',8);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARIELA MILAGROS','ZAMALLOA VEGA','JACINT VERDAGUER , 13','ZAMALLOA VEGA JACINT VERDAGUER , 13','2818-2835','EN CURSO','17/11/2020','OPEN',8);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MONICA ','ZAPATA CHANG','SANT BENET , 28, 2N., 2A.','ZAPATA CHANG SANT BENET , 28, 2N., 2A.','2284-2719','EN CURSO','17/11/2020','OPEN',9);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JUAN CARLOS','ZEGARRA SALCEDO','FONERIA , 12','ZEGARRA SALCEDO FONERIA , 12','2404-2230','EN CURSO','17/11/2020','OPEN',9);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('HILRICH MARIELA','ZU FLORES','ALFONS XII , 9, 4T 1A','ZU FLORES ALFONS XII , 9, 4T 1A','2309-2542','EN CURSO','17/11/2020','OPEN',9);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ESTEFANIA','AROCAS PASADAS','JAUME I , 10','AROCAS PASADAS JAUME I , 10','2911-2479','EN CURSO','17/11/2020','OPEN',9);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('QUERALT','VISO GILABERT','DE LA PESCA , 1, 1R., 2A.','VISO GILABERT DE LA PESCA , 1, 1R., 2A.','2795-2828','EN CURSO','17/11/2020','OPEN',9);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JOAN','AYALA FERRERAS','PIRINEUS , 22','AYALA FERRERAS PIRINEUS , 22','2607-2761','EN CURSO','17/11/2020','OPEN',9);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JOAN','BAEZ TEJADO','PROL. JACINT VERDAGUER , 1, 3R., 2A.','BAEZ TEJADO PROL. JACINT VERDAGUER , 1, 3R., 2A.','2792-2831','EN CURSO','17/11/2020','OPEN',9);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARC','BASTARDES SOTO','JAUME GALOBART , 14','BASTARDES SOTO JAUME GALOBART , 14','2741-2232','EN CURSO','17/11/2020','OPEN',9);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JOSEP','ANGUERA VILAFRANCA','SANT JOAN, EDIFICI D , 3R A','ANGUERA VILAFRANCA SANT JOAN, EDIFICI D , 3R A','2572-2860','EN CURSO','17/11/2020','OPEN',9);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ESTHER','PASCUAL ALOY','VIC , 119, 3R., 2A.','PASCUAL ALOY VIC , 119, 3R., 2A.','2247-2881','EN CURSO','17/11/2020','OPEN',9);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('LAURA','VALLÉS GIRVENT','CARLES BUÏGAS , 10, 1R., 1A.','VALLÉS GIRVENT CARLES BUÏGAS , 10, 1R., 1A.','2999-2326','EN CURSO','17/11/2020','OPEN',9);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('RAQUEL','RAYA GARCIA','JAUME BALMES , 67, 2N.','RAYA GARCIA JAUME BALMES , 67, 2N.','2622-2286','EN CURSO','17/11/2020','OPEN',9);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JOAN','ANDREU CRUZ','PROL. PADRÓ , 1, 3R., 2A.','ANDREU CRUZ PROL. PADRÓ , 1, 3R., 2A.','2694-2693','EN CURSO','17/11/2020','OPEN',9);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARIA ISABEL','BARALDÉS COMAS','DE LA CAÇA , 12, 2N., C','BARALDÉS COMAS DE LA CAÇA , 12, 2N., C','2403-2773','EN CURSO','17/11/2020','OPEN',10);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ADRIÀ','BERENGUERAS CULLERÉS','VIC , 119, 2N., 1A.','BERENGUERAS CULLERÉS VIC , 119, 2N., 1A.','2771-2802','EN CURSO','17/11/2020','OPEN',10);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GERARD','LÓPEZ DE PABLO GARCIA UCEDA','MORAGUES , 1','LÓPEZ DE PABLO GARCIA UCEDA MORAGUES , 1','2358-2247','EN CURSO','17/11/2020','OPEN',10);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ELIOT','ARNAU MORENO','TRABUCAIRES , 12','ARNAU MORENO TRABUCAIRES , 12','2515-2354','EN CURSO','17/11/2020','OPEN',10);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JORDI','RAYA GAVILAN','FONERIA , 12','RAYA GAVILAN FONERIA , 12','2752-2911','EN CURSO','17/11/2020','OPEN',10);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('LLUÍS','ZAMBUDIO FIGULS','PADRÓ , 109','ZAMBUDIO FIGULS PADRÓ , 109','2804-2927','EN CURSO','17/11/2020','OPEN',10);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('LAURA','BIDAULT CULLERÉS','CASA CORDELLAS , ','BIDAULT CULLERÉS CASA CORDELLAS , ','2233-2811','EN CURSO','17/11/2020','OPEN',10);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JORDI','BIOSCA FONTANET','DOCTOR FLEMING , 11','BIOSCA FONTANET DOCTOR FLEMING , 11','2627-2763','EN CURSO','17/11/2020','OPEN',10);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('DOUNYA','ZAFRA FIGULS','BERTRAND I SERRA , 11, 3R.','ZAFRA FIGULS BERTRAND I SERRA , 11, 3R.','2392-2926','EN CURSO','17/11/2020','OPEN',10);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JULIO','ALEU ICART','CARRIÓ , 12, 5È A','ALEU ICART CARRIÓ , 12, 5È A','2519-2998','EN CURSO','17/11/2020','OPEN',10);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ANDREU','BADIA TORNÉ','PIRINEUS , 10','BADIA TORNÉ PIRINEUS , 10','2206-2775','EN CURSO','17/11/2020','OPEN',10);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('RAMON','MORALES GESE','JACINT VERDAGUER , 43','MORALES GESE JACINT VERDAGUER , 43','2641-2488','EN CURSO','17/11/2020','OPEN',10);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('DAVID-JESE','BLANCO FONTANET','NOU , 9, 2N.','BLANCO FONTANET NOU , 9, 2N.','2994-2323','EN CURSO','17/11/2020','OPEN',10);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ARAN','ALVAREZ FERNÁNDEZ','JACINT VERDAGUER , 52, 3R, 1A.','ALVAREZ FERNÁNDEZ JACINT VERDAGUER , 52, 3R, 1A.','2447-2938','EN CURSO','17/11/2020','OPEN',11);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GEMMA','GARCIA ALMOGUERA','JOAN MIRÓ , 10','GARCIA ALMOGUERA JOAN MIRÓ , 10','2416-2926','EN CURSO','17/11/2020','OPEN',11);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('IVAN','LIBORI FIGUERAS','JAUME GALOBART , 12','LIBORI FIGUERAS JAUME GALOBART , 12','2405-2585','EN CURSO','17/11/2020','OPEN',11);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('DAVID','BIDAULT PUEYO','PINTOR SERT , 12, 1R., 1A.','BIDAULT PUEYO PINTOR SERT , 12, 1R., 1A.','2943-2617','EN CURSO','17/11/2020','OPEN',11);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('XAVIER','BENITEZ JOSE','BELLAVISTA , 30','BENITEZ JOSE BELLAVISTA , 30','2887-2304','EN CURSO','17/11/2020','OPEN',11);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARIO','PASCUAL FLORES','MONTURIOL , 10, 1R.','PASCUAL FLORES MONTURIOL , 10, 1R.','2411-2420','EN CURSO','17/11/2020','OPEN',11);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JESUS','AYALA TORNÉ','JACINT VERDAGUER , 52, 2N., 4A.','AYALA TORNÉ JACINT VERDAGUER , 52, 2N., 4A.','2872-2604','EN CURSO','17/11/2020','OPEN',11);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GEMMA','LISTAN FIGUERAS','CASA NOVA , ','LISTAN FIGUERAS CASA NOVA , ','2554-2738','EN CURSO','17/11/2020','OPEN',11);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('SILVIA','RASERO GAVILAN','DE LA CAÇA , 12, 1R., C','RASERO GAVILAN DE LA CAÇA , 12, 1R., C','2536-2866','EN CURSO','17/11/2020','OPEN',11);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ALBERT','ARNALOT PUIG','PINTOR SERT , 12, 2N., 1A.','ARNALOT PUIG PINTOR SERT , 12, 2N., 1A.','2974-2808','EN CURSO','17/11/2020','OPEN',11);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARIA','MOLINER GARRIDO','CASA SARA , ','MOLINER GARRIDO CASA SARA , ','2201-2580','EN CURSO','17/11/2020','OPEN',11);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('BERTA','GALOBART GARCIA','ARTÈS , 1, 1R, 1A.','GALOBART GARCIA ARTÈS , 1, 1R, 1A.','2434-2680','EN CURSO','17/11/2020','OPEN',11);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('BERTA','LÓPEZ GARRIGASSAIT','GENERAL PRIM , 11, 2N.','LÓPEZ GARRIGASSAIT GENERAL PRIM , 11, 2N.','2700-2657','EN CURSO','17/11/2020','OPEN',11);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MIREIA','SÁNCHEZ GÓMEZ','CAU DE LA GUINEU , 4','SÁNCHEZ GÓMEZ CAU DE LA GUINEU , 4','2644-2993','EN CURSO','17/11/2020','OPEN',12);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GEMMA','ALAVEDRA SUNYÉ','JOAN SANMARTÍ , 12, 2N., 2A.','ALAVEDRA SUNYÉ JOAN SANMARTÍ , 12, 2N., 2A.','2956-2203','EN CURSO','17/11/2020','OPEN',12);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARIA ISABEL','ALIGUÉ BONVEHÍ','PROL. PADRÓ , 1, 3R. 1A.','ALIGUÉ BONVEHÍ PROL. PADRÓ , 1, 3R. 1A.','2437-2782','EN CURSO','17/11/2020','OPEN',12);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('TONI','MAS FRANCH','SALLENT , 22, 2N.','MAS FRANCH SALLENT , 22, 2N.','2619-2637','EN CURSO','17/11/2020','OPEN',12);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ALEJANDRO','ALOY COMPTE','JOAN MIRÓ , 3','ALOY COMPTE JOAN MIRÓ , 3','2823-2735','EN CURSO','17/11/2020','OPEN',12);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JOAN MARTÍ','ASENSIO VEGA','LLUÍS CASTELLS , 12, 2N.','ASENSIO VEGA LLUÍS CASTELLS , 12, 2N.','2999-2432','EN CURSO','17/11/2020','OPEN',12);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('INGRID','BIDAULT PÉREZ','SANT VALENTÍ , 12, 1R.','BIDAULT PÉREZ SANT VALENTÍ , 12, 1R.','2881-2873','EN CURSO','17/11/2020','OPEN',12);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('OLIVER','ALOY CODINACHS','ÀNGEL GUIMERÀ , 43, 2N','ALOY CODINACHS ÀNGEL GUIMERÀ , 43, 2N','2777-2554','EN CURSO','17/11/2020','OPEN',12);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('SANDRA','ALTIMIRAS ARMENTEROS','JAUME GALOBART , 11','ALTIMIRAS ARMENTEROS JAUME GALOBART , 11','2200-2247','EN CURSO','17/11/2020','OPEN',12);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JORDI','BELMONTE SÁNCHEZ','AVINGUDA TRES , 3, 1R., 1A.','BELMONTE SÁNCHEZ AVINGUDA TRES , 3, 1R., 1A.','2899-2583','EN CURSO','17/11/2020','OPEN',12);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARC','BAJONA GARCIA','JACINT VERDAGUER , 52, 2N., 1A.','BAJONA GARCIA JACINT VERDAGUER , 52, 2N., 1A.','2700-2340','EN CURSO','17/11/2020','OPEN',12);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JORDINA','AGUILAR RODRIGUEZ','DIPUTACIÓ , 10','AGUILAR RODRIGUEZ DIPUTACIÓ , 10','2534-2552','EN CURSO','17/11/2020','OPEN',12);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARIA JOSÉ','BARRIGA SOTO','VIC , 39, 1R., 2A.','BARRIGA SOTO VIC , 39, 1R., 2A.','2831-2564','EN CURSO','17/11/2020','OPEN',12);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('RAQUEL','AVILA MASJUAN','GERMAN DURAN , 21','AVILA MASJUAN GERMAN DURAN , 21','2982-2856','EN CURSO','17/11/2020','OPEN',13);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ENRIC','PARRAMON FLORES','BELLAVISTA , 30','PARRAMON FLORES BELLAVISTA , 30','2325-2848','EN CURSO','17/11/2020','OPEN',13);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARTA','AGUILAR RAMOS','NOU , 7, 2N.','AGUILAR RAMOS NOU , 7, 2N.','2716-2311','EN CURSO','17/11/2020','OPEN',13);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CARLA','AYALA ALSINA','MANELIC , 1','AYALA ALSINA MANELIC , 1','2276-2601','EN CURSO','17/11/2020','OPEN',13);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARIA NOELIA','ALVAREZ TROYANO','DE LA PESCA , 1, 1R., 1A.','ALVAREZ TROYANO DE LA PESCA , 1, 1R., 1A.','2555-2531','EN CURSO','17/11/2020','OPEN',13);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CRISTINA','ALINS GONZÁLEZ','PIRINEUS , 34','ALINS GONZÁLEZ PIRINEUS , 34','2919-2794','EN CURSO','17/11/2020','OPEN',13);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CARLOS','ACUÑA TORT','PROL. JACINT VERDAGUER , 1, 2N., 2A.','ACUÑA TORT PROL. JACINT VERDAGUER , 1, 2N., 2A.','2936-2642','EN CURSO','17/11/2020','OPEN',13);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('DAVID','ALGUÉ TRANCHO','MALLORCA , 11','ALGUÉ TRANCHO MALLORCA , 11','2278-2742','EN CURSO','17/11/2020','OPEN',13);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CRISTIAN','BADIA CASTILLO','SANT BENET , 12, 2N.','BADIA CASTILLO SANT BENET , 12, 2N.','2992-2213','EN CURSO','17/11/2020','OPEN',13);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JULIO ALBERTO','BENITEZ FLORES','PROL. PADRÓ , 1, 2N., 2A.','BENITEZ FLORES PROL. PADRÓ , 1, 2N., 2A.','2944-2603','EN CURSO','17/11/2020','OPEN',13);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('SERGI','TORRUELLA GARCIA','ARTÈS , 1, 2N., 2A.','TORRUELLA GARCIA ARTÈS , 1, 2N., 2A.','2747-2426','EN CURSO','17/11/2020','OPEN',13);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ALEIX','ALBERICH RODRIGUEZ','JOAN XXIII , 12, 1R, 2A.','ALBERICH RODRIGUEZ JOAN XXIII , 12, 1R, 2A.','2431-2834','EN CURSO','17/11/2020','OPEN',13);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('VERÒNICA','ARMENCOT PUIG','BERTRAND I SERRA , 11, 3R.','ARMENCOT PUIG BERTRAND I SERRA , 11, 3R.','2325-2963','EN CURSO','17/11/2020','OPEN',13);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARIONA','ALIGUÉ RIVERA','LA SARDANA , 1','ALIGUÉ RIVERA LA SARDANA , 1','2527-2856','EN CURSO','17/11/2020','OPEN',14);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARC','BARRIGA RIU','GALILEU , 12','BARRIGA RIU GALILEU , 12','2499-2907','EN CURSO','17/11/2020','OPEN',14);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GEMMA','PORTELLA GISPETS','SANT VALENTÍ , 11','PORTELLA GISPETS SANT VALENTÍ , 11','2274-2712','EN CURSO','17/11/2020','OPEN',14);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('RICARD','AGUILERA BAENA','JOAN XXIII , 43','AGUILERA BAENA JOAN XXIII , 43','2465-2505','EN CURSO','17/11/2020','OPEN',14);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JUAN','RODRIGUEZ GARCÍA','DE LA PAU , 1','RODRIGUEZ GARCÍA DE LA PAU , 1','2251-2948','EN CURSO','17/11/2020','OPEN',14);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARTA','AGUILAR SUNYÉ','SANT ANTONI MARIA CLARET , 11','AGUILAR SUNYÉ SANT ANTONI MARIA CLARET , 11','2821-2761','EN CURSO','17/11/2020','OPEN',14);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('NATÀLIA','BARRIGA TARDÀ','AVINGUDA TRES , 1, 3R., 1A.','BARRIGA TARDÀ AVINGUDA TRES , 1, 3R., 1A.','2271-2563','EN CURSO','17/11/2020','OPEN',14);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARTA','BARCONS LARA','PROL. PADRÓ , 1, 2N., 1A.','BARCONS LARA PROL. PADRÓ , 1, 2N., 1A.','2617-2675','EN CURSO','17/11/2020','OPEN',14);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('LAURA','AGUILERA TATJÉ','SANT JOAN , 0, C, 3R. A','AGUILERA TATJÉ SANT JOAN , 0, C, 3R. A','2314-2370','EN CURSO','17/11/2020','OPEN',14);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JOAN','ALEU PRAT','PROL. JACINT VERDAGUER , 1, 1R., 1A.','ALEU PRAT PROL. JACINT VERDAGUER , 1, 1R., 1A.','2380-2300','EN CURSO','17/11/2020','OPEN',14);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ALEXIA','VALLÉS GIRVENT','JOAN XXIII , 11, 1R., 1A.','VALLÉS GIRVENT JOAN XXIII , 11, 1R., 1A.','2888-2390','EN CURSO','17/11/2020','OPEN',14);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('FERRAN','MOLINA GARRIDO','LLUÍS CASTELLS , 12, 1R.','MOLINA GARRIDO LLUÍS CASTELLS , 12, 1R.','2794-2403','EN CURSO','17/11/2020','OPEN',14);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CRISTINA','ARISSA HERMOSO','PADRÓ , 83','ARISSA HERMOSO PADRÓ , 83','2539-2249','EN CURSO','17/11/2020','OPEN',14);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JOSÉ ANTONIO','BARALDÉS PARDO','SANT ISCLE , 1','BARALDÉS PARDO SANT ISCLE , 1','2471-2358','EN CURSO','17/11/2020','OPEN',15);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JORDI','SUAREZ GARZÓN','MONTSERRAT , 10','SUAREZ GARZÓN MONTSERRAT , 10','2446-2295','EN CURSO','17/11/2020','OPEN',15);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('BEGONYA','ARPA MORENO','PROL. JACINT VERDAGUER , 1, 1R., 2A.','ARPA MORENO PROL. JACINT VERDAGUER , 1, 1R., 2A.','2364-2324','EN CURSO','17/11/2020','OPEN',15);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('INGRID','ALOY FARRANDO','TRABUCAIRES , 12','ALOY FARRANDO TRABUCAIRES , 12','2270-2756','EN CURSO','17/11/2020','OPEN',15);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MIQUEL','LUQUE GARRIGASAIT','JACINT VERDAGUER , 49, 4T., 2A.','LUQUE GARRIGASAIT JACINT VERDAGUER , 49, 4T., 2A.','2900-2563','EN CURSO','17/11/2020','OPEN',15);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('AGUSTÍ','RIDÓ GÓMEZ','MANELIC , 1','RIDÓ GÓMEZ MANELIC , 1','2356-2837','EN CURSO','17/11/2020','OPEN',15);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ANTONI','SANTAMARIA FLOTATS','VERGE DE FÀTIMA , 6, BX., 2A.','SANTAMARIA FLOTATS VERGE DE FÀTIMA , 6, BX., 2A.','2638-2257','EN CURSO','17/11/2020','OPEN',15);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JOAN','HERMS GÓMEZ','SANT JOAN , 0, D, 3R. A','HERMS GÓMEZ SANT JOAN , 0, D, 3R. A','2378-2739','EN CURSO','17/11/2020','OPEN',15);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MÒNICA','ARTIGAS MATURANO','GALILEU , 12','ARTIGAS MATURANO GALILEU , 12','2462-2366','EN CURSO','17/11/2020','OPEN',15);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GERARD','AGUILAR MASANA','ESPORTS , 12','AGUILAR MASANA ESPORTS , 12','2923-2549','EN CURSO','17/11/2020','OPEN',15);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GEMMA','ALTIMIRAS SERAROLS','JOSEP BOIXADERAS , 1','ALTIMIRAS SERAROLS JOSEP BOIXADERAS , 1','2760-2637','EN CURSO','17/11/2020','OPEN',15);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARIA','TORRESCASANA GARCIA','CERVANTES , 1, 1R.','TORRESCASANA GARCIA CERVANTES , 1, 1R.','2751-2525','EN CURSO','17/11/2020','OPEN',15);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ORIOL','ARIZA PUIGBÓ','CERVANTES , 9, 1R.','ARIZA PUIGBÓ CERVANTES , 9, 1R.','2432-2748','EN CURSO','17/11/2020','OPEN',15);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('VIRGINIA','ALVAREZ ARMENTEROS','JOAN XXIII , 39','ALVAREZ ARMENTEROS JOAN XXIII , 39','2799-2857','EN CURSO','17/11/2020','OPEN',16);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('DAMIÀ','BARALDÉS TARRAGÓ','DOCTOR BARNARD , 10','BARALDÉS TARRAGÓ DOCTOR BARNARD , 10','2375-2218','EN CURSO','17/11/2020','OPEN',16);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('VALENTÍ','GARCIA GARCÍA','ESPORTS , 12','GARCIA GARCÍA ESPORTS , 12','2899-2755','EN CURSO','17/11/2020','OPEN',16);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('AINA','AROCA GÓMEZ','DE LA PAU , 8','AROCA GÓMEZ DE LA PAU , 8','2966-2963','EN CURSO','17/11/2020','OPEN',16);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('DAVID','ALONSO RODRIGUEZ','SANT VALENTÍ , 11','ALONSO RODRIGUEZ SANT VALENTÍ , 11','2281-2566','EN CURSO','17/11/2020','OPEN',16);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GERARD','CANO GÓMEZ','PROL. PADRÓ , 1, 2N., 2A.','CANO GÓMEZ PROL. PADRÓ , 1, 2N., 2A.','2982-2699','EN CURSO','17/11/2020','OPEN',16);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARTA','ALCAIDE MOLINA','VIC , 30 (TORROELLA)','ALCAIDE MOLINA VIC , 30 (TORROELLA)','2419-2332','EN CURSO','17/11/2020','OPEN',16);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MIREIA','AGUILERA PRAT','SANT ISCLE , 6','AGUILERA PRAT SANT ISCLE , 6','2826-2681','EN CURSO','17/11/2020','OPEN',16);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ELOI','ALAPONT ICART','JAUME BALMES , 70, 3R, 1A.','ALAPONT ICART JAUME BALMES , 70, 3R, 1A.','2710-2948','EN CURSO','17/11/2020','OPEN',16);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ANNA','RIVERO FLORIDO','GERMAN DURAN , 27, 3R., 1A.','RIVERO FLORIDO GERMAN DURAN , 27, 3R., 1A.','2399-2289','EN CURSO','17/11/2020','OPEN',16);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ALBA','AVILA MASJUAN','SANT JOAN , 11','AVILA MASJUAN SANT JOAN , 11','2425-2878','EN CURSO','17/11/2020','OPEN',16);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('SANDRA','GRANADOS ANDRÉS','PUIG , 1','GRANADOS ANDRÉS PUIG , 1','2873-2755','EN CURSO','17/11/2020','OPEN',16);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ERIC','FERRER GASSET','PROL. JACINT VERDAGUER , 1, 2N., 2A.','FERRER GASSET PROL. JACINT VERDAGUER , 1, 2N., 2A.','2665-2518','EN CURSO','17/11/2020','OPEN',16);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('LLUÍS','AMIGO MODREGO','RAMON I CAJAL , 81, 2N.','AMIGO MODREGO RAMON I CAJAL , 81, 2N.','2693-2832','EN CURSO','17/11/2020','OPEN',17);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CRISTIAN','ABDIN TATJÈ','MORAGUES , 10','ABDIN TATJÈ MORAGUES , 10','2401-2259','EN CURSO','17/11/2020','OPEN',17);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('GUILLEM','CANELLAS GOMEZ','PROL. PADRÓ , 1, 3R., 2A.','CANELLAS GOMEZ PROL. PADRÓ , 1, 3R., 2A.','2317-2611','EN CURSO','17/11/2020','OPEN',17);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('DIMAS','HIDALGO ALTIMIRAS','FRANCESC DE VITÒRIA , 11, 4T 2A','HIDALGO ALTIMIRAS FRANCESC DE VITÒRIA , 11, 4T 2A','2564-2764','EN CURSO','17/11/2020','OPEN',17);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ANA INÉS','BASTARDAS FRANCH','ALBÉNIZ , 22, 2N.','BASTARDAS FRANCH ALBÉNIZ , 22, 2N.','2396-2462','EN CURSO','17/11/2020','OPEN',17);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('IVET','ABADIAS MASANA','TRES ROURES , 10, 4T 2A','ABADIAS MASANA TRES ROURES , 10, 4T 2A','2478-2817','EN CURSO','17/11/2020','OPEN',17);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JÚLIA','AREVALO SANCHEZ','PROL. PADRÓ , 1, 2N., 1A.','AREVALO SANCHEZ PROL. PADRÓ , 1, 2N., 1A.','2493-2371','EN CURSO','17/11/2020','OPEN',17);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('DANIEL','ALINS MULET','ALBÉNIZ , 13, 2N., 1A.','ALINS MULET ALBÉNIZ , 13, 2N., 1A.','2577-2838','EN CURSO','17/11/2020','OPEN',17);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ABEL','GARCIA GONZÁLEZ','FONT DEL GAT , 1','GARCIA GONZÁLEZ FONT DEL GAT , 1','2593-2645','EN CURSO','17/11/2020','OPEN',17);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('IRENE','ALVAREZ PARCERISA','MONTCAU , 1','ALVAREZ PARCERISA MONTCAU , 1','2558-2408','EN CURSO','17/11/2020','OPEN',17);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ADRIÀ','CASAS ANDRÉS','MONTURIOL , 1','CASAS ANDRÉS MONTURIOL , 1','2798-2894','EN CURSO','17/11/2020','OPEN',17);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('JAIRO','MORALES GESE','VILATORRADA , 6','MORALES GESE VILATORRADA , 6','2895-2641','EN CURSO','17/11/2020','OPEN',17);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CRISTINA','BARALDÉS MARTORELL','JAUME GALOBART , 11','BARALDÉS MARTORELL JAUME GALOBART , 11','2645-2523','EN CURSO','17/11/2020','OPEN',17);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('DAVID','AROCA GÓMEZ','SANT GENÍS , 25','AROCA GÓMEZ SANT GENÍS , 25','2679-2821','EN CURSO','17/11/2020','OPEN',18);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ADRIÀ','RUEDA ALVAREZ','VERGE DE FÀTIMA , 2, 3R., 1A.','RUEDA ALVAREZ VERGE DE FÀTIMA , 2, 3R., 1A.','2267-2594','EN CURSO','17/11/2020','OPEN',18);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('LUCIA','ALVAREZ DOMENECH','JAUME I , 10','ALVAREZ DOMENECH JAUME I , 10','2810-2534','EN CURSO','17/11/2020','OPEN',18);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('CARLA','BOIX GONZÁLEZ','SANT JOAN , 0, C, 1R., B','BOIX GONZÁLEZ SANT JOAN , 0, C, 1R., B','2280-2215','EN CURSO','17/11/2020','OPEN',18);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ADRIÀ','BARALDÉS MONRÓS','JACINT VERDAGUER , 13','BARALDÉS MONRÓS JACINT VERDAGUER , 13','2831-2766','EN CURSO','17/11/2020','OPEN',18);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARTA','AGUILERA MERINO','SANT BENET , 28, 2N., 2A.','AGUILERA MERINO SANT BENET , 28, 2N., 2A.','2598-2467','EN CURSO','17/11/2020','OPEN',18);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('MARC','BAREA DHAENE','FONERIA , 12','BAREA DHAENE FONERIA , 12','2491-2451','EN CURSO','17/11/2020','OPEN',18);
INSERT INTO Estudiante(nombre,apellido,direccion,padreResponsable,telefono,estado_promedio,fecha_creacion,estado,id_grado) VALUES('ALEX','BARROSO DHAENE','ALFONS XII , 9, 4T 1A','BARROSO DHAENE ALFONS XII , 9, 4T 1A','2715-2426','EN CURSO','17/11/2020','OPEN',18);



--DOCENTE_MATERIA_GRADO

	--PRIMER CICLO

INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(1,1,1,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(1,1,2,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(1,1,3,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(1,1,11,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(1,1,10,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(1,1,12,'17/11/2020','OPEN');

INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(2,2,1,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(2,2,2,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(2,2,3,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(2,2,11,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(2,2,10,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(2,2,12,'17/11/2020','OPEN');

INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(3,3,1,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(3,3,2,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(3,3,3,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(3,3,11,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(3,3,10,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(3,3,12,'17/11/2020','OPEN');

INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(4,4,1,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(4,4,2,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(4,4,3,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(4,4,11,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(4,4,10,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(4,4,12,'17/11/2020','OPEN');

INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(5,5,1,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(5,5,2,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(5,5,3,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(5,5,11,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(5,5,10,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(5,5,12,'17/11/2020','OPEN');

INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(6,6,1,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(6,6,2,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(6,6,3,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(6,6,11,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(6,6,10,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(6,6,12,'17/11/2020','OPEN');


	--SEGUNDO CICLO
	
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(7,1,4,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(8,1,5,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(9,1,6,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(10,1,13,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(11,1,14,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(12,1,15,'17/11/2020','OPEN');

INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(7,2,4,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(8,2,5,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(9,2,6,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(10,2,13,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(11,2,14,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(12,2,15,'17/11/2020','OPEN');

INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(7,3,4,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(8,3,5,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(9,3,6,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(10,3,13,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(11,3,14,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(12,3,15,'17/11/2020','OPEN');

INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(7,4,4,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(8,4,5,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(9,4,6,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(10,4,13,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(11,4,14,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(12,4,15,'17/11/2020','OPEN');

INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(7,5,4,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(8,5,5,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(9,5,6,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(10,5,13,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(11,5,14,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(12,5,15,'17/11/2020','OPEN');

INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(7,6,4,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(8,6,5,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(9,6,6,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(10,6,13,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(11,6,14,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(12,6,14,'17/11/2020','OPEN');	


	--TERCER CICLO
	
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(13,1,7,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(14,1,8,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(15,1,9,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(16,1,16,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(17,1,17,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(18,1,18,'17/11/2020','OPEN');

INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(13,2,7,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(14,2,8,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(15,2,9,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(16,2,16,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(17,2,17,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(18,2,18,'17/11/2020','OPEN');
																	
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(13,3,7,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(14,3,8,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(15,3,9,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(16,3,16,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(17,3,17,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(18,3,18,'17/11/2020','OPEN');
																	 
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(13,4,7,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(14,4,8,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(15,4,9,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(16,4,16,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(17,4,17,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(18,4,18,'17/11/2020','OPEN');
																	  
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(13,5,7,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(14,5,8,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(15,5,9,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(16,5,16,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(17,5,17,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(18,5,18,'17/11/2020','OPEN');
																	  
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(13,6,7,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(14,6,8,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(15,6,9,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(16,6,16,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(17,6,17,'17/11/2020','OPEN');
INSERT INTO Docente_Materia_Grado(id_docente,id_materia,id_grado,fecha,estado) VALUES(18,6,18,'17/11/2020','OPEN');

--DOCENTE_GRADO
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(1,1,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(2,2,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(3,3,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(4,4,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(5,5,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(6,6,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(7,7,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(8,8,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(9,9,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(10,10,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(11,11,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(12,12,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(13,13,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(14,14,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(15,15,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(16,16,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(17,17,'17/11/2020','OPEN');
INSERT INTO Docente_Grado(id_docente,id_grado,fecha,estado) VALUES(18,18,'17/11/2020','OPEN');



-- PROCEDURE

--1----------------------------------------------------
create procedure reporteDocente
as
select * from Docente where estado = 'OPEN';
------------------------------------------------------


--2---------------------------------------------------------------------------------------------------------------------------------
create procedure reporteMateriasDocente
as
select d.id_docente,d.nombre,d.apellido,m.nombre_materia, g.grado,s.seccion from Docente d inner join Docente_Materia_Grado dmg on d.id_docente = dmg.id_docente
inner join Materia m on m.id_materia = dmg.id_materia
inner join Grado g on g.id_grado = dmg.id_grado
inner join Seccion s on s.id_seccion = g.id_seccion
where d.estado = 'OPEN' and dmg.estado = 'OPEN' and m.estado = 'OPEN' and g.estado = 'OPEN' and s.estado = 'OPEN'
GROUP BY d.id_docente,d.nombre,d.apellido,m.nombre_materia, g.grado,s.seccion;
--------------------------------------------------------------------------------------------------------------------------------------


--3------------------------------------------------------------------------------------------------------------------------------------
create procedure reporteDocenteGrado
as
select d.id_docente,d.nombre,d.apellido,g.grado,s.seccion from Docente d inner join Docente_Grado dg on d.id_docente = dg.id_docente
inner join Grado g on g.id_grado = dg.id_grado 
inner join Seccion s on s.id_seccion = g.id_seccion
Where d.estado = 'OPEN' and dg.estado = 'OPEN' and g.estado = 'OPEN' and s.estado = 'OPEN';
---------------------------------------------------------------------------------------------------------------------------------------


--4-------------------------------------------------------------------------------------------------------------------------------------
create procedure rptBoletaPorEstudiante
@idEstudiante int
as
declare @idGrado int
set @idGrado = (select id_grado from Estudiante where id_estudiante = @idEstudiante)

SELECT distinct e.id_estudiante,e.nombre,e.apellido,m.nombre_materia,
(select CONVERT(decimal(18,2),SUM(promedio)/3) from Nota where id_estudiante = e.id_estudiante and id_materia = n.id_materia and e.id_grado = @idGrado) as promedio
FROM Estudiante e inner join grado g on g.id_grado = e.id_grado 
inner join Seccion s on s.id_seccion = g.id_seccion
inner join Nota n on e.id_estudiante = n.id_estudiante
inner join Materia m on m.id_materia = n.id_materia
where e.id_estudiante = @idEstudiante;
-----------------------------------------------------------------------------------------------------------------------------------------

--5---------------------------------------------------------------------------
CREATE procedure rptEstudiantesMatriculado
@idGrado int
as
select * from Estudiante e INNER JOIN GRADO g ON e.id_grado = g.id_grado and e.id_grado = @idGrado and e.estado = 'OPEN' INNER JOIN SECCION s on s.id_seccion=g.id_seccion
-------------------------------------------------------------------------------


--6---------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE reporteNotaPorMateria
@idMateria int,
@idGrado int,
@idPeriodo int
as
select e.id_estudiante,e.nombre,e.apellido,e.direccion,e.padreResponsable,e.telefono,e.fecha_creacion,n.id_nota,n.actividad1,n.actividad2,n.actividad3,n.examen,n.promedio,p.periodo,m.nombre_materia,g.grado,s.seccion from Estudiante e inner join nota n on n.id_estudiante = e.id_estudiante
inner join Periodo p on p.id_periodo = n.id_periodo
inner join Materia m on n.id_materia = m.id_materia
inner join Grado g on g.id_grado = e.id_grado
inner join Seccion s on s.id_seccion = g.id_seccion
where p.id_periodo = @idPeriodo and g.id_grado = @idGrado and m.id_materia = @idMateria and e.estado = 'OPEN' and m.estado = 'OPEN' and g.estado = 'OPEN';
-------------------------------------------------------------------------------------------------------------------------------------